--------------------------------------------------------------------------------
--                               Compressor_3_2
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_3_2 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          R : out  std_logic_vector(1 downto 0)   );
end entity;

architecture arch of Compressor_3_2 is
signal X :  std_logic_vector(2 downto 0);
begin
   X <=X0 ;
   with X select R <= 
      "00" when "000", 
      "01" when "001", 
      "01" when "010", 
      "10" when "011", 
      "01" when "100", 
      "10" when "101", 
      "10" when "110", 
      "11" when "111", 
      "--" when others;

end architecture;

--------------------------------------------------------------------------------
--                               LZOC_23_5_uid3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: 
--------------------------------------------------------------------------------
-- Pipeline depth: 3 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZOC_23_5_uid3 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(22 downto 0);
          OZB : in std_logic;
          O : out  std_logic_vector(4 downto 0)   );
end entity;

architecture arch of LZOC_23_5_uid3 is
signal sozb, sozb_d1, sozb_d2, sozb_d3 : std_logic;
signal level5, level5_d1 :  std_logic_vector(31 downto 0);
signal digit5, digit5_d1, digit5_d2, digit5_d3 : std_logic;
signal level4, level4_d1 :  std_logic_vector(15 downto 0);
signal digit4, digit4_d1, digit4_d2 : std_logic;
signal level3 :  std_logic_vector(7 downto 0);
signal digit3, digit3_d1 : std_logic;
signal level2, level2_d1 :  std_logic_vector(3 downto 0);
signal digit2 : std_logic;
signal level1 :  std_logic_vector(1 downto 0);
signal digit1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            sozb_d1 <=  sozb;
            sozb_d2 <=  sozb_d1;
            sozb_d3 <=  sozb_d2;
            level5_d1 <=  level5;
            digit5_d1 <=  digit5;
            digit5_d2 <=  digit5_d1;
            digit5_d3 <=  digit5_d2;
            level4_d1 <=  level4;
            digit4_d1 <=  digit4;
            digit4_d2 <=  digit4_d1;
            digit3_d1 <=  digit3;
            level2_d1 <=  level2;
         end if;
      end process;
   sozb <= OZB;
   level5<= I& (8 downto 0 => not(sozb));
   digit5<= '1' when level5(31 downto 16) = (31 downto 16 => sozb) else '0';
   ----------------Synchro barrier, entering cycle 1----------------
   level4<= level5_d1(15 downto 0) when digit5_d1='1' else level5_d1(31 downto 16);
   digit4<= '1' when level4(15 downto 8) = (15 downto 8 => sozb_d1) else '0';
   ----------------Synchro barrier, entering cycle 2----------------
   level3<= level4_d1(7 downto 0) when digit4_d1='1' else level4_d1(15 downto 8);
   digit3<= '1' when level3(7 downto 4) = (7 downto 4 => sozb_d2) else '0';
   level2<= level3(3 downto 0) when digit3='1' else level3(7 downto 4);
   ----------------Synchro barrier, entering cycle 3----------------
   digit2<= '1' when level2_d1(3 downto 2) = (3 downto 2 => sozb_d3) else '0';
   level1<= level2_d1(1 downto 0) when digit2='1' else level2_d1(3 downto 2);
   digit1<= '1' when level1(1 downto 1) = (1 downto 1 => sozb_d3) else '0';
   O <= digit5_d3 & digit4_d2 & digit3_d1 & digit2 & digit1;
end architecture;

--------------------------------------------------------------------------------
--                       LeftShifter_13_by_max_13_uid6
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_13_by_max_13_uid6 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(12 downto 0);
          S : in  std_logic_vector(3 downto 0);
          R : out  std_logic_vector(25 downto 0)   );
end entity;

architecture arch of LeftShifter_13_by_max_13_uid6 is
signal level0 :  std_logic_vector(12 downto 0);
signal ps :  std_logic_vector(3 downto 0);
signal level1 :  std_logic_vector(13 downto 0);
signal level2 :  std_logic_vector(15 downto 0);
signal level3 :  std_logic_vector(19 downto 0);
signal level4 :  std_logic_vector(27 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps(3)= '1' else     (7 downto 0 => '0') & level3;
   R <= level4(25 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                               InvTable_0_6_7
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity InvTable_0_6_7 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(6 downto 0)   );
end entity;

architecture arch of InvTable_0_6_7 is
begin
  with X select  Y <= 
   "1000000" when "000000",
   "1000000" when "000001",
   "0111111" when "000010",
   "0111110" when "000011",
   "0111101" when "000100",
   "0111100" when "000101",
   "0111011" when "000110",
   "0111010" when "000111",
   "0111001" when "001000",
   "0111001" when "001001",
   "0111000" when "001010",
   "0110111" when "001011",
   "0110110" when "001100",
   "0110110" when "001101",
   "0110101" when "001110",
   "0110100" when "001111",
   "0110100" when "010000",
   "0110011" when "010001",
   "0110010" when "010010",
   "0110010" when "010011",
   "0110001" when "010100",
   "0110001" when "010101",
   "0110000" when "010110",
   "0110000" when "010111",
   "0101111" when "011000",
   "0101111" when "011001",
   "0101110" when "011010",
   "0101110" when "011011",
   "0101101" when "011100",
   "0101101" when "011101",
   "0101100" when "011110",
   "0101100" when "011111",
   "1010110" when "100000",
   "1010101" when "100001",
   "1010100" when "100010",
   "1010011" when "100011",
   "1010010" when "100100",
   "1010010" when "100101",
   "1010001" when "100110",
   "1010000" when "100111",
   "1001111" when "101000",
   "1001111" when "101001",
   "1001110" when "101010",
   "1001101" when "101011",
   "1001100" when "101100",
   "1001100" when "101101",
   "1001011" when "101110",
   "1001010" when "101111",
   "1001010" when "110000",
   "1001001" when "110001",
   "1001000" when "110010",
   "1001000" when "110011",
   "1000111" when "110100",
   "1000111" when "110101",
   "1000110" when "110110",
   "1000101" when "110111",
   "1000101" when "111000",
   "1000100" when "111001",
   "1000100" when "111010",
   "1000011" when "111011",
   "1000011" when "111100",
   "1000010" when "111101",
   "1000010" when "111110",
   "1000001" when "111111",
   "-------" when others;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_31_f400_uid22
--                     (IntAdderClassical_31_f400_uid24)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_31_f400_uid22 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(30 downto 0);
          Y : in  std_logic_vector(30 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(30 downto 0)   );
end entity;

architecture arch of IntAdder_31_f400_uid22 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                IntMultiplier_UsingDSP_4_26_0_unsigned_uid11
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_4_26_0_unsigned_uid11 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(3 downto 0);
          Y : in  std_logic_vector(25 downto 0);
          R : out  std_logic_vector(29 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_4_26_0_unsigned_uid11 is
   component IntAdder_31_f400_uid22 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(30 downto 0);
             Y : in  std_logic_vector(30 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(30 downto 0)   );
   end component;

signal XX_m12 :  std_logic_vector(25 downto 0);
signal YY_m12 :  std_logic_vector(3 downto 0);
signal DSP_bh13_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh13_w29_0, heap_bh13_w29_0_d1 : std_logic;
signal heap_bh13_w28_0, heap_bh13_w28_0_d1 : std_logic;
signal heap_bh13_w27_0, heap_bh13_w27_0_d1 : std_logic;
signal heap_bh13_w26_0, heap_bh13_w26_0_d1 : std_logic;
signal heap_bh13_w25_0, heap_bh13_w25_0_d1 : std_logic;
signal heap_bh13_w24_0, heap_bh13_w24_0_d1 : std_logic;
signal heap_bh13_w23_0, heap_bh13_w23_0_d1 : std_logic;
signal heap_bh13_w22_0, heap_bh13_w22_0_d1 : std_logic;
signal heap_bh13_w21_0, heap_bh13_w21_0_d1 : std_logic;
signal heap_bh13_w20_0, heap_bh13_w20_0_d1 : std_logic;
signal heap_bh13_w19_0, heap_bh13_w19_0_d1 : std_logic;
signal heap_bh13_w18_0, heap_bh13_w18_0_d1 : std_logic;
signal heap_bh13_w17_0, heap_bh13_w17_0_d1 : std_logic;
signal heap_bh13_w16_0, heap_bh13_w16_0_d1 : std_logic;
signal heap_bh13_w15_0, heap_bh13_w15_0_d1 : std_logic;
signal heap_bh13_w14_0, heap_bh13_w14_0_d1 : std_logic;
signal heap_bh13_w13_0, heap_bh13_w13_0_d1 : std_logic;
signal heap_bh13_w12_0, heap_bh13_w12_0_d1 : std_logic;
signal heap_bh13_w11_0, heap_bh13_w11_0_d1 : std_logic;
signal heap_bh13_w10_0, heap_bh13_w10_0_d1 : std_logic;
signal heap_bh13_w9_0, heap_bh13_w9_0_d1 : std_logic;
signal heap_bh13_w8_0, heap_bh13_w8_0_d1 : std_logic;
signal heap_bh13_w7_0, heap_bh13_w7_0_d1 : std_logic;
signal heap_bh13_w6_0, heap_bh13_w6_0_d1 : std_logic;
signal heap_bh13_w5_0, heap_bh13_w5_0_d1 : std_logic;
signal heap_bh13_w4_0, heap_bh13_w4_0_d1 : std_logic;
signal heap_bh13_w3_0, heap_bh13_w3_0_d1 : std_logic;
signal heap_bh13_w2_0, heap_bh13_w2_0_d1 : std_logic;
signal heap_bh13_w1_0, heap_bh13_w1_0_d1 : std_logic;
signal heap_bh13_w0_0, heap_bh13_w0_0_d1 : std_logic;
signal DSP_bh13_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh13_w5_1, heap_bh13_w5_1_d1 : std_logic;
signal heap_bh13_w4_1, heap_bh13_w4_1_d1 : std_logic;
signal heap_bh13_w3_1, heap_bh13_w3_1_d1 : std_logic;
signal heap_bh13_w2_1, heap_bh13_w2_1_d1 : std_logic;
signal heap_bh13_w1_1, heap_bh13_w1_1_d1 : std_logic;
signal heap_bh13_w0_1, heap_bh13_w0_1_d1 : std_logic;
signal finalAdderIn0_bh13 :  std_logic_vector(30 downto 0);
signal finalAdderIn1_bh13 :  std_logic_vector(30 downto 0);
signal finalAdderCin_bh13 : std_logic;
signal finalAdderOut_bh13 :  std_logic_vector(30 downto 0);
signal CompressionResult13 :  std_logic_vector(30 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh13_w29_0_d1 <=  heap_bh13_w29_0;
            heap_bh13_w28_0_d1 <=  heap_bh13_w28_0;
            heap_bh13_w27_0_d1 <=  heap_bh13_w27_0;
            heap_bh13_w26_0_d1 <=  heap_bh13_w26_0;
            heap_bh13_w25_0_d1 <=  heap_bh13_w25_0;
            heap_bh13_w24_0_d1 <=  heap_bh13_w24_0;
            heap_bh13_w23_0_d1 <=  heap_bh13_w23_0;
            heap_bh13_w22_0_d1 <=  heap_bh13_w22_0;
            heap_bh13_w21_0_d1 <=  heap_bh13_w21_0;
            heap_bh13_w20_0_d1 <=  heap_bh13_w20_0;
            heap_bh13_w19_0_d1 <=  heap_bh13_w19_0;
            heap_bh13_w18_0_d1 <=  heap_bh13_w18_0;
            heap_bh13_w17_0_d1 <=  heap_bh13_w17_0;
            heap_bh13_w16_0_d1 <=  heap_bh13_w16_0;
            heap_bh13_w15_0_d1 <=  heap_bh13_w15_0;
            heap_bh13_w14_0_d1 <=  heap_bh13_w14_0;
            heap_bh13_w13_0_d1 <=  heap_bh13_w13_0;
            heap_bh13_w12_0_d1 <=  heap_bh13_w12_0;
            heap_bh13_w11_0_d1 <=  heap_bh13_w11_0;
            heap_bh13_w10_0_d1 <=  heap_bh13_w10_0;
            heap_bh13_w9_0_d1 <=  heap_bh13_w9_0;
            heap_bh13_w8_0_d1 <=  heap_bh13_w8_0;
            heap_bh13_w7_0_d1 <=  heap_bh13_w7_0;
            heap_bh13_w6_0_d1 <=  heap_bh13_w6_0;
            heap_bh13_w5_0_d1 <=  heap_bh13_w5_0;
            heap_bh13_w4_0_d1 <=  heap_bh13_w4_0;
            heap_bh13_w3_0_d1 <=  heap_bh13_w3_0;
            heap_bh13_w2_0_d1 <=  heap_bh13_w2_0;
            heap_bh13_w1_0_d1 <=  heap_bh13_w1_0;
            heap_bh13_w0_0_d1 <=  heap_bh13_w0_0;
            heap_bh13_w5_1_d1 <=  heap_bh13_w5_1;
            heap_bh13_w4_1_d1 <=  heap_bh13_w4_1;
            heap_bh13_w3_1_d1 <=  heap_bh13_w3_1;
            heap_bh13_w2_1_d1 <=  heap_bh13_w2_1;
            heap_bh13_w1_1_d1 <=  heap_bh13_w1_1;
            heap_bh13_w0_1_d1 <=  heap_bh13_w0_1;
         end if;
      end process;
   XX_m12 <= Y ;
   YY_m12 <= X ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh13_ch0_0 <= ("" & XX_m12(25 downto 2) & "") * ("" & YY_m12(3 downto 0) & "0000000000000");
   heap_bh13_w29_0 <= DSP_bh13_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w28_0 <= DSP_bh13_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w27_0 <= DSP_bh13_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w26_0 <= DSP_bh13_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w25_0 <= DSP_bh13_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w24_0 <= DSP_bh13_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w23_0 <= DSP_bh13_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w22_0 <= DSP_bh13_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w21_0 <= DSP_bh13_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w20_0 <= DSP_bh13_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w19_0 <= DSP_bh13_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w18_0 <= DSP_bh13_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w17_0 <= DSP_bh13_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w16_0 <= DSP_bh13_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w15_0 <= DSP_bh13_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w14_0 <= DSP_bh13_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w13_0 <= DSP_bh13_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w12_0 <= DSP_bh13_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w11_0 <= DSP_bh13_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w10_0 <= DSP_bh13_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w9_0 <= DSP_bh13_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w8_0 <= DSP_bh13_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w7_0 <= DSP_bh13_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w6_0 <= DSP_bh13_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w5_0 <= DSP_bh13_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w4_0 <= DSP_bh13_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w3_0 <= DSP_bh13_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w2_0 <= DSP_bh13_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w1_0 <= DSP_bh13_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w0_0 <= DSP_bh13_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh13_ch1_0 <= ("" & XX_m12(1 downto 0) & "0000000000000000000000") * ("" & YY_m12(3 downto 0) & "0000000000000");
   heap_bh13_w5_1 <= DSP_bh13_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w4_1 <= DSP_bh13_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w3_1 <= DSP_bh13_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w2_1 <= DSP_bh13_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w1_1 <= DSP_bh13_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh13_w0_1 <= DSP_bh13_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh13 <= "0" & heap_bh13_w29_0_d1 & heap_bh13_w28_0_d1 & heap_bh13_w27_0_d1 & heap_bh13_w26_0_d1 & heap_bh13_w25_0_d1 & heap_bh13_w24_0_d1 & heap_bh13_w23_0_d1 & heap_bh13_w22_0_d1 & heap_bh13_w21_0_d1 & heap_bh13_w20_0_d1 & heap_bh13_w19_0_d1 & heap_bh13_w18_0_d1 & heap_bh13_w17_0_d1 & heap_bh13_w16_0_d1 & heap_bh13_w15_0_d1 & heap_bh13_w14_0_d1 & heap_bh13_w13_0_d1 & heap_bh13_w12_0_d1 & heap_bh13_w11_0_d1 & heap_bh13_w10_0_d1 & heap_bh13_w9_0_d1 & heap_bh13_w8_0_d1 & heap_bh13_w7_0_d1 & heap_bh13_w6_0_d1 & heap_bh13_w5_1_d1 & heap_bh13_w4_1_d1 & heap_bh13_w3_1_d1 & heap_bh13_w2_1_d1 & heap_bh13_w1_1_d1 & heap_bh13_w0_1_d1;
   finalAdderIn1_bh13 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh13_w5_0_d1 & heap_bh13_w4_0_d1 & heap_bh13_w3_0_d1 & heap_bh13_w2_0_d1 & heap_bh13_w1_0_d1 & heap_bh13_w0_0_d1;
   finalAdderCin_bh13 <= '0';
   Adder_final13_0: IntAdder_31_f400_uid22  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh13,
                 R => finalAdderOut_bh13   ,
                 X => finalAdderIn0_bh13,
                 Y => finalAdderIn1_bh13);
   -- concatenate all the compressed chunks
   CompressionResult13 <= finalAdderOut_bh13;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult13(29 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_32_f400_uid30
--                     (IntAdderClassical_32_f400_uid32)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_32_f400_uid30 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntAdder_32_f400_uid30 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_32_f400_uid37
--                     (IntAdderClassical_32_f400_uid39)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_32_f400_uid37 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntAdder_32_f400_uid37 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_32_f400_uid55
--                     (IntAdderClassical_32_f400_uid57)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_32_f400_uid55 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntAdder_32_f400_uid55 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                IntMultiplier_UsingDSP_6_25_0_unsigned_uid44
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_6_25_0_unsigned_uid44 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : in  std_logic_vector(24 downto 0);
          R : out  std_logic_vector(30 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_6_25_0_unsigned_uid44 is
   component IntAdder_32_f400_uid55 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(31 downto 0);
             Y : in  std_logic_vector(31 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(31 downto 0)   );
   end component;

signal XX_m45 :  std_logic_vector(24 downto 0);
signal YY_m45 :  std_logic_vector(5 downto 0);
signal DSP_bh46_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh46_w30_0, heap_bh46_w30_0_d1 : std_logic;
signal heap_bh46_w29_0, heap_bh46_w29_0_d1 : std_logic;
signal heap_bh46_w28_0, heap_bh46_w28_0_d1 : std_logic;
signal heap_bh46_w27_0, heap_bh46_w27_0_d1 : std_logic;
signal heap_bh46_w26_0, heap_bh46_w26_0_d1 : std_logic;
signal heap_bh46_w25_0, heap_bh46_w25_0_d1 : std_logic;
signal heap_bh46_w24_0, heap_bh46_w24_0_d1 : std_logic;
signal heap_bh46_w23_0, heap_bh46_w23_0_d1 : std_logic;
signal heap_bh46_w22_0, heap_bh46_w22_0_d1 : std_logic;
signal heap_bh46_w21_0, heap_bh46_w21_0_d1 : std_logic;
signal heap_bh46_w20_0, heap_bh46_w20_0_d1 : std_logic;
signal heap_bh46_w19_0, heap_bh46_w19_0_d1 : std_logic;
signal heap_bh46_w18_0, heap_bh46_w18_0_d1 : std_logic;
signal heap_bh46_w17_0, heap_bh46_w17_0_d1 : std_logic;
signal heap_bh46_w16_0, heap_bh46_w16_0_d1 : std_logic;
signal heap_bh46_w15_0, heap_bh46_w15_0_d1 : std_logic;
signal heap_bh46_w14_0, heap_bh46_w14_0_d1 : std_logic;
signal heap_bh46_w13_0, heap_bh46_w13_0_d1 : std_logic;
signal heap_bh46_w12_0, heap_bh46_w12_0_d1 : std_logic;
signal heap_bh46_w11_0, heap_bh46_w11_0_d1 : std_logic;
signal heap_bh46_w10_0, heap_bh46_w10_0_d1 : std_logic;
signal heap_bh46_w9_0, heap_bh46_w9_0_d1 : std_logic;
signal heap_bh46_w8_0, heap_bh46_w8_0_d1 : std_logic;
signal heap_bh46_w7_0, heap_bh46_w7_0_d1 : std_logic;
signal heap_bh46_w6_0, heap_bh46_w6_0_d1 : std_logic;
signal heap_bh46_w5_0, heap_bh46_w5_0_d1 : std_logic;
signal heap_bh46_w4_0, heap_bh46_w4_0_d1 : std_logic;
signal heap_bh46_w3_0, heap_bh46_w3_0_d1 : std_logic;
signal heap_bh46_w2_0, heap_bh46_w2_0_d1 : std_logic;
signal heap_bh46_w1_0, heap_bh46_w1_0_d1 : std_logic;
signal heap_bh46_w0_0, heap_bh46_w0_0_d1 : std_logic;
signal DSP_bh46_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh46_w6_1, heap_bh46_w6_1_d1 : std_logic;
signal heap_bh46_w5_1, heap_bh46_w5_1_d1 : std_logic;
signal heap_bh46_w4_1, heap_bh46_w4_1_d1 : std_logic;
signal heap_bh46_w3_1, heap_bh46_w3_1_d1 : std_logic;
signal heap_bh46_w2_1, heap_bh46_w2_1_d1 : std_logic;
signal heap_bh46_w1_1, heap_bh46_w1_1_d1 : std_logic;
signal heap_bh46_w0_1, heap_bh46_w0_1_d1 : std_logic;
signal finalAdderIn0_bh46 :  std_logic_vector(31 downto 0);
signal finalAdderIn1_bh46 :  std_logic_vector(31 downto 0);
signal finalAdderCin_bh46 : std_logic;
signal finalAdderOut_bh46 :  std_logic_vector(31 downto 0);
signal CompressionResult46 :  std_logic_vector(31 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh46_w30_0_d1 <=  heap_bh46_w30_0;
            heap_bh46_w29_0_d1 <=  heap_bh46_w29_0;
            heap_bh46_w28_0_d1 <=  heap_bh46_w28_0;
            heap_bh46_w27_0_d1 <=  heap_bh46_w27_0;
            heap_bh46_w26_0_d1 <=  heap_bh46_w26_0;
            heap_bh46_w25_0_d1 <=  heap_bh46_w25_0;
            heap_bh46_w24_0_d1 <=  heap_bh46_w24_0;
            heap_bh46_w23_0_d1 <=  heap_bh46_w23_0;
            heap_bh46_w22_0_d1 <=  heap_bh46_w22_0;
            heap_bh46_w21_0_d1 <=  heap_bh46_w21_0;
            heap_bh46_w20_0_d1 <=  heap_bh46_w20_0;
            heap_bh46_w19_0_d1 <=  heap_bh46_w19_0;
            heap_bh46_w18_0_d1 <=  heap_bh46_w18_0;
            heap_bh46_w17_0_d1 <=  heap_bh46_w17_0;
            heap_bh46_w16_0_d1 <=  heap_bh46_w16_0;
            heap_bh46_w15_0_d1 <=  heap_bh46_w15_0;
            heap_bh46_w14_0_d1 <=  heap_bh46_w14_0;
            heap_bh46_w13_0_d1 <=  heap_bh46_w13_0;
            heap_bh46_w12_0_d1 <=  heap_bh46_w12_0;
            heap_bh46_w11_0_d1 <=  heap_bh46_w11_0;
            heap_bh46_w10_0_d1 <=  heap_bh46_w10_0;
            heap_bh46_w9_0_d1 <=  heap_bh46_w9_0;
            heap_bh46_w8_0_d1 <=  heap_bh46_w8_0;
            heap_bh46_w7_0_d1 <=  heap_bh46_w7_0;
            heap_bh46_w6_0_d1 <=  heap_bh46_w6_0;
            heap_bh46_w5_0_d1 <=  heap_bh46_w5_0;
            heap_bh46_w4_0_d1 <=  heap_bh46_w4_0;
            heap_bh46_w3_0_d1 <=  heap_bh46_w3_0;
            heap_bh46_w2_0_d1 <=  heap_bh46_w2_0;
            heap_bh46_w1_0_d1 <=  heap_bh46_w1_0;
            heap_bh46_w0_0_d1 <=  heap_bh46_w0_0;
            heap_bh46_w6_1_d1 <=  heap_bh46_w6_1;
            heap_bh46_w5_1_d1 <=  heap_bh46_w5_1;
            heap_bh46_w4_1_d1 <=  heap_bh46_w4_1;
            heap_bh46_w3_1_d1 <=  heap_bh46_w3_1;
            heap_bh46_w2_1_d1 <=  heap_bh46_w2_1;
            heap_bh46_w1_1_d1 <=  heap_bh46_w1_1;
            heap_bh46_w0_1_d1 <=  heap_bh46_w0_1;
         end if;
      end process;
   XX_m45 <= Y ;
   YY_m45 <= X ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh46_ch0_0 <= ("" & XX_m45(24 downto 1) & "") * ("" & YY_m45(5 downto 0) & "00000000000");
   heap_bh46_w30_0 <= DSP_bh46_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w29_0 <= DSP_bh46_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w28_0 <= DSP_bh46_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w27_0 <= DSP_bh46_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w26_0 <= DSP_bh46_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w25_0 <= DSP_bh46_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w24_0 <= DSP_bh46_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w23_0 <= DSP_bh46_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w22_0 <= DSP_bh46_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w21_0 <= DSP_bh46_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w20_0 <= DSP_bh46_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w19_0 <= DSP_bh46_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w18_0 <= DSP_bh46_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w17_0 <= DSP_bh46_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w16_0 <= DSP_bh46_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w15_0 <= DSP_bh46_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w14_0 <= DSP_bh46_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w13_0 <= DSP_bh46_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w12_0 <= DSP_bh46_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w11_0 <= DSP_bh46_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w10_0 <= DSP_bh46_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w9_0 <= DSP_bh46_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w8_0 <= DSP_bh46_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w7_0 <= DSP_bh46_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w6_0 <= DSP_bh46_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w5_0 <= DSP_bh46_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w4_0 <= DSP_bh46_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w3_0 <= DSP_bh46_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w2_0 <= DSP_bh46_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w1_0 <= DSP_bh46_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w0_0 <= DSP_bh46_ch0_0(10); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh46_ch1_0 <= ("" & XX_m45(0 downto 0) & "00000000000000000000000") * ("" & YY_m45(5 downto 0) & "00000000000");
   heap_bh46_w6_1 <= DSP_bh46_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w5_1 <= DSP_bh46_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w4_1 <= DSP_bh46_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w3_1 <= DSP_bh46_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w2_1 <= DSP_bh46_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w1_1 <= DSP_bh46_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh46_w0_1 <= DSP_bh46_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh46 <= "0" & heap_bh46_w30_0_d1 & heap_bh46_w29_0_d1 & heap_bh46_w28_0_d1 & heap_bh46_w27_0_d1 & heap_bh46_w26_0_d1 & heap_bh46_w25_0_d1 & heap_bh46_w24_0_d1 & heap_bh46_w23_0_d1 & heap_bh46_w22_0_d1 & heap_bh46_w21_0_d1 & heap_bh46_w20_0_d1 & heap_bh46_w19_0_d1 & heap_bh46_w18_0_d1 & heap_bh46_w17_0_d1 & heap_bh46_w16_0_d1 & heap_bh46_w15_0_d1 & heap_bh46_w14_0_d1 & heap_bh46_w13_0_d1 & heap_bh46_w12_0_d1 & heap_bh46_w11_0_d1 & heap_bh46_w10_0_d1 & heap_bh46_w9_0_d1 & heap_bh46_w8_0_d1 & heap_bh46_w7_0_d1 & heap_bh46_w6_1_d1 & heap_bh46_w5_1_d1 & heap_bh46_w4_1_d1 & heap_bh46_w3_1_d1 & heap_bh46_w2_1_d1 & heap_bh46_w1_1_d1 & heap_bh46_w0_1_d1;
   finalAdderIn1_bh46 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh46_w6_0_d1 & heap_bh46_w5_0_d1 & heap_bh46_w4_0_d1 & heap_bh46_w3_0_d1 & heap_bh46_w2_0_d1 & heap_bh46_w1_0_d1 & heap_bh46_w0_0_d1;
   finalAdderCin_bh46 <= '0';
   Adder_final46_0: IntAdder_32_f400_uid55  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh46,
                 R => finalAdderOut_bh46   ,
                 X => finalAdderIn0_bh46,
                 Y => finalAdderIn1_bh46);
   -- concatenate all the compressed chunks
   CompressionResult46 <= finalAdderOut_bh46;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult46(30 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_27_f400_uid63
--                     (IntAdderClassical_27_f400_uid65)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_27_f400_uid63 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(26 downto 0);
          Y : in  std_logic_vector(26 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(26 downto 0)   );
end entity;

architecture arch of IntAdder_27_f400_uid63 is
signal X_d1 :  std_logic_vector(26 downto 0);
signal Y_d1 :  std_logic_vector(26 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_27_f400_uid70
--                    (IntAdderAlternative_27_f400_uid74)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_27_f400_uid70 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(26 downto 0);
          Y : in  std_logic_vector(26 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(26 downto 0)   );
end entity;

architecture arch of IntAdder_27_f400_uid70 is
signal s_sum_l0_idx0 :  std_logic_vector(23 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(4 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(22 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(3 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(4 downto 0);
signal sum_l1_idx1 :  std_logic_vector(3 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(22 downto 0)) + ( "0" & Y(22 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(26 downto 23)) + ( "0" & Y(26 downto 23));
   sum_l0_idx0 <= s_sum_l0_idx0(22 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(23 downto 23);
   sum_l0_idx1 <= s_sum_l0_idx1(3 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(4 downto 4);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(3 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(4 downto 4);
   R <= sum_l1_idx1(3 downto 0) & sum_l0_idx0_d1(22 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                            IntSquarer_17_uid77
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca (2009)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library work;
entity IntSquarer_17_uid77 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(16 downto 0);
          R : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of IntSquarer_17_uid77 is
signal sX, sX_d1 :  std_logic_vector(16 downto 0);
signal sY, sY_d1 :  std_logic_vector(16 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            sX_d1 <=  sX;
            sY_d1 <=  sY;
         end if;
      end process;
   sX <= X;
   sY <= X;
   ----------------Synchro barrier, entering cycle 1----------------
   R <= sX_d1 * sY_d1;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_27_f400_uid80
--                    (IntAdderAlternative_27_f400_uid84)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_27_f400_uid80 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(26 downto 0);
          Y : in  std_logic_vector(26 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(26 downto 0)   );
end entity;

architecture arch of IntAdder_27_f400_uid80 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_0_6_39
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_0_6_39 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of LogTable_0_6_39 is
begin
  with X select  Y <= 
   "111111101111110000000000000000000000000" when "000000",
   "111111101111110000000000000000000000000" when "000001",
   "000000110000010000010101100101100010010" when "000010",
   "000001110001110010101110110001001111010" when "000011",
   "000010110100011001010101000010100101000" when "000100",
   "000011111000000110011000101101011001111" when "000101",
   "000100111100111100010001010111010010000" when "000110",
   "000110000010111101011110010111010101101" when "000111",
   "000111001010001100100111011000111000010" when "001000",
   "000111001010001100100111011000111000010" when "001001",
   "001000010010101100011101000001000101000" when "001010",
   "001001011100011111111001011000000010110" when "001011",
   "001010100111101010000000110101101010100" when "001100",
   "001010100111101010000000110101101010100" when "001101",
   "001011110100001110000010110010101010010" when "001110",
   "001101000010001111011010011110010001011" when "001111",
   "001101000010001111011010011110010001011" when "010000",
   "001110010001110001101111111001001100101" when "010001",
   "001111100010111000111000110110010110100" when "010010",
   "001111100010111000111000110110010110100" when "010011",
   "010000110101101000111010000010001010000" when "010100",
   "010000110101101000111010000010001010000" when "010101",
   "010010001010000110001000010001001101010" when "010110",
   "010010001010000110001000010001001101010" when "010111",
   "010011100000010101001001110111001100110" when "011000",
   "010011100000010101001001110111001100110" when "011001",
   "010100111000011010111000000111001110010" when "011010",
   "010100111000011010111000000111001110010" when "011011",
   "010110010010011100100000111110100111001" when "011100",
   "010110010010011100100000111110100111001" when "011101",
   "010111101110011111101000111011110110000" when "011110",
   "010111101110011111101000111011110110000" when "011111",
   "101100110101100001110101000101000111110" when "100000",
   "101101100101011011111000000100001100001" when "100001",
   "101110010101111010001101010100010101000" when "100010",
   "101111000110111101101100011101100011001" when "100011",
   "101111111000100111001111001001010100010" when "100100",
   "101111111000100111001111001001010100010" when "100101",
   "110000101010110111110001001000111010101" when "100110",
   "110001011101110000010000011100001100110" when "100111",
   "110010010001010001101101011000101111111" when "101000",
   "110010010001010001101101011000101111111" when "101001",
   "110011000101011101001010110001100001100" when "101010",
   "110011111010010011101101111110111101111" when "101011",
   "110100101111110110011111000111101011000" when "101100",
   "110100101111110110011111000111101011000" when "101101",
   "110101100110000110101001001001100110100" when "101110",
   "110110011101000101011010000011111100000" when "101111",
   "110110011101000101011010000011111100000" when "110000",
   "110111010100110100000011000001100110000" when "110001",
   "111000001101010011111000100100011101010" when "110010",
   "111000001101010011111000100100011101010" when "110011",
   "111001000110100110010010110001011011011" when "110100",
   "111001000110100110010010110001011011011" when "110101",
   "111010000000101100101101011101010001101" when "110110",
   "111010111011101000101000011010011110010" when "110111",
   "111010111011101000101000011010011110010" when "111000",
   "111011110111011011100111100111111111100" when "111001",
   "111011110111011011100111100111111111100" when "111010",
   "111100110100000111010011100001001110011" when "111011",
   "111100110100000111010011100001001110011" when "111100",
   "111101110001101101011001001111000110001" when "111101",
   "111101110001101101011001001111000110001" when "111110",
   "111110110000001111101010111010011110000" when "111111",
   "---------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_1_4_35
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_1_4_35 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(3 downto 0);
          Y : out  std_logic_vector(34 downto 0)   );
end entity;

architecture arch of LogTable_1_4_35 is
begin
  with X select  Y <= 
   "00001000000000011111111101010101101" when "0000",
   "00011000000000100000000010101010111" when "0001",
   "00101000000100100001001000010100011" when "0010",
   "00111000001100100101001111110010110" when "0011",
   "01001000011000101110011100001001100" when "0100",
   "01011000101000111110110001111111110" when "0101",
   "01101000111101011000010111100001101" when "0110",
   "01111001010101111101010100100011000" when "0111",
   "10000001100011110100101110110000010" when "1000",
   "10010010000010101110110001001111010" when "1001",
   "10100010100101111001100101111100011" when "1010",
   "10110011001101010111011010100001011" when "1011",
   "11000011111001001010011110010110101" when "1100",
   "11010100101001010101000010100101000" when "1101",
   "11100101011101111001011010000111111" when "1110",
   "11110110010110111001111001101110111" when "1111",
   "-----------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_39_f400_uid103
--                     (IntAdderClassical_39_f400_uid105)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_39_f400_uid103 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(38 downto 0);
          Y : in  std_logic_vector(38 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of IntAdder_39_f400_uid103 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_2_6_32
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_2_6_32 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of LogTable_2_6_32 is
begin
  with X select  Y <= 
   "00000000000000000000010000000000" when "000000",
   "00000100000000000000010000000000" when "000001",
   "00001000000000000010010000000001" when "000010",
   "00001100000000000110010000000101" when "000011",
   "00010000000000001100010000001110" when "000100",
   "00010100000000010100010000011110" when "000101",
   "00011000000000011110010000111000" when "000110",
   "00011100000000101010010001011100" when "000111",
   "00100000000000111000010010001101" when "001000",
   "00100100000001001000010011001101" when "001001",
   "00101000000001011010010100011110" when "001010",
   "00101100000001101110010110000010" when "001011",
   "00110000000010000100010111111100" when "001100",
   "00110100000010011100011010001100" when "001101",
   "00111000000010110110011100110101" when "001110",
   "00111100000011010010011111111010" when "001111",
   "01000000000011110000100011011011" when "010000",
   "01000100000100010000100111011100" when "010001",
   "01001000000100110010101011111110" when "010010",
   "01001100000101010110110001000010" when "010011",
   "01010000000101111100110110101100" when "010100",
   "01010100000110100100111100111101" when "010101",
   "01011000000111001111000011111000" when "010110",
   "01011100000111111011001011011100" when "010111",
   "01100000001000101001010011110000" when "011000",
   "01100100001001011001011100110001" when "011001",
   "01101000001010001011100110100100" when "011010",
   "01101100001010111111110001001010" when "011011",
   "01110000001011110101111100100110" when "011100",
   "01110100001100101110001000111000" when "011101",
   "01111000001101101000010110000100" when "011110",
   "01111100001110100100100100001100" when "011111",
   "10000000001111100010110011010001" when "100000",
   "10000100010000100011000011010101" when "100001",
   "10001000010001100101010100011010" when "100010",
   "10001100010010101001100110100011" when "100011",
   "10010000010011101111111001110010" when "100100",
   "10010100010100111000001110000111" when "100101",
   "10011000010110000010100011100111" when "100110",
   "10011100010111001110111010010010" when "100111",
   "10100000011000011101010010001010" when "101000",
   "10100100011001101101101011010010" when "101001",
   "10101000011011000000000101101011" when "101010",
   "10101100011100010100100001011001" when "101011",
   "10110000011101101010111110011011" when "101100",
   "10110100011111000011011100110110" when "101101",
   "10111000100000011101111100101010" when "101110",
   "10111100100001111010011101111010" when "101111",
   "11000000100011011001000000101000" when "110000",
   "11000100100100111001100100110110" when "110001",
   "11001000100110011100001010100101" when "110010",
   "11001100101000000000110001111001" when "110011",
   "11010000101001100111011010110010" when "110100",
   "11010100101011010000000101010011" when "110101",
   "11011000101100111010110001011111" when "110110",
   "11011100101110100111011111010110" when "110111",
   "11100000110000010110001110111100" when "111000",
   "11100100110010000111000000010001" when "111001",
   "11101000110011111001110011011001" when "111010",
   "11101100110101101110101000010101" when "111011",
   "11110000110111100101011111001000" when "111100",
   "11110100111001011110010111110010" when "111101",
   "11111000111011011001010010010111" when "111110",
   "11111100111101010110001110111001" when "111111",
   "--------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_39_f400_uid112
--                     (IntAdderClassical_39_f400_uid114)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_39_f400_uid112 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(38 downto 0);
          Y : in  std_logic_vector(38 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of IntAdder_39_f400_uid112 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_39_f400_uid119
--                     (IntAdderClassical_39_f400_uid121)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_39_f400_uid119 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(38 downto 0);
          Y : in  std_logic_vector(38 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of IntAdder_39_f400_uid119 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                        KCMTable_6_93032640_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity KCMTable_6_93032640_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(32 downto 0)   );
end entity;

architecture arch of KCMTable_6_93032640_unsigned is
begin
  with X select  Y <= 
   "000000000000000000000000000000000" when "000000",
   "000000101100010111001000011000000" when "000001",
   "000001011000101110010000110000000" when "000010",
   "000010000101000101011001001000000" when "000011",
   "000010110001011100100001100000000" when "000100",
   "000011011101110011101001111000000" when "000101",
   "000100001010001010110010010000000" when "000110",
   "000100110110100001111010101000000" when "000111",
   "000101100010111001000011000000000" when "001000",
   "000110001111010000001011011000000" when "001001",
   "000110111011100111010011110000000" when "001010",
   "000111100111111110011100001000000" when "001011",
   "001000010100010101100100100000000" when "001100",
   "001001000000101100101100111000000" when "001101",
   "001001101101000011110101010000000" when "001110",
   "001010011001011010111101101000000" when "001111",
   "001011000101110010000110000000000" when "010000",
   "001011110010001001001110011000000" when "010001",
   "001100011110100000010110110000000" when "010010",
   "001101001010110111011111001000000" when "010011",
   "001101110111001110100111100000000" when "010100",
   "001110100011100101101111111000000" when "010101",
   "001111001111111100111000010000000" when "010110",
   "001111111100010100000000101000000" when "010111",
   "010000101000101011001001000000000" when "011000",
   "010001010101000010010001011000000" when "011001",
   "010010000001011001011001110000000" when "011010",
   "010010101101110000100010001000000" when "011011",
   "010011011010000111101010100000000" when "011100",
   "010100000110011110110010111000000" when "011101",
   "010100110010110101111011010000000" when "011110",
   "010101011111001101000011101000000" when "011111",
   "010110001011100100001100000000000" when "100000",
   "010110110111111011010100011000000" when "100001",
   "010111100100010010011100110000000" when "100010",
   "011000010000101001100101001000000" when "100011",
   "011000111101000000101101100000000" when "100100",
   "011001101001010111110101111000000" when "100101",
   "011010010101101110111110010000000" when "100110",
   "011011000010000110000110101000000" when "100111",
   "011011101110011101001111000000000" when "101000",
   "011100011010110100010111011000000" when "101001",
   "011101000111001011011111110000000" when "101010",
   "011101110011100010101000001000000" when "101011",
   "011110011111111001110000100000000" when "101100",
   "011111001100010000111000111000000" when "101101",
   "011111111000101000000001010000000" when "101110",
   "100000100100111111001001101000000" when "101111",
   "100001010001010110010010000000000" when "110000",
   "100001111101101101011010011000000" when "110001",
   "100010101010000100100010110000000" when "110010",
   "100011010110011011101011001000000" when "110011",
   "100100000010110010110011100000000" when "110100",
   "100100101111001001111011111000000" when "110101",
   "100101011011100001000100010000000" when "110110",
   "100110000111111000001100101000000" when "110111",
   "100110110100001111010101000000000" when "111000",
   "100111100000100110011101011000000" when "111001",
   "101000001100111101100101110000000" when "111010",
   "101000111001010100101110001000000" when "111011",
   "101001100101101011110110100000000" when "111100",
   "101010010010000010111110111000000" when "111101",
   "101010111110011010000111010000000" when "111110",
   "101011101010110001001111101000000" when "111111",
   "---------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                        KCMTable_2_93032640_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity KCMTable_2_93032640_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(28 downto 0)   );
end entity;

architecture arch of KCMTable_2_93032640_unsigned is
begin
  with X select  Y <= 
   "00000000000000000000000000000" when "00",
   "00101100010111001000011000000" when "01",
   "01011000101110010000110000000" when "10",
   "10000101000101011001001000000" when "11",
   "-----------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_29_f400_uid131
--                    (IntAdderAlternative_29_f400_uid135)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_29_f400_uid131 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(28 downto 0);
          Y : in  std_logic_vector(28 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(28 downto 0)   );
end entity;

architecture arch of IntAdder_29_f400_uid131 is
signal s_sum_l0_idx0 :  std_logic_vector(26 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(3 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(25 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(2 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(3 downto 0);
signal sum_l1_idx1 :  std_logic_vector(2 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(25 downto 0)) + ( "0" & Y(25 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(28 downto 26)) + ( "0" & Y(28 downto 26));
   sum_l0_idx0 <= s_sum_l0_idx0(25 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(26 downto 26);
   sum_l0_idx1 <= s_sum_l0_idx1(2 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(3 downto 3);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(2 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(3 downto 3);
   R <= sum_l1_idx1(2 downto 0) & sum_l0_idx0_d1(25 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                       IntIntKCM_8_93032640_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2009,2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntIntKCM_8_93032640_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          R : out  std_logic_vector(34 downto 0)   );
end entity;

architecture arch of IntIntKCM_8_93032640_unsigned is
   component IntAdder_29_f400_uid131 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(28 downto 0);
             Y : in  std_logic_vector(28 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(28 downto 0)   );
   end component;

   component KCMTable_2_93032640_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(28 downto 0)   );
   end component;

   component KCMTable_6_93032640_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(32 downto 0)   );
   end component;

signal d0 :  std_logic_vector(5 downto 0);
signal pp0, pp0_d1 :  std_logic_vector(32 downto 0);
signal d1 :  std_logic_vector(1 downto 0);
signal pp1 :  std_logic_vector(28 downto 0);
signal addOp0 :  std_logic_vector(28 downto 0);
signal addOp1 :  std_logic_vector(28 downto 0);
signal OutRes :  std_logic_vector(28 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of KCMTable_2_93032640_unsigned: component is "yes";
attribute rom_extract of KCMTable_6_93032640_unsigned: component is "yes";
attribute rom_style of KCMTable_2_93032640_unsigned: component is "distributed";
attribute rom_style of KCMTable_6_93032640_unsigned: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            pp0_d1 <=  pp0;
         end if;
      end process;
   d0 <= X(5 downto 0);
   KCMTable_0: KCMTable_6_93032640_unsigned  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0,
                 Y => pp0);
   d1 <= X(7 downto 6);
   KCMTable_1: KCMTable_2_93032640_unsigned  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1,
                 Y => pp1);
   addOp0 <= (28 downto 27 => '0') & pp0(32 downto 6) & "";
   addOp1 <= pp1(28 downto 0) & "";
   Result_Adder: IntAdder_29_f400_uid131  -- pipelineDepth=1 maxInDelay=1.2196e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => OutRes,
                 X => addOp0,
                 Y => addOp1);
   ----------------Synchro barrier, entering cycle 1----------------
   R <= OutRes & pp0_d1(5 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_47_f400_uid139
--                    (IntAdderAlternative_47_f400_uid143)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_47_f400_uid139 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(46 downto 0);
          Y : in  std_logic_vector(46 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(46 downto 0)   );
end entity;

architecture arch of IntAdder_47_f400_uid139 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(5 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(4 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(5 downto 0);
signal sum_l1_idx1 :  std_logic_vector(4 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(46 downto 42)) + ( "0" & Y(46 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(4 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(5 downto 5);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(4 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(5 downto 5);
   R <= sum_l1_idx1(4 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                   LZCShifter_47_to_39_counting_64_uid146
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Bogdan Pasca (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 6 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZCShifter_47_to_39_counting_64_uid146 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(46 downto 0);
          Count : out  std_logic_vector(5 downto 0);
          O : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of LZCShifter_47_to_39_counting_64_uid146 is
signal level6, level6_d1 :  std_logic_vector(46 downto 0);
signal count5, count5_d1, count5_d2, count5_d3, count5_d4, count5_d5 : std_logic;
signal level5, level5_d1 :  std_logic_vector(46 downto 0);
signal count4, count4_d1, count4_d2, count4_d3, count4_d4 : std_logic;
signal level4, level4_d1 :  std_logic_vector(46 downto 0);
signal count3, count3_d1, count3_d2, count3_d3 : std_logic;
signal level3, level3_d1 :  std_logic_vector(45 downto 0);
signal count2, count2_d1, count2_d2 : std_logic;
signal level2, level2_d1 :  std_logic_vector(41 downto 0);
signal count1, count1_d1 : std_logic;
signal level1, level1_d1 :  std_logic_vector(39 downto 0);
signal count0 : std_logic;
signal level0 :  std_logic_vector(38 downto 0);
signal sCount :  std_logic_vector(5 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level6_d1 <=  level6;
            count5_d1 <=  count5;
            count5_d2 <=  count5_d1;
            count5_d3 <=  count5_d2;
            count5_d4 <=  count5_d3;
            count5_d5 <=  count5_d4;
            level5_d1 <=  level5;
            count4_d1 <=  count4;
            count4_d2 <=  count4_d1;
            count4_d3 <=  count4_d2;
            count4_d4 <=  count4_d3;
            level4_d1 <=  level4;
            count3_d1 <=  count3;
            count3_d2 <=  count3_d1;
            count3_d3 <=  count3_d2;
            level3_d1 <=  level3;
            count2_d1 <=  count2;
            count2_d2 <=  count2_d1;
            level2_d1 <=  level2;
            count1_d1 <=  count1;
            level1_d1 <=  level1;
         end if;
      end process;
   level6 <= I ;
   ----------------Synchro barrier, entering cycle 1----------------
   count5<= '1' when level6_d1(46 downto 15) = (46 downto 15=>'0') else '0';
   level5<= level6_d1(46 downto 0) when count5='0' else level6_d1(14 downto 0) & (31 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 2----------------
   count4<= '1' when level5_d1(46 downto 31) = (46 downto 31=>'0') else '0';
   level4<= level5_d1(46 downto 0) when count4='0' else level5_d1(30 downto 0) & (15 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 3----------------
   count3<= '1' when level4_d1(46 downto 39) = (46 downto 39=>'0') else '0';
   level3<= level4_d1(46 downto 1) when count3='0' else level4_d1(38 downto 0) & (6 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 4----------------
   count2<= '1' when level3_d1(45 downto 42) = (45 downto 42=>'0') else '0';
   level2<= level3_d1(45 downto 4) when count2='0' else level3_d1(41 downto 0);

   ----------------Synchro barrier, entering cycle 5----------------
   count1<= '1' when level2_d1(41 downto 40) = (41 downto 40=>'0') else '0';
   level1<= level2_d1(41 downto 2) when count1='0' else level2_d1(39 downto 0);

   ----------------Synchro barrier, entering cycle 6----------------
   count0<= '1' when level1_d1(39 downto 39) = (39 downto 39=>'0') else '0';
   level0<= level1_d1(39 downto 1) when count0='0' else level1_d1(38 downto 0);

   O <= level0;
   sCount <= count5_d5 & count4_d4 & count3_d3 & count2_d2 & count1_d1 & count0;
   Count <= sCount;
end architecture;

--------------------------------------------------------------------------------
--                      RightShifter_17_by_max_16_uid149
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity RightShifter_17_by_max_16_uid149 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(16 downto 0);
          S : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(32 downto 0)   );
end entity;

architecture arch of RightShifter_17_by_max_16_uid149 is
signal level0, level0_d1 :  std_logic_vector(16 downto 0);
signal ps, ps_d1 :  std_logic_vector(4 downto 0);
signal level1 :  std_logic_vector(17 downto 0);
signal level2 :  std_logic_vector(19 downto 0);
signal level3 :  std_logic_vector(23 downto 0);
signal level4 :  std_logic_vector(31 downto 0);
signal level5 :  std_logic_vector(47 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level0_d1 <=  level0;
            ps_d1 <=  ps;
         end if;
      end process;
   level0<= X;
   ps<= S;
   ----------------Synchro barrier, entering cycle 1----------------
   level1<=  (0 downto 0 => '0') & level0_d1 when ps_d1(0) = '1' else    level0_d1 & (0 downto 0 => '0');
   level2<=  (1 downto 0 => '0') & level1 when ps_d1(1) = '1' else    level1 & (1 downto 0 => '0');
   level3<=  (3 downto 0 => '0') & level2 when ps_d1(2) = '1' else    level2 & (3 downto 0 => '0');
   level4<=  (7 downto 0 => '0') & level3 when ps_d1(3) = '1' else    level3 & (7 downto 0 => '0');
   level5<=  (15 downto 0 => '0') & level4 when ps_d1(4) = '1' else    level4 & (15 downto 0 => '0');
   R <= level5(47 downto 15);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_29_f400_uid152
--                     (IntAdderClassical_29_f400_uid154)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_29_f400_uid152 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(28 downto 0);
          Y : in  std_logic_vector(28 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(28 downto 0)   );
end entity;

architecture arch of IntAdder_29_f400_uid152 is
signal X_d1 :  std_logic_vector(28 downto 0);
signal Y_d1 :  std_logic_vector(28 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_31_f400_uid159
--                    (IntAdderAlternative_31_f400_uid163)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_31_f400_uid159 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(30 downto 0);
          Y : in  std_logic_vector(30 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(30 downto 0)   );
end entity;

architecture arch of IntAdder_31_f400_uid159 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                              FPLog_8_23_9_400
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, C. Klein  (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 20 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPLog_8_23_9_400 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8+23+2 downto 0);
          R : out  std_logic_vector(8+23+2 downto 0)   );
end entity;

architecture arch of FPLog_8_23_9_400 is
   component IntAdder_27_f400_uid63 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(26 downto 0);
             Y : in  std_logic_vector(26 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(26 downto 0)   );
   end component;

   component IntAdder_27_f400_uid70 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(26 downto 0);
             Y : in  std_logic_vector(26 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(26 downto 0)   );
   end component;

   component IntAdder_27_f400_uid80 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(26 downto 0);
             Y : in  std_logic_vector(26 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(26 downto 0)   );
   end component;

   component IntAdder_29_f400_uid152 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(28 downto 0);
             Y : in  std_logic_vector(28 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(28 downto 0)   );
   end component;

   component IntAdder_31_f400_uid159 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(30 downto 0);
             Y : in  std_logic_vector(30 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(30 downto 0)   );
   end component;

   component IntAdder_32_f400_uid30 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(31 downto 0);
             Y : in  std_logic_vector(31 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(31 downto 0)   );
   end component;

   component IntAdder_32_f400_uid37 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(31 downto 0);
             Y : in  std_logic_vector(31 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(31 downto 0)   );
   end component;

   component IntAdder_39_f400_uid103 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(38 downto 0);
             Y : in  std_logic_vector(38 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(38 downto 0)   );
   end component;

   component IntAdder_39_f400_uid112 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(38 downto 0);
             Y : in  std_logic_vector(38 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(38 downto 0)   );
   end component;

   component IntAdder_39_f400_uid119 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(38 downto 0);
             Y : in  std_logic_vector(38 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(38 downto 0)   );
   end component;

   component IntAdder_47_f400_uid139 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(46 downto 0);
             Y : in  std_logic_vector(46 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(46 downto 0)   );
   end component;

   component IntIntKCM_8_93032640_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             R : out  std_logic_vector(34 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_4_26_0_unsigned_uid11 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(3 downto 0);
             Y : in  std_logic_vector(25 downto 0);
             R : out  std_logic_vector(29 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_6_25_0_unsigned_uid44 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : in  std_logic_vector(24 downto 0);
             R : out  std_logic_vector(30 downto 0)   );
   end component;

   component IntSquarer_17_uid77 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(16 downto 0);
             R : out  std_logic_vector(33 downto 0)   );
   end component;

   component InvTable_0_6_7 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(6 downto 0)   );
   end component;

   component LZCShifter_47_to_39_counting_64_uid146 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(46 downto 0);
             Count : out  std_logic_vector(5 downto 0);
             O : out  std_logic_vector(38 downto 0)   );
   end component;

   component LZOC_23_5_uid3 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(22 downto 0);
             OZB : in std_logic;
             O : out  std_logic_vector(4 downto 0)   );
   end component;

   component LeftShifter_13_by_max_13_uid6 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(12 downto 0);
             S : in  std_logic_vector(3 downto 0);
             R : out  std_logic_vector(25 downto 0)   );
   end component;

   component LogTable_0_6_39 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(38 downto 0)   );
   end component;

   component LogTable_1_4_35 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(3 downto 0);
             Y : out  std_logic_vector(34 downto 0)   );
   end component;

   component LogTable_2_6_32 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(31 downto 0)   );
   end component;

   component RightShifter_17_by_max_16_uid149 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(16 downto 0);
             S : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(32 downto 0)   );
   end component;

signal XExnSgn, XExnSgn_d1, XExnSgn_d2, XExnSgn_d3, XExnSgn_d4, XExnSgn_d5, XExnSgn_d6, XExnSgn_d7, XExnSgn_d8, XExnSgn_d9, XExnSgn_d10, XExnSgn_d11, XExnSgn_d12, XExnSgn_d13, XExnSgn_d14, XExnSgn_d15, XExnSgn_d16, XExnSgn_d17, XExnSgn_d18, XExnSgn_d19, XExnSgn_d20 :  std_logic_vector(2 downto 0);
signal FirstBit : std_logic;
signal Y0, Y0_d1, Y0_d2, Y0_d3 :  std_logic_vector(24 downto 0);
signal Y0h :  std_logic_vector(22 downto 0);
signal sR, sR_d1, sR_d2, sR_d3, sR_d4, sR_d5, sR_d6, sR_d7, sR_d8, sR_d9, sR_d10, sR_d11, sR_d12, sR_d13, sR_d14, sR_d15, sR_d16, sR_d17, sR_d18, sR_d19, sR_d20 : std_logic;
signal absZ0, absZ0_d1, absZ0_d2, absZ0_d3, absZ0_d4 :  std_logic_vector(12 downto 0);
signal E, E_d1 :  std_logic_vector(7 downto 0);
signal absE, absE_d1, absE_d2, absE_d3, absE_d4, absE_d5, absE_d6, absE_d7, absE_d8 :  std_logic_vector(7 downto 0);
signal EeqZero, EeqZero_d1, EeqZero_d2, EeqZero_d3 : std_logic;
signal lzo, lzo_d1, lzo_d2, lzo_d3, lzo_d4, lzo_d5, lzo_d6, lzo_d7, lzo_d8, lzo_d9, lzo_d10, lzo_d11, lzo_d12 :  std_logic_vector(4 downto 0);
signal pfinal_s :  std_logic_vector(4 downto 0);
signal shiftval :  std_logic_vector(5 downto 0);
signal shiftvalinL :  std_logic_vector(3 downto 0);
signal shiftvalinR, shiftvalinR_d1, shiftvalinR_d2, shiftvalinR_d3, shiftvalinR_d4, shiftvalinR_d5, shiftvalinR_d6, shiftvalinR_d7, shiftvalinR_d8 :  std_logic_vector(4 downto 0);
signal doRR, doRR_d1, doRR_d2, doRR_d3 : std_logic;
signal small, small_d1, small_d2, small_d3, small_d4, small_d5, small_d6, small_d7, small_d8, small_d9, small_d10, small_d11, small_d12, small_d13, small_d14, small_d15, small_d16 : std_logic;
signal small_absZ0_normd_full :  std_logic_vector(25 downto 0);
signal small_absZ0_normd, small_absZ0_normd_d1, small_absZ0_normd_d2, small_absZ0_normd_d3, small_absZ0_normd_d4, small_absZ0_normd_d5, small_absZ0_normd_d6, small_absZ0_normd_d7, small_absZ0_normd_d8, small_absZ0_normd_d9 :  std_logic_vector(12 downto 0);
signal A0, A0_d1, A0_d2, A0_d3 :  std_logic_vector(5 downto 0);
signal InvA0, InvA0_d1 :  std_logic_vector(6 downto 0);
signal P0, P0_d1 :  std_logic_vector(31 downto 0);
signal Z1 :  std_logic_vector(25 downto 0);
signal A1 :  std_logic_vector(3 downto 0);
signal B1 :  std_logic_vector(21 downto 0);
signal ZM1 :  std_logic_vector(25 downto 0);
signal P1 :  std_logic_vector(29 downto 0);
signal Y1 :  std_logic_vector(30 downto 0);
signal EiY1 :  std_logic_vector(31 downto 0);
signal addXIter1 :  std_logic_vector(31 downto 0);
signal EiYPB1, EiYPB1_d1 :  std_logic_vector(31 downto 0);
signal Pp1 :  std_logic_vector(31 downto 0);
signal Z2 :  std_logic_vector(31 downto 0);
signal A2, A2_d1, A2_d2 :  std_logic_vector(5 downto 0);
signal B2 :  std_logic_vector(25 downto 0);
signal ZM2 :  std_logic_vector(24 downto 0);
signal P2 :  std_logic_vector(30 downto 0);
signal Y2 :  std_logic_vector(39 downto 0);
signal EiY2 :  std_logic_vector(26 downto 0);
signal addXIter2 :  std_logic_vector(26 downto 0);
signal EiYPB2 :  std_logic_vector(26 downto 0);
signal Pp2 :  std_logic_vector(26 downto 0);
signal Z3 :  std_logic_vector(26 downto 0);
signal Zfinal, Zfinal_d1, Zfinal_d2 :  std_logic_vector(26 downto 0);
signal squarerIn :  std_logic_vector(16 downto 0);
signal Z2o2_full, Z2o2_full_d1 :  std_logic_vector(33 downto 0);
signal Z2o2_full_dummy, Z2o2_full_dummy_d1, Z2o2_full_dummy_d2, Z2o2_full_dummy_d3 :  std_logic_vector(33 downto 0);
signal Z2o2_normal :  std_logic_vector(13 downto 0);
signal addFinalLog1pY :  std_logic_vector(26 downto 0);
signal Log1p_normal, Log1p_normal_d1 :  std_logic_vector(26 downto 0);
signal L0 :  std_logic_vector(38 downto 0);
signal S1, S1_d1, S1_d2 :  std_logic_vector(38 downto 0);
signal L1 :  std_logic_vector(34 downto 0);
signal sopX1, sopX1_d1, sopX1_d2 :  std_logic_vector(38 downto 0);
signal S2, S2_d1, S2_d2, S2_d3 :  std_logic_vector(38 downto 0);
signal L2 :  std_logic_vector(31 downto 0);
signal sopX2, sopX2_d1, sopX2_d2 :  std_logic_vector(38 downto 0);
signal S3, S3_d1 :  std_logic_vector(38 downto 0);
signal almostLog :  std_logic_vector(38 downto 0);
signal adderLogF_normalY :  std_logic_vector(38 downto 0);
signal LogF_normal :  std_logic_vector(38 downto 0);
signal absELog2 :  std_logic_vector(34 downto 0);
signal absELog2_pad :  std_logic_vector(46 downto 0);
signal LogF_normal_pad, LogF_normal_pad_d1 :  std_logic_vector(46 downto 0);
signal lnaddX, lnaddX_d1 :  std_logic_vector(46 downto 0);
signal lnaddY :  std_logic_vector(46 downto 0);
signal Log_normal :  std_logic_vector(46 downto 0);
signal E_normal, E_normal_d1 :  std_logic_vector(5 downto 0);
signal Log_normal_normd, Log_normal_normd_d1, Log_normal_normd_d2 :  std_logic_vector(38 downto 0);
signal Z2o2_small_bs :  std_logic_vector(16 downto 0);
signal Z2o2_small_s :  std_logic_vector(32 downto 0);
signal Z2o2_small :  std_logic_vector(28 downto 0);
signal Z_small :  std_logic_vector(28 downto 0);
signal Log_smallY :  std_logic_vector(28 downto 0);
signal nsRCin : std_logic;
signal Log_small, Log_small_d1 :  std_logic_vector(28 downto 0);
signal E0_sub, E0_sub_d1 :  std_logic_vector(1 downto 0);
signal ufl, ufl_d1, ufl_d2, ufl_d3, ufl_d4, ufl_d5, ufl_d6 : std_logic;
signal E_small, E_small_d1, E_small_d2, E_small_d3, E_small_d4 :  std_logic_vector(7 downto 0);
signal Log_small_normd, Log_small_normd_d1, Log_small_normd_d2, Log_small_normd_d3, Log_small_normd_d4, Log_small_normd_d5 :  std_logic_vector(26 downto 0);
signal E0offset :  std_logic_vector(7 downto 0);
signal ER :  std_logic_vector(7 downto 0);
signal Log_g, Log_g_d1 :  std_logic_vector(26 downto 0);
signal round, round_d1 : std_logic;
signal fraX :  std_logic_vector(30 downto 0);
signal fraY :  std_logic_vector(30 downto 0);
signal EFR, EFR_d1 :  std_logic_vector(30 downto 0);
constant g: positive := 4;
constant log2wF: positive := 5;
constant pfinal: positive := 12;
constant sfinal: positive := 27;
constant targetprec: positive := 39;
constant wE: positive := 8;
constant wF: positive := 23;
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of InvTable_0_6_7: component is "yes";
attribute rom_style of InvTable_0_6_7: component is "block";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            XExnSgn_d1 <=  XExnSgn;
            XExnSgn_d2 <=  XExnSgn_d1;
            XExnSgn_d3 <=  XExnSgn_d2;
            XExnSgn_d4 <=  XExnSgn_d3;
            XExnSgn_d5 <=  XExnSgn_d4;
            XExnSgn_d6 <=  XExnSgn_d5;
            XExnSgn_d7 <=  XExnSgn_d6;
            XExnSgn_d8 <=  XExnSgn_d7;
            XExnSgn_d9 <=  XExnSgn_d8;
            XExnSgn_d10 <=  XExnSgn_d9;
            XExnSgn_d11 <=  XExnSgn_d10;
            XExnSgn_d12 <=  XExnSgn_d11;
            XExnSgn_d13 <=  XExnSgn_d12;
            XExnSgn_d14 <=  XExnSgn_d13;
            XExnSgn_d15 <=  XExnSgn_d14;
            XExnSgn_d16 <=  XExnSgn_d15;
            XExnSgn_d17 <=  XExnSgn_d16;
            XExnSgn_d18 <=  XExnSgn_d17;
            XExnSgn_d19 <=  XExnSgn_d18;
            XExnSgn_d20 <=  XExnSgn_d19;
            Y0_d1 <=  Y0;
            Y0_d2 <=  Y0_d1;
            Y0_d3 <=  Y0_d2;
            sR_d1 <=  sR;
            sR_d2 <=  sR_d1;
            sR_d3 <=  sR_d2;
            sR_d4 <=  sR_d3;
            sR_d5 <=  sR_d4;
            sR_d6 <=  sR_d5;
            sR_d7 <=  sR_d6;
            sR_d8 <=  sR_d7;
            sR_d9 <=  sR_d8;
            sR_d10 <=  sR_d9;
            sR_d11 <=  sR_d10;
            sR_d12 <=  sR_d11;
            sR_d13 <=  sR_d12;
            sR_d14 <=  sR_d13;
            sR_d15 <=  sR_d14;
            sR_d16 <=  sR_d15;
            sR_d17 <=  sR_d16;
            sR_d18 <=  sR_d17;
            sR_d19 <=  sR_d18;
            sR_d20 <=  sR_d19;
            absZ0_d1 <=  absZ0;
            absZ0_d2 <=  absZ0_d1;
            absZ0_d3 <=  absZ0_d2;
            absZ0_d4 <=  absZ0_d3;
            E_d1 <=  E;
            absE_d1 <=  absE;
            absE_d2 <=  absE_d1;
            absE_d3 <=  absE_d2;
            absE_d4 <=  absE_d3;
            absE_d5 <=  absE_d4;
            absE_d6 <=  absE_d5;
            absE_d7 <=  absE_d6;
            absE_d8 <=  absE_d7;
            EeqZero_d1 <=  EeqZero;
            EeqZero_d2 <=  EeqZero_d1;
            EeqZero_d3 <=  EeqZero_d2;
            lzo_d1 <=  lzo;
            lzo_d2 <=  lzo_d1;
            lzo_d3 <=  lzo_d2;
            lzo_d4 <=  lzo_d3;
            lzo_d5 <=  lzo_d4;
            lzo_d6 <=  lzo_d5;
            lzo_d7 <=  lzo_d6;
            lzo_d8 <=  lzo_d7;
            lzo_d9 <=  lzo_d8;
            lzo_d10 <=  lzo_d9;
            lzo_d11 <=  lzo_d10;
            lzo_d12 <=  lzo_d11;
            shiftvalinR_d1 <=  shiftvalinR;
            shiftvalinR_d2 <=  shiftvalinR_d1;
            shiftvalinR_d3 <=  shiftvalinR_d2;
            shiftvalinR_d4 <=  shiftvalinR_d3;
            shiftvalinR_d5 <=  shiftvalinR_d4;
            shiftvalinR_d6 <=  shiftvalinR_d5;
            shiftvalinR_d7 <=  shiftvalinR_d6;
            shiftvalinR_d8 <=  shiftvalinR_d7;
            doRR_d1 <=  doRR;
            doRR_d2 <=  doRR_d1;
            doRR_d3 <=  doRR_d2;
            small_d1 <=  small;
            small_d2 <=  small_d1;
            small_d3 <=  small_d2;
            small_d4 <=  small_d3;
            small_d5 <=  small_d4;
            small_d6 <=  small_d5;
            small_d7 <=  small_d6;
            small_d8 <=  small_d7;
            small_d9 <=  small_d8;
            small_d10 <=  small_d9;
            small_d11 <=  small_d10;
            small_d12 <=  small_d11;
            small_d13 <=  small_d12;
            small_d14 <=  small_d13;
            small_d15 <=  small_d14;
            small_d16 <=  small_d15;
            small_absZ0_normd_d1 <=  small_absZ0_normd;
            small_absZ0_normd_d2 <=  small_absZ0_normd_d1;
            small_absZ0_normd_d3 <=  small_absZ0_normd_d2;
            small_absZ0_normd_d4 <=  small_absZ0_normd_d3;
            small_absZ0_normd_d5 <=  small_absZ0_normd_d4;
            small_absZ0_normd_d6 <=  small_absZ0_normd_d5;
            small_absZ0_normd_d7 <=  small_absZ0_normd_d6;
            small_absZ0_normd_d8 <=  small_absZ0_normd_d7;
            small_absZ0_normd_d9 <=  small_absZ0_normd_d8;
            A0_d1 <=  A0;
            A0_d2 <=  A0_d1;
            A0_d3 <=  A0_d2;
            InvA0_d1 <=  InvA0;
            P0_d1 <=  P0;
            EiYPB1_d1 <=  EiYPB1;
            A2_d1 <=  A2;
            A2_d2 <=  A2_d1;
            Zfinal_d1 <=  Zfinal;
            Zfinal_d2 <=  Zfinal_d1;
            Z2o2_full_d1 <=  Z2o2_full;
            Z2o2_full_dummy_d1 <=  Z2o2_full_dummy;
            Z2o2_full_dummy_d2 <=  Z2o2_full_dummy_d1;
            Z2o2_full_dummy_d3 <=  Z2o2_full_dummy_d2;
            Log1p_normal_d1 <=  Log1p_normal;
            S1_d1 <=  S1;
            S1_d2 <=  S1_d1;
            sopX1_d1 <=  sopX1;
            sopX1_d2 <=  sopX1_d1;
            S2_d1 <=  S2;
            S2_d2 <=  S2_d1;
            S2_d3 <=  S2_d2;
            sopX2_d1 <=  sopX2;
            sopX2_d2 <=  sopX2_d1;
            S3_d1 <=  S3;
            LogF_normal_pad_d1 <=  LogF_normal_pad;
            lnaddX_d1 <=  lnaddX;
            E_normal_d1 <=  E_normal;
            Log_normal_normd_d1 <=  Log_normal_normd;
            Log_normal_normd_d2 <=  Log_normal_normd_d1;
            Log_small_d1 <=  Log_small;
            E0_sub_d1 <=  E0_sub;
            ufl_d1 <=  ufl;
            ufl_d2 <=  ufl_d1;
            ufl_d3 <=  ufl_d2;
            ufl_d4 <=  ufl_d3;
            ufl_d5 <=  ufl_d4;
            ufl_d6 <=  ufl_d5;
            E_small_d1 <=  E_small;
            E_small_d2 <=  E_small_d1;
            E_small_d3 <=  E_small_d2;
            E_small_d4 <=  E_small_d3;
            Log_small_normd_d1 <=  Log_small_normd;
            Log_small_normd_d2 <=  Log_small_normd_d1;
            Log_small_normd_d3 <=  Log_small_normd_d2;
            Log_small_normd_d4 <=  Log_small_normd_d3;
            Log_small_normd_d5 <=  Log_small_normd_d4;
            Log_g_d1 <=  Log_g;
            round_d1 <=  round;
            EFR_d1 <=  EFR;
         end if;
      end process;
   XExnSgn <=  X(wE+wF+2 downto wE+wF);
   FirstBit <=  X(wF-1);
   Y0 <= "1" & X(wF-1 downto 0) & "0" when FirstBit = '0' else "01" & X(wF-1 downto 0);
   Y0h <= Y0(wF downto 1);
   -- Sign of the result;
   sR <= '0'   when  (X(wE+wF-1 downto wF) = ('0' & (wE-2 downto 0 => '1')))  -- binade [1..2)
     else not X(wE+wF-1);                -- MSB of exponent
   absZ0 <=   Y0(wF-pfinal+1 downto 0)          when (sR='0') else
             ((wF-pfinal+1 downto 0 => '0') - Y0(wF-pfinal+1 downto 0));
   E <= (X(wE+wF-1 downto wF)) - ("0" & (wE-2 downto 1 => '1') & (not FirstBit));
   ----------------Synchro barrier, entering cycle 1----------------
   absE <= ((wE-1 downto 0 => '0') - E_d1)   when sR_d1 = '1' else E_d1;
   EeqZero <= '1' when E_d1=(wE-1 downto 0 => '0') else '0';
   ---------------- cycle 0----------------
   lzoc1: LZOC_23_5_uid3  -- pipelineDepth=3 maxInDelay=5.46e-10
      port map ( clk  => clk,
                 rst  => rst,
                 I => Y0h,
                 O => lzo,
                 OZB => FirstBit);
   ---------------- cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   pfinal_s <= "01100";
   shiftval <= ('0' & lzo_d1) - ('0' & pfinal_s); 
   shiftvalinL <= shiftval(3 downto 0);
   shiftvalinR <= shiftval(4 downto 0);
   doRR <= shiftval(log2wF); -- sign of the result
   small <= EeqZero_d3 and not(doRR);
   ---------------- cycle 4----------------
   -- The left shifter for the 'small' case
   small_lshift: LeftShifter_13_by_max_13_uid6  -- pipelineDepth=0 maxInDelay=1.25072e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => small_absZ0_normd_full,
                 S => shiftvalinL,
                 X => absZ0_d4);
   small_absZ0_normd <= small_absZ0_normd_full(12 downto 0); -- get rid of leading zeroes
   ----------------Synchro barrier, entering cycle 0----------------
   ---------------- The range reduction box ---------------
   A0 <= X(22 downto 17);
   ----------------Synchro barrier, entering cycle 1----------------
   -- First inv table
   itO: InvTable_0_6_7  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A0_d1,
                 Y => InvA0);
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   P0 <= InvA0_d1 * Y0_d3;

   ----------------Synchro barrier, entering cycle 4----------------
   Z1 <= P0_d1(25 downto 0);

   A1 <= Z1(25 downto 22);
   B1 <= Z1(21 downto 0);
   ZM1 <= Z1;
   p1_mult: IntMultiplier_UsingDSP_4_26_0_unsigned_uid11  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => P1,
                 X => A1,
                 Y => ZM1);

   ----------------Synchro barrier, entering cycle 5----------------
    -- delay at multiplier output is 0
   ---------------- cycle 4----------------
   Y1 <= "1" & (3 downto 0 => '0') & Z1;
   EiY1 <= Y1 & (0 downto 0 => '0')  when A1(3) = '1'
     else  "0" & Y1;
   addXIter1 <= "0" & B1 & (8 downto 0 => '0');
   addIter1_1: IntAdder_32_f400_uid30  -- pipelineDepth=0 maxInDelay=8.6e-11
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB1,
                 X => addXIter1,
                 Y => EiY1);

   ----------------Synchro barrier, entering cycle 5----------------
   Pp1 <= (0 downto 0 => '1') & not(P1 & (0 downto 0 => '0'));
   addIter2_1: IntAdder_32_f400_uid37  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z2,
                 X => EiYPB1_d1,
                 Y => Pp1);

 -- the critical path at the adder output = 2.26672e-09

   A2 <= Z2(31 downto 26);
   B2 <= Z2(25 downto 0);
   ZM2 <= Z2(31 downto 7);
   p2_mult: IntMultiplier_UsingDSP_6_25_0_unsigned_uid44  -- pipelineDepth=1 maxInDelay=2.26672e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => P2,
                 X => A2,
                 Y => ZM2);

   ----------------Synchro barrier, entering cycle 6----------------
    -- delay at multiplier output is 0
   ---------------- cycle 5----------------
   Y2 <= "1" & (6 downto 0 => '0') & Z2;
   EiY2 <= (0 downto 0 => '0') & Y2(39 downto 14);
   addXIter2 <= "0" & B2;
   addIter1_2: IntAdder_27_f400_uid63  -- pipelineDepth=1 maxInDelay=2.26672e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB2,
                 X => addXIter2,
                 Y => EiY2);

   ----------------Synchro barrier, entering cycle 6----------------
   Pp2 <= (1 downto 0 => '1') & not(P2(30 downto 6));
   addIter2_2: IntAdder_27_f400_uid70  -- pipelineDepth=1 maxInDelay=1.289e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z3,
                 X => EiYPB2,
                 Y => Pp2);

   ----------------Synchro barrier, entering cycle 7----------------
 -- the critical path at the adder output = 7.6e-10
   Zfinal <= Z3;
   --  Synchro between RR box and case almost 1
   squarerIn <= Zfinal(sfinal-1 downto sfinal-17) when doRR_d3='1'
                    else (small_absZ0_normd_d3 & (3 downto 0 => '0'));  
   squarer: IntSquarer_17_uid77  -- pipelineDepth=1 maxInDelay=1.72672e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => Z2o2_full,
                 X => squarerIn);
   ----------------Synchro barrier, entering cycle 8----------------
   ----------------Synchro barrier, entering cycle 9----------------
   Z2o2_full_dummy <= Z2o2_full_d1;
   Z2o2_normal <= Z2o2_full_dummy (33  downto 20);
   addFinalLog1pY <= (pfinal downto 0  => '1') & not(Z2o2_normal);
   addFinalLog1p_normalAdder: IntAdder_27_f400_uid80  -- pipelineDepth=0 maxInDelay=8.8072e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Log1p_normal,
                 X => Zfinal_d2,
                 Y => addFinalLog1pY);

   -- Now the log tables, as late as possible
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 6----------------
   ----------------Synchro barrier, entering cycle 6----------------
   ----------------Synchro barrier, entering cycle 3----------------
   -- First log table
   ltO: LogTable_0_6_39  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A0_d3,
                 Y => L0);
   ----------------Synchro barrier, entering cycle 4----------------
   S1 <= L0;
   lt1: LogTable_1_4_35  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A1,
                 Y => L1);
   sopX1 <= ((38 downto 35 => '0') & L1);
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 6----------------
   adderS1: IntAdder_39_f400_uid103  -- pipelineDepth=0 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S2,
                 X => S1_d2,
                 Y => sopX1_d2);

   ----------------Synchro barrier, entering cycle 7----------------
   lt2: LogTable_2_6_32  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A2_d2,
                 Y => L2);
   sopX2 <= ((38 downto 32 => '0') & L2);
   ----------------Synchro barrier, entering cycle 8----------------
   ----------------Synchro barrier, entering cycle 9----------------
   adderS2: IntAdder_39_f400_uid112  -- pipelineDepth=0 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S3,
                 X => S2_d3,
                 Y => sopX2_d2);

   ----------------Synchro barrier, entering cycle 10----------------
   almostLog <= S3_d1;
   adderLogF_normalY <= ((targetprec-1 downto sfinal => '0') & Log1p_normal_d1);
   adderLogF_normal: IntAdder_39_f400_uid119  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => LogF_normal,
                 X => almostLog,
                 Y => adderLogF_normalY);
   ----------------Synchro barrier, entering cycle 9----------------
   Log2KCM: IntIntKCM_8_93032640_unsigned  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absELog2,
                 X => absE_d8);
   ----------------Synchro barrier, entering cycle 10----------------
   absELog2_pad <=   absELog2 & (targetprec-wF-g-1 downto 0 => '0');       
   LogF_normal_pad <= (wE-1  downto 0 => LogF_normal(targetprec-1))  & LogF_normal;
   lnaddX <= absELog2_pad;
   ----------------Synchro barrier, entering cycle 11----------------
   lnaddY <= LogF_normal_pad_d1 when sR_d11='0' else not(LogF_normal_pad_d1); 
   lnadder: IntAdder_47_f400_uid139  -- pipelineDepth=1 maxInDelay=8.6e-11
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => sR_d11,
                 R => Log_normal,
                 X => lnaddX_d1,
                 Y => lnaddY);

   ----------------Synchro barrier, entering cycle 12----------------
   final_norm: LZCShifter_47_to_39_counting_64_uid146  -- pipelineDepth=6 maxInDelay=1.22772e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Count => E_normal,
                 I => Log_normal,
                 O => Log_normal_normd);
   Z2o2_small_bs <= Z2o2_full_dummy_d3(33 downto 17);
   ao_rshift: RightShifter_17_by_max_16_uid149  -- pipelineDepth=1 maxInDelay=1.38944e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => Z2o2_small_s,
                 S => shiftvalinR_d8,
                 X => Z2o2_small_bs);
   ---------------- cycle 13----------------
   -- output delay at shifter output is 1.3492e-09
     -- send the MSB to position pfinal
   Z2o2_small <=  (pfinal-1 downto 0  => '0') & Z2o2_small_s(32 downto 16);
   -- mantissa will be either Y0-z^2/2  or  -Y0+z^2/2,  depending on sR  
   Z_small <= small_absZ0_normd_d9 & (15 downto 0 => '0');
   Log_smallY <= Z2o2_small when sR_d13='1' else not(Z2o2_small);
   nsRCin <= not ( sR_d13 );
   log_small_adder: IntAdder_29_f400_uid152  -- pipelineDepth=1 maxInDelay=2.32464e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => nsRCin,
                 R => Log_small,
                 X => Z_small,
                 Y => Log_smallY);

   ----------------Synchro barrier, entering cycle 14----------------
 -- critical path here is 1.335e-09
   -- Possibly subtract 1 or 2 to the exponent, depending on the LZC of Log_small
   E0_sub <=   "11" when Log_small(wF+g+1) = '1'
          else "10" when Log_small(wF+g+1 downto wF+g) = "01"
          else "01" ;
   -- The smallest log will be log(1+2^{-wF}) \approx 2^{-wF}  = 2^-23
   -- The smallest representable number is 2^{1-2^(wE-1)} = 2^-127
   -- No underflow possible
   ufl <= '0';
   ----------------Synchro barrier, entering cycle 15----------------
   E_small <=  ("0" & (wE-2 downto 2 => '1') & E0_sub_d1)  -  ((wE-1 downto 5 => '0') & lzo_d12) ;
   Log_small_normd <= Log_small_d1(wF+g+1 downto 2) when Log_small_d1(wF+g+1)='1'
           else Log_small_d1(wF+g downto 1)  when Log_small_d1(wF+g)='1'  -- remove the first zero
           else Log_small_d1(wF+g-1 downto 0)  ; -- remove two zeroes (extremely rare, 001000000 only)
   ----------------Synchro barrier, entering cycle 18----------------
   ----------------Synchro barrier, entering cycle 19----------------
   E0offset <= "10000110"; -- E0 + wE 
   ER <= E_small_d4(7 downto 0) when small_d15='1'
      else E0offset - ((7 downto 6 => '0') & E_normal_d1);
   ---------------- cycle 18----------------
   Log_g <=  Log_small_normd_d3(wF+g-2 downto 0) & "0" when small_d14='1'           -- remove implicit 1
      else Log_normal_normd(targetprec-2 downto targetprec-wF-g-1 );  -- remove implicit 1
   round <= Log_g(g-1) ; -- sticky is always 1 for a transcendental function 
   -- if round leads to a change of binade, the carry propagation magically updates both mantissa and exponent
   ----------------Synchro barrier, entering cycle 19----------------
   fraX <= (ER & Log_g_d1(wF+g-1 downto g)) ; 
   fraY <= ((wE+wF-1 downto 1 => '0') & round_d1); 
   finalRoundAdder: IntAdder_31_f400_uid159  -- pipelineDepth=0 maxInDelay=9.38e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => EFR,
                 X => fraX,
                 Y => fraY);
   ----------------Synchro barrier, entering cycle 20----------------
   R(wE+wF+2 downto wE+wF) <= "110" when ((XExnSgn_d20(2) and (XExnSgn_d20(1) or XExnSgn_d20(0))) or (XExnSgn_d20(1) and XExnSgn_d20(0))) = '1' else
                              "101" when XExnSgn_d20(2 downto 1) = "00"  else
                              "100" when XExnSgn_d20(2 downto 1) = "10"  else
                              "00" & sR_d20 when (((Log_normal_normd_d2(targetprec-1)='0') and (small_d16='0')) or ( (Log_small_normd_d5 (wF+g-1)='0') and (small_d16='1'))) or (ufl_d6 = '1') else
                               "01" & sR_d20;
   R(wE+wF-1 downto 0) <=  EFR_d1;
end architecture;

--------------------------------------------------------------------------------
--                          FPLog_8_23_9_400_Wrapper
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 22 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPLog_8_23_9_400_Wrapper is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          R : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of FPLog_8_23_9_400_Wrapper is
   component FPLog_8_23_9_400 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8+23+2 downto 0);
             R : out  std_logic_vector(8+23+2 downto 0)   );
   end component;

signal i_X, i_X_d1 :  std_logic_vector(33 downto 0);
signal o_R, o_R_d1 :  std_logic_vector(33 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            i_X_d1 <=  i_X;
            o_R_d1 <=  o_R;
         end if;
      end process;
   i_X <= X;
   ----------------Synchro barrier, entering cycle 1----------------
   test: FPLog_8_23_9_400  -- pipelineDepth=20 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => o_R,
                 X => i_X_d1);
   ----------------Synchro barrier, entering cycle 22----------------
   R <= o_R_d1;
end architecture;

