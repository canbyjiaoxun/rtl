--------------------------------------------------------------------------------
--                               Compressor_3_2
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_3_2 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          R : out  std_logic_vector(1 downto 0)   );
end entity;

architecture arch of Compressor_3_2 is
signal X :  std_logic_vector(2 downto 0);
begin
   X <=X0 ;
   with X select R <= 
      "00" when "000", 
      "01" when "001", 
      "01" when "010", 
      "10" when "011", 
      "01" when "100", 
      "10" when "101", 
      "10" when "110", 
      "11" when "111", 
      "--" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_23_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_23_3 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          X1 : in  std_logic_vector(1 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_23_3 is
signal X :  std_logic_vector(4 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "00000", 
      "001" when "00001", 
      "001" when "00010", 
      "010" when "00011", 
      "001" when "00100", 
      "010" when "00101", 
      "010" when "00110", 
      "011" when "00111", 
      "010" when "01000", 
      "011" when "01001", 
      "011" when "01010", 
      "100" when "01011", 
      "011" when "01100", 
      "100" when "01101", 
      "100" when "01110", 
      "101" when "01111", 
      "010" when "10000", 
      "011" when "10001", 
      "011" when "10010", 
      "100" when "10011", 
      "011" when "10100", 
      "100" when "10101", 
      "100" when "10110", 
      "101" when "10111", 
      "100" when "11000", 
      "101" when "11001", 
      "101" when "11010", 
      "110" when "11011", 
      "101" when "11100", 
      "110" when "11101", 
      "110" when "11110", 
      "111" when "11111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_13_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_13_3 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          X1 : in  std_logic_vector(0 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_13_3 is
signal X :  std_logic_vector(3 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "0000", 
      "001" when "0001", 
      "001" when "0010", 
      "010" when "0011", 
      "001" when "0100", 
      "010" when "0101", 
      "010" when "0110", 
      "011" when "0111", 
      "010" when "1000", 
      "011" when "1001", 
      "011" when "1010", 
      "100" when "1011", 
      "011" when "1100", 
      "100" when "1101", 
      "100" when "1110", 
      "101" when "1111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_32_f400_uid3
--                      (IntAdderClassical_32_f400_uid5)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_32_f400_uid3 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntAdder_32_f400_uid3 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                              LZOC_23_5_uid10
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: 
--------------------------------------------------------------------------------
-- Pipeline depth: 3 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZOC_23_5_uid10 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(22 downto 0);
          OZB : in std_logic;
          O : out  std_logic_vector(4 downto 0)   );
end entity;

architecture arch of LZOC_23_5_uid10 is
signal sozb, sozb_d1, sozb_d2, sozb_d3 : std_logic;
signal level5 :  std_logic_vector(31 downto 0);
signal digit5, digit5_d1, digit5_d2, digit5_d3 : std_logic;
signal level4, level4_d1 :  std_logic_vector(15 downto 0);
signal digit4, digit4_d1, digit4_d2 : std_logic;
signal level3, level3_d1 :  std_logic_vector(7 downto 0);
signal digit3, digit3_d1 : std_logic;
signal level2, level2_d1 :  std_logic_vector(3 downto 0);
signal digit2, digit2_d1 : std_logic;
signal level1 :  std_logic_vector(1 downto 0);
signal digit1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            sozb_d1 <=  sozb;
            sozb_d2 <=  sozb_d1;
            sozb_d3 <=  sozb_d2;
            digit5_d1 <=  digit5;
            digit5_d2 <=  digit5_d1;
            digit5_d3 <=  digit5_d2;
            level4_d1 <=  level4;
            digit4_d1 <=  digit4;
            digit4_d2 <=  digit4_d1;
            level3_d1 <=  level3;
            digit3_d1 <=  digit3;
            level2_d1 <=  level2;
            digit2_d1 <=  digit2;
         end if;
      end process;
   sozb <= OZB;
   level5<= I& (8 downto 0 => not(sozb));
   digit5<= '1' when level5(31 downto 16) = (31 downto 16 => sozb) else '0';
   level4<= level5(15 downto 0) when digit5='1' else level5(31 downto 16);
   ----------------Synchro barrier, entering cycle 1----------------
   digit4<= '1' when level4_d1(15 downto 8) = (15 downto 8 => sozb_d1) else '0';
   level3<= level4_d1(7 downto 0) when digit4='1' else level4_d1(15 downto 8);
   ----------------Synchro barrier, entering cycle 2----------------
   digit3<= '1' when level3_d1(7 downto 4) = (7 downto 4 => sozb_d2) else '0';
   level2<= level3_d1(3 downto 0) when digit3='1' else level3_d1(7 downto 4);
   digit2<= '1' when level2(3 downto 2) = (3 downto 2 => sozb_d2) else '0';
   ----------------Synchro barrier, entering cycle 3----------------
   level1<= level2_d1(1 downto 0) when digit2_d1='1' else level2_d1(3 downto 2);
   digit1<= '1' when level1(1 downto 1) = (1 downto 1 => sozb_d3) else '0';
   O <= digit5_d3 & digit4_d2 & digit3_d1 & digit2_d1 & digit1;
end architecture;

--------------------------------------------------------------------------------
--                              LZOC_33_6_uid14
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: 
--------------------------------------------------------------------------------
-- Pipeline depth: 4 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZOC_33_6_uid14 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(32 downto 0);
          OZB : in std_logic;
          O : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of LZOC_33_6_uid14 is
signal sozb, sozb_d1, sozb_d2, sozb_d3, sozb_d4 : std_logic;
signal level6, level6_d1 :  std_logic_vector(63 downto 0);
signal digit6, digit6_d1, digit6_d2, digit6_d3, digit6_d4 : std_logic;
signal level5, level5_d1 :  std_logic_vector(31 downto 0);
signal digit5, digit5_d1, digit5_d2, digit5_d3 : std_logic;
signal level4, level4_d1 :  std_logic_vector(15 downto 0);
signal digit4, digit4_d1, digit4_d2 : std_logic;
signal level3 :  std_logic_vector(7 downto 0);
signal digit3, digit3_d1 : std_logic;
signal level2, level2_d1 :  std_logic_vector(3 downto 0);
signal digit2 : std_logic;
signal level1 :  std_logic_vector(1 downto 0);
signal digit1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            sozb_d1 <=  sozb;
            sozb_d2 <=  sozb_d1;
            sozb_d3 <=  sozb_d2;
            sozb_d4 <=  sozb_d3;
            level6_d1 <=  level6;
            digit6_d1 <=  digit6;
            digit6_d2 <=  digit6_d1;
            digit6_d3 <=  digit6_d2;
            digit6_d4 <=  digit6_d3;
            level5_d1 <=  level5;
            digit5_d1 <=  digit5;
            digit5_d2 <=  digit5_d1;
            digit5_d3 <=  digit5_d2;
            level4_d1 <=  level4;
            digit4_d1 <=  digit4;
            digit4_d2 <=  digit4_d1;
            digit3_d1 <=  digit3;
            level2_d1 <=  level2;
         end if;
      end process;
   sozb <= OZB;
   level6<= I& (30 downto 0 => not(sozb));
   digit6<= '1' when level6(63 downto 32) = (63 downto 32 => sozb) else '0';
   ----------------Synchro barrier, entering cycle 1----------------
   level5<= level6_d1(31 downto 0) when digit6_d1='1' else level6_d1(63 downto 32);
   digit5<= '1' when level5(31 downto 16) = (31 downto 16 => sozb_d1) else '0';
   ----------------Synchro barrier, entering cycle 2----------------
   level4<= level5_d1(15 downto 0) when digit5_d1='1' else level5_d1(31 downto 16);
   digit4<= '1' when level4(15 downto 8) = (15 downto 8 => sozb_d2) else '0';
   ----------------Synchro barrier, entering cycle 3----------------
   level3<= level4_d1(7 downto 0) when digit4_d1='1' else level4_d1(15 downto 8);
   digit3<= '1' when level3(7 downto 4) = (7 downto 4 => sozb_d3) else '0';
   level2<= level3(3 downto 0) when digit3='1' else level3(7 downto 4);
   ----------------Synchro barrier, entering cycle 4----------------
   digit2<= '1' when level2_d1(3 downto 2) = (3 downto 2 => sozb_d4) else '0';
   level1<= level2_d1(1 downto 0) when digit2='1' else level2_d1(3 downto 2);
   digit1<= '1' when level1(1 downto 1) = (1 downto 1 => sozb_d4) else '0';
   O <= digit6_d4 & digit5_d3 & digit4_d2 & digit3_d1 & digit2 & digit1;
end architecture;

--------------------------------------------------------------------------------
--                       LeftShifter_18_by_max_18_uid17
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_18_by_max_18_uid17 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(17 downto 0);
          S : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(35 downto 0)   );
end entity;

architecture arch of LeftShifter_18_by_max_18_uid17 is
signal level0, level0_d1 :  std_logic_vector(17 downto 0);
signal ps, ps_d1 :  std_logic_vector(4 downto 0);
signal level1 :  std_logic_vector(18 downto 0);
signal level2 :  std_logic_vector(20 downto 0);
signal level3 :  std_logic_vector(24 downto 0);
signal level4 :  std_logic_vector(32 downto 0);
signal level5 :  std_logic_vector(48 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level0_d1 <=  level0;
            ps_d1 <=  ps;
         end if;
      end process;
   level0<= X;
   ps<= S;
   ----------------Synchro barrier, entering cycle 1----------------
   level1<= level0_d1 & (0 downto 0 => '0') when ps_d1(0)= '1' else     (0 downto 0 => '0') & level0_d1;
   level2<= level1 & (1 downto 0 => '0') when ps_d1(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps_d1(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps_d1(3)= '1' else     (7 downto 0 => '0') & level3;
   level5<= level4 & (15 downto 0 => '0') when ps_d1(4)= '1' else     (15 downto 0 => '0') & level4;
   R <= level5(35 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                               InvTable_0_8_9
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity InvTable_0_8_9 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          Y : out  std_logic_vector(8 downto 0)   );
end entity;

architecture arch of InvTable_0_8_9 is
   -- Build a 2-D array type for the RoM
   subtype word_t is std_logic_vector(8 downto 0);
   type memory_t is array(0 to 255) of word_t;
   function init_rom
      return memory_t is 
      variable tmp : memory_t := (
   "100000000",
   "100000000",
   "011111111",
   "011111110",
   "011111101",
   "011111100",
   "011111011",
   "011111010",
   "011111001",
   "011111000",
   "011110111",
   "011110110",
   "011110101",
   "011110100",
   "011110011",
   "011110010",
   "011110001",
   "011110001",
   "011110000",
   "011101111",
   "011101110",
   "011101101",
   "011101100",
   "011101011",
   "011101011",
   "011101010",
   "011101001",
   "011101000",
   "011100111",
   "011100110",
   "011100110",
   "011100101",
   "011100100",
   "011100011",
   "011100010",
   "011100010",
   "011100001",
   "011100000",
   "011011111",
   "011011111",
   "011011110",
   "011011101",
   "011011100",
   "011011100",
   "011011011",
   "011011010",
   "011011010",
   "011011001",
   "011011000",
   "011010111",
   "011010111",
   "011010110",
   "011010101",
   "011010101",
   "011010100",
   "011010011",
   "011010011",
   "011010010",
   "011010001",
   "011010001",
   "011010000",
   "011001111",
   "011001111",
   "011001110",
   "011001101",
   "011001101",
   "011001100",
   "011001011",
   "011001011",
   "011001010",
   "011001010",
   "011001001",
   "011001000",
   "011001000",
   "011000111",
   "011000110",
   "011000110",
   "011000101",
   "011000101",
   "011000100",
   "011000100",
   "011000011",
   "011000010",
   "011000010",
   "011000001",
   "011000001",
   "011000000",
   "011000000",
   "010111111",
   "010111110",
   "010111110",
   "010111101",
   "010111101",
   "010111100",
   "010111100",
   "010111011",
   "010111011",
   "010111010",
   "010111010",
   "010111001",
   "010111001",
   "010111000",
   "010111000",
   "010110111",
   "010110111",
   "010110110",
   "010110110",
   "010110101",
   "010110101",
   "010110100",
   "010110100",
   "010110011",
   "010110011",
   "010110010",
   "010110010",
   "010110001",
   "010110001",
   "010110000",
   "010110000",
   "010101111",
   "010101111",
   "010101110",
   "010101110",
   "010101101",
   "010101101",
   "010101101",
   "010101100",
   "010101100",
   "101010110",
   "101010101",
   "101010100",
   "101010011",
   "101010010",
   "101010001",
   "101010001",
   "101010000",
   "101001111",
   "101001110",
   "101001101",
   "101001100",
   "101001011",
   "101001011",
   "101001010",
   "101001001",
   "101001000",
   "101000111",
   "101000111",
   "101000110",
   "101000101",
   "101000100",
   "101000011",
   "101000011",
   "101000010",
   "101000001",
   "101000000",
   "100111111",
   "100111111",
   "100111110",
   "100111101",
   "100111100",
   "100111100",
   "100111011",
   "100111010",
   "100111001",
   "100111001",
   "100111000",
   "100110111",
   "100110110",
   "100110110",
   "100110101",
   "100110100",
   "100110011",
   "100110011",
   "100110010",
   "100110001",
   "100110001",
   "100110000",
   "100101111",
   "100101111",
   "100101110",
   "100101101",
   "100101100",
   "100101100",
   "100101011",
   "100101010",
   "100101010",
   "100101001",
   "100101000",
   "100101000",
   "100100111",
   "100100110",
   "100100110",
   "100100101",
   "100100100",
   "100100100",
   "100100011",
   "100100010",
   "100100010",
   "100100001",
   "100100001",
   "100100000",
   "100011111",
   "100011111",
   "100011110",
   "100011101",
   "100011101",
   "100011100",
   "100011100",
   "100011011",
   "100011010",
   "100011010",
   "100011001",
   "100011001",
   "100011000",
   "100010111",
   "100010111",
   "100010110",
   "100010110",
   "100010101",
   "100010100",
   "100010100",
   "100010011",
   "100010011",
   "100010010",
   "100010010",
   "100010001",
   "100010000",
   "100010000",
   "100001111",
   "100001111",
   "100001110",
   "100001110",
   "100001101",
   "100001101",
   "100001100",
   "100001011",
   "100001011",
   "100001010",
   "100001010",
   "100001001",
   "100001001",
   "100001000",
   "100001000",
   "100000111",
   "100000111",
   "100000110",
   "100000110",
   "100000101",
   "100000101",
   "100000100",
   "100000100",
   "100000011",
   "100000011",
   "100000010",
   "100000010",
   "100000001",
      others => (others => '0'));
      	begin 
      return tmp;
      end init_rom;
	signal rom : memory_t := init_rom;
   signal Y0 :  std_logic_vector(8 downto 0);
begin
	process(clk)
   begin
   if(rising_edge(clk)) then
   	Y0 <= rom(  TO_INTEGER(unsigned(X))  );
   end if;
   end process;
    Y <= Y0;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_42_f400_uid33
--                     (IntAdderClassical_42_f400_uid35)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_42_f400_uid33 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(41 downto 0);
          Y : in  std_logic_vector(41 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(41 downto 0)   );
end entity;

architecture arch of IntAdder_42_f400_uid33 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                IntMultiplier_UsingDSP_9_35_0_unsigned_uid22
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_9_35_0_unsigned_uid22 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8 downto 0);
          Y : in  std_logic_vector(34 downto 0);
          R : out  std_logic_vector(43 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_9_35_0_unsigned_uid22 is
   component IntAdder_42_f400_uid33 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(41 downto 0);
             Y : in  std_logic_vector(41 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(41 downto 0)   );
   end component;

signal XX_m23 :  std_logic_vector(34 downto 0);
signal YY_m23 :  std_logic_vector(8 downto 0);
signal DSP_bh24_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh24_w43_0, heap_bh24_w43_0_d1 : std_logic;
signal heap_bh24_w42_0, heap_bh24_w42_0_d1 : std_logic;
signal heap_bh24_w41_0, heap_bh24_w41_0_d1 : std_logic;
signal heap_bh24_w40_0, heap_bh24_w40_0_d1 : std_logic;
signal heap_bh24_w39_0, heap_bh24_w39_0_d1 : std_logic;
signal heap_bh24_w38_0, heap_bh24_w38_0_d1 : std_logic;
signal heap_bh24_w37_0, heap_bh24_w37_0_d1 : std_logic;
signal heap_bh24_w36_0, heap_bh24_w36_0_d1 : std_logic;
signal heap_bh24_w35_0, heap_bh24_w35_0_d1 : std_logic;
signal heap_bh24_w34_0, heap_bh24_w34_0_d1 : std_logic;
signal heap_bh24_w33_0, heap_bh24_w33_0_d1 : std_logic;
signal heap_bh24_w32_0, heap_bh24_w32_0_d1 : std_logic;
signal heap_bh24_w31_0, heap_bh24_w31_0_d1 : std_logic;
signal heap_bh24_w30_0, heap_bh24_w30_0_d1 : std_logic;
signal heap_bh24_w29_0, heap_bh24_w29_0_d1 : std_logic;
signal heap_bh24_w28_0, heap_bh24_w28_0_d1 : std_logic;
signal heap_bh24_w27_0, heap_bh24_w27_0_d1 : std_logic;
signal heap_bh24_w26_0, heap_bh24_w26_0_d1 : std_logic;
signal heap_bh24_w25_0, heap_bh24_w25_0_d1 : std_logic;
signal heap_bh24_w24_0, heap_bh24_w24_0_d1 : std_logic;
signal heap_bh24_w23_0, heap_bh24_w23_0_d1 : std_logic;
signal heap_bh24_w22_0, heap_bh24_w22_0_d1 : std_logic;
signal heap_bh24_w21_0, heap_bh24_w21_0_d1 : std_logic;
signal heap_bh24_w20_0, heap_bh24_w20_0_d1 : std_logic;
signal heap_bh24_w19_0, heap_bh24_w19_0_d1 : std_logic;
signal heap_bh24_w18_0, heap_bh24_w18_0_d1 : std_logic;
signal heap_bh24_w17_0, heap_bh24_w17_0_d1 : std_logic;
signal heap_bh24_w16_0, heap_bh24_w16_0_d1 : std_logic;
signal heap_bh24_w15_0, heap_bh24_w15_0_d1 : std_logic;
signal heap_bh24_w14_0, heap_bh24_w14_0_d1 : std_logic;
signal heap_bh24_w13_0, heap_bh24_w13_0_d1 : std_logic;
signal heap_bh24_w12_0, heap_bh24_w12_0_d1 : std_logic;
signal heap_bh24_w11_0, heap_bh24_w11_0_d1 : std_logic;
signal heap_bh24_w10_0, heap_bh24_w10_0_d1 : std_logic;
signal heap_bh24_w9_0, heap_bh24_w9_0_d1 : std_logic;
signal heap_bh24_w8_0, heap_bh24_w8_0_d1 : std_logic;
signal heap_bh24_w7_0, heap_bh24_w7_0_d1 : std_logic;
signal heap_bh24_w6_0, heap_bh24_w6_0_d1 : std_logic;
signal heap_bh24_w5_0, heap_bh24_w5_0_d1 : std_logic;
signal heap_bh24_w4_0, heap_bh24_w4_0_d1 : std_logic;
signal heap_bh24_w3_0, heap_bh24_w3_0_d1 : std_logic;
signal DSP_bh24_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh24_w19_1, heap_bh24_w19_1_d1 : std_logic;
signal heap_bh24_w18_1, heap_bh24_w18_1_d1 : std_logic;
signal heap_bh24_w17_1, heap_bh24_w17_1_d1 : std_logic;
signal heap_bh24_w16_1, heap_bh24_w16_1_d1 : std_logic;
signal heap_bh24_w15_1, heap_bh24_w15_1_d1 : std_logic;
signal heap_bh24_w14_1, heap_bh24_w14_1_d1 : std_logic;
signal heap_bh24_w13_1, heap_bh24_w13_1_d1 : std_logic;
signal heap_bh24_w12_1, heap_bh24_w12_1_d1 : std_logic;
signal heap_bh24_w11_1, heap_bh24_w11_1_d1 : std_logic;
signal heap_bh24_w10_1, heap_bh24_w10_1_d1 : std_logic;
signal heap_bh24_w9_1, heap_bh24_w9_1_d1 : std_logic;
signal heap_bh24_w8_1, heap_bh24_w8_1_d1 : std_logic;
signal heap_bh24_w7_1, heap_bh24_w7_1_d1 : std_logic;
signal heap_bh24_w6_1, heap_bh24_w6_1_d1 : std_logic;
signal heap_bh24_w5_1, heap_bh24_w5_1_d1 : std_logic;
signal heap_bh24_w4_1, heap_bh24_w4_1_d1 : std_logic;
signal heap_bh24_w3_1, heap_bh24_w3_1_d1 : std_logic;
signal heap_bh24_w2_0 : std_logic;
signal heap_bh24_w1_0 : std_logic;
signal heap_bh24_w0_0 : std_logic;
signal finalAdderIn0_bh24 :  std_logic_vector(41 downto 0);
signal finalAdderIn1_bh24 :  std_logic_vector(41 downto 0);
signal finalAdderCin_bh24 : std_logic;
signal finalAdderOut_bh24 :  std_logic_vector(41 downto 0);
signal tempR_bh24_0, tempR_bh24_0_d1 :  std_logic_vector(2 downto 0);
signal CompressionResult24 :  std_logic_vector(44 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh24_w43_0_d1 <=  heap_bh24_w43_0;
            heap_bh24_w42_0_d1 <=  heap_bh24_w42_0;
            heap_bh24_w41_0_d1 <=  heap_bh24_w41_0;
            heap_bh24_w40_0_d1 <=  heap_bh24_w40_0;
            heap_bh24_w39_0_d1 <=  heap_bh24_w39_0;
            heap_bh24_w38_0_d1 <=  heap_bh24_w38_0;
            heap_bh24_w37_0_d1 <=  heap_bh24_w37_0;
            heap_bh24_w36_0_d1 <=  heap_bh24_w36_0;
            heap_bh24_w35_0_d1 <=  heap_bh24_w35_0;
            heap_bh24_w34_0_d1 <=  heap_bh24_w34_0;
            heap_bh24_w33_0_d1 <=  heap_bh24_w33_0;
            heap_bh24_w32_0_d1 <=  heap_bh24_w32_0;
            heap_bh24_w31_0_d1 <=  heap_bh24_w31_0;
            heap_bh24_w30_0_d1 <=  heap_bh24_w30_0;
            heap_bh24_w29_0_d1 <=  heap_bh24_w29_0;
            heap_bh24_w28_0_d1 <=  heap_bh24_w28_0;
            heap_bh24_w27_0_d1 <=  heap_bh24_w27_0;
            heap_bh24_w26_0_d1 <=  heap_bh24_w26_0;
            heap_bh24_w25_0_d1 <=  heap_bh24_w25_0;
            heap_bh24_w24_0_d1 <=  heap_bh24_w24_0;
            heap_bh24_w23_0_d1 <=  heap_bh24_w23_0;
            heap_bh24_w22_0_d1 <=  heap_bh24_w22_0;
            heap_bh24_w21_0_d1 <=  heap_bh24_w21_0;
            heap_bh24_w20_0_d1 <=  heap_bh24_w20_0;
            heap_bh24_w19_0_d1 <=  heap_bh24_w19_0;
            heap_bh24_w18_0_d1 <=  heap_bh24_w18_0;
            heap_bh24_w17_0_d1 <=  heap_bh24_w17_0;
            heap_bh24_w16_0_d1 <=  heap_bh24_w16_0;
            heap_bh24_w15_0_d1 <=  heap_bh24_w15_0;
            heap_bh24_w14_0_d1 <=  heap_bh24_w14_0;
            heap_bh24_w13_0_d1 <=  heap_bh24_w13_0;
            heap_bh24_w12_0_d1 <=  heap_bh24_w12_0;
            heap_bh24_w11_0_d1 <=  heap_bh24_w11_0;
            heap_bh24_w10_0_d1 <=  heap_bh24_w10_0;
            heap_bh24_w9_0_d1 <=  heap_bh24_w9_0;
            heap_bh24_w8_0_d1 <=  heap_bh24_w8_0;
            heap_bh24_w7_0_d1 <=  heap_bh24_w7_0;
            heap_bh24_w6_0_d1 <=  heap_bh24_w6_0;
            heap_bh24_w5_0_d1 <=  heap_bh24_w5_0;
            heap_bh24_w4_0_d1 <=  heap_bh24_w4_0;
            heap_bh24_w3_0_d1 <=  heap_bh24_w3_0;
            heap_bh24_w19_1_d1 <=  heap_bh24_w19_1;
            heap_bh24_w18_1_d1 <=  heap_bh24_w18_1;
            heap_bh24_w17_1_d1 <=  heap_bh24_w17_1;
            heap_bh24_w16_1_d1 <=  heap_bh24_w16_1;
            heap_bh24_w15_1_d1 <=  heap_bh24_w15_1;
            heap_bh24_w14_1_d1 <=  heap_bh24_w14_1;
            heap_bh24_w13_1_d1 <=  heap_bh24_w13_1;
            heap_bh24_w12_1_d1 <=  heap_bh24_w12_1;
            heap_bh24_w11_1_d1 <=  heap_bh24_w11_1;
            heap_bh24_w10_1_d1 <=  heap_bh24_w10_1;
            heap_bh24_w9_1_d1 <=  heap_bh24_w9_1;
            heap_bh24_w8_1_d1 <=  heap_bh24_w8_1;
            heap_bh24_w7_1_d1 <=  heap_bh24_w7_1;
            heap_bh24_w6_1_d1 <=  heap_bh24_w6_1;
            heap_bh24_w5_1_d1 <=  heap_bh24_w5_1;
            heap_bh24_w4_1_d1 <=  heap_bh24_w4_1;
            heap_bh24_w3_1_d1 <=  heap_bh24_w3_1;
            tempR_bh24_0_d1 <=  tempR_bh24_0;
         end if;
      end process;
   XX_m23 <= Y ;
   YY_m23 <= X ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh24_ch0_0 <= ("" & XX_m23(34 downto 11) & "") * ("" & YY_m23(8 downto 0) & "00000000");
   heap_bh24_w43_0 <= DSP_bh24_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w42_0 <= DSP_bh24_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w41_0 <= DSP_bh24_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w40_0 <= DSP_bh24_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w39_0 <= DSP_bh24_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w38_0 <= DSP_bh24_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w37_0 <= DSP_bh24_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w36_0 <= DSP_bh24_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w35_0 <= DSP_bh24_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w34_0 <= DSP_bh24_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w33_0 <= DSP_bh24_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w32_0 <= DSP_bh24_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w31_0 <= DSP_bh24_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w30_0 <= DSP_bh24_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w29_0 <= DSP_bh24_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w28_0 <= DSP_bh24_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w27_0 <= DSP_bh24_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w26_0 <= DSP_bh24_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w25_0 <= DSP_bh24_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w24_0 <= DSP_bh24_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w23_0 <= DSP_bh24_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w22_0 <= DSP_bh24_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w21_0 <= DSP_bh24_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w20_0 <= DSP_bh24_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w19_0 <= DSP_bh24_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w18_0 <= DSP_bh24_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w17_0 <= DSP_bh24_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w16_0 <= DSP_bh24_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w15_0 <= DSP_bh24_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w14_0 <= DSP_bh24_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w13_0 <= DSP_bh24_ch0_0(10); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w12_0 <= DSP_bh24_ch0_0(9); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w11_0 <= DSP_bh24_ch0_0(8); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w10_0 <= DSP_bh24_ch0_0(7); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w9_0 <= DSP_bh24_ch0_0(6); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w8_0 <= DSP_bh24_ch0_0(5); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w7_0 <= DSP_bh24_ch0_0(4); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w6_0 <= DSP_bh24_ch0_0(3); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w5_0 <= DSP_bh24_ch0_0(2); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w4_0 <= DSP_bh24_ch0_0(1); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w3_0 <= DSP_bh24_ch0_0(0); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh24_ch1_0 <= ("" & XX_m23(10 downto 0) & "0000000000000") * ("" & YY_m23(8 downto 0) & "00000000");
   heap_bh24_w19_1 <= DSP_bh24_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w18_1 <= DSP_bh24_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w17_1 <= DSP_bh24_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w16_1 <= DSP_bh24_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w15_1 <= DSP_bh24_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w14_1 <= DSP_bh24_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w13_1 <= DSP_bh24_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w12_1 <= DSP_bh24_ch1_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w11_1 <= DSP_bh24_ch1_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w10_1 <= DSP_bh24_ch1_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w9_1 <= DSP_bh24_ch1_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w8_1 <= DSP_bh24_ch1_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w7_1 <= DSP_bh24_ch1_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w6_1 <= DSP_bh24_ch1_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w5_1 <= DSP_bh24_ch1_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w4_1 <= DSP_bh24_ch1_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w3_1 <= DSP_bh24_ch1_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w2_0 <= DSP_bh24_ch1_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w1_0 <= DSP_bh24_ch1_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh24_w0_0 <= DSP_bh24_ch1_0(21); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh24 <= "0" & heap_bh24_w43_0_d1 & heap_bh24_w42_0_d1 & heap_bh24_w41_0_d1 & heap_bh24_w40_0_d1 & heap_bh24_w39_0_d1 & heap_bh24_w38_0_d1 & heap_bh24_w37_0_d1 & heap_bh24_w36_0_d1 & heap_bh24_w35_0_d1 & heap_bh24_w34_0_d1 & heap_bh24_w33_0_d1 & heap_bh24_w32_0_d1 & heap_bh24_w31_0_d1 & heap_bh24_w30_0_d1 & heap_bh24_w29_0_d1 & heap_bh24_w28_0_d1 & heap_bh24_w27_0_d1 & heap_bh24_w26_0_d1 & heap_bh24_w25_0_d1 & heap_bh24_w24_0_d1 & heap_bh24_w23_0_d1 & heap_bh24_w22_0_d1 & heap_bh24_w21_0_d1 & heap_bh24_w20_0_d1 & heap_bh24_w19_1_d1 & heap_bh24_w18_1_d1 & heap_bh24_w17_1_d1 & heap_bh24_w16_1_d1 & heap_bh24_w15_1_d1 & heap_bh24_w14_1_d1 & heap_bh24_w13_1_d1 & heap_bh24_w12_1_d1 & heap_bh24_w11_1_d1 & heap_bh24_w10_1_d1 & heap_bh24_w9_1_d1 & heap_bh24_w8_1_d1 & heap_bh24_w7_1_d1 & heap_bh24_w6_1_d1 & heap_bh24_w5_1_d1 & heap_bh24_w4_1_d1 & heap_bh24_w3_1_d1;
   finalAdderIn1_bh24 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh24_w19_0_d1 & heap_bh24_w18_0_d1 & heap_bh24_w17_0_d1 & heap_bh24_w16_0_d1 & heap_bh24_w15_0_d1 & heap_bh24_w14_0_d1 & heap_bh24_w13_0_d1 & heap_bh24_w12_0_d1 & heap_bh24_w11_0_d1 & heap_bh24_w10_0_d1 & heap_bh24_w9_0_d1 & heap_bh24_w8_0_d1 & heap_bh24_w7_0_d1 & heap_bh24_w6_0_d1 & heap_bh24_w5_0_d1 & heap_bh24_w4_0_d1 & heap_bh24_w3_0_d1;
   finalAdderCin_bh24 <= '0';
   Adder_final24_0: IntAdder_42_f400_uid33  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh24,
                 R => finalAdderOut_bh24   ,
                 X => finalAdderIn0_bh24,
                 Y => finalAdderIn1_bh24);
   ----------------Synchro barrier, entering cycle 0----------------
   tempR_bh24_0 <= heap_bh24_w2_0 & heap_bh24_w1_0 & heap_bh24_w0_0; -- already compressed
   -- concatenate all the compressed chunks
   ----------------Synchro barrier, entering cycle 1----------------
   CompressionResult24 <= finalAdderOut_bh24 & tempR_bh24_0_d1;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult24(43 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_42_f400_uid52
--                     (IntAdderClassical_42_f400_uid54)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_42_f400_uid52 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(41 downto 0);
          Y : in  std_logic_vector(41 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(41 downto 0)   );
end entity;

architecture arch of IntAdder_42_f400_uid52 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                IntMultiplier_UsingDSP_6_36_0_unsigned_uid41
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_6_36_0_unsigned_uid41 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : in  std_logic_vector(35 downto 0);
          R : out  std_logic_vector(41 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_6_36_0_unsigned_uid41 is
   component IntAdder_42_f400_uid52 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(41 downto 0);
             Y : in  std_logic_vector(41 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(41 downto 0)   );
   end component;

signal XX_m42 :  std_logic_vector(35 downto 0);
signal YY_m42 :  std_logic_vector(5 downto 0);
signal DSP_bh43_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh43_w41_0, heap_bh43_w41_0_d1 : std_logic;
signal heap_bh43_w40_0, heap_bh43_w40_0_d1 : std_logic;
signal heap_bh43_w39_0, heap_bh43_w39_0_d1 : std_logic;
signal heap_bh43_w38_0, heap_bh43_w38_0_d1 : std_logic;
signal heap_bh43_w37_0, heap_bh43_w37_0_d1 : std_logic;
signal heap_bh43_w36_0, heap_bh43_w36_0_d1 : std_logic;
signal heap_bh43_w35_0, heap_bh43_w35_0_d1 : std_logic;
signal heap_bh43_w34_0, heap_bh43_w34_0_d1 : std_logic;
signal heap_bh43_w33_0, heap_bh43_w33_0_d1 : std_logic;
signal heap_bh43_w32_0, heap_bh43_w32_0_d1 : std_logic;
signal heap_bh43_w31_0, heap_bh43_w31_0_d1 : std_logic;
signal heap_bh43_w30_0, heap_bh43_w30_0_d1 : std_logic;
signal heap_bh43_w29_0, heap_bh43_w29_0_d1 : std_logic;
signal heap_bh43_w28_0, heap_bh43_w28_0_d1 : std_logic;
signal heap_bh43_w27_0, heap_bh43_w27_0_d1 : std_logic;
signal heap_bh43_w26_0, heap_bh43_w26_0_d1 : std_logic;
signal heap_bh43_w25_0, heap_bh43_w25_0_d1 : std_logic;
signal heap_bh43_w24_0, heap_bh43_w24_0_d1 : std_logic;
signal heap_bh43_w23_0, heap_bh43_w23_0_d1 : std_logic;
signal heap_bh43_w22_0, heap_bh43_w22_0_d1 : std_logic;
signal heap_bh43_w21_0, heap_bh43_w21_0_d1 : std_logic;
signal heap_bh43_w20_0, heap_bh43_w20_0_d1 : std_logic;
signal heap_bh43_w19_0, heap_bh43_w19_0_d1 : std_logic;
signal heap_bh43_w18_0, heap_bh43_w18_0_d1 : std_logic;
signal heap_bh43_w17_0, heap_bh43_w17_0_d1 : std_logic;
signal heap_bh43_w16_0, heap_bh43_w16_0_d1 : std_logic;
signal heap_bh43_w15_0, heap_bh43_w15_0_d1 : std_logic;
signal heap_bh43_w14_0, heap_bh43_w14_0_d1 : std_logic;
signal heap_bh43_w13_0, heap_bh43_w13_0_d1 : std_logic;
signal heap_bh43_w12_0, heap_bh43_w12_0_d1 : std_logic;
signal heap_bh43_w11_0, heap_bh43_w11_0_d1 : std_logic;
signal heap_bh43_w10_0, heap_bh43_w10_0_d1 : std_logic;
signal heap_bh43_w9_0, heap_bh43_w9_0_d1 : std_logic;
signal heap_bh43_w8_0, heap_bh43_w8_0_d1 : std_logic;
signal heap_bh43_w7_0, heap_bh43_w7_0_d1 : std_logic;
signal heap_bh43_w6_0, heap_bh43_w6_0_d1 : std_logic;
signal heap_bh43_w5_0, heap_bh43_w5_0_d1 : std_logic;
signal heap_bh43_w4_0, heap_bh43_w4_0_d1 : std_logic;
signal heap_bh43_w3_0, heap_bh43_w3_0_d1 : std_logic;
signal heap_bh43_w2_0, heap_bh43_w2_0_d1 : std_logic;
signal heap_bh43_w1_0, heap_bh43_w1_0_d1 : std_logic;
signal DSP_bh43_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh43_w17_1, heap_bh43_w17_1_d1 : std_logic;
signal heap_bh43_w16_1, heap_bh43_w16_1_d1 : std_logic;
signal heap_bh43_w15_1, heap_bh43_w15_1_d1 : std_logic;
signal heap_bh43_w14_1, heap_bh43_w14_1_d1 : std_logic;
signal heap_bh43_w13_1, heap_bh43_w13_1_d1 : std_logic;
signal heap_bh43_w12_1, heap_bh43_w12_1_d1 : std_logic;
signal heap_bh43_w11_1, heap_bh43_w11_1_d1 : std_logic;
signal heap_bh43_w10_1, heap_bh43_w10_1_d1 : std_logic;
signal heap_bh43_w9_1, heap_bh43_w9_1_d1 : std_logic;
signal heap_bh43_w8_1, heap_bh43_w8_1_d1 : std_logic;
signal heap_bh43_w7_1, heap_bh43_w7_1_d1 : std_logic;
signal heap_bh43_w6_1, heap_bh43_w6_1_d1 : std_logic;
signal heap_bh43_w5_1, heap_bh43_w5_1_d1 : std_logic;
signal heap_bh43_w4_1, heap_bh43_w4_1_d1 : std_logic;
signal heap_bh43_w3_1, heap_bh43_w3_1_d1 : std_logic;
signal heap_bh43_w2_1, heap_bh43_w2_1_d1 : std_logic;
signal heap_bh43_w1_1, heap_bh43_w1_1_d1 : std_logic;
signal heap_bh43_w0_0 : std_logic;
signal finalAdderIn0_bh43 :  std_logic_vector(41 downto 0);
signal finalAdderIn1_bh43 :  std_logic_vector(41 downto 0);
signal finalAdderCin_bh43 : std_logic;
signal finalAdderOut_bh43 :  std_logic_vector(41 downto 0);
signal tempR_bh43_0, tempR_bh43_0_d1 : std_logic;
signal CompressionResult43 :  std_logic_vector(42 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh43_w41_0_d1 <=  heap_bh43_w41_0;
            heap_bh43_w40_0_d1 <=  heap_bh43_w40_0;
            heap_bh43_w39_0_d1 <=  heap_bh43_w39_0;
            heap_bh43_w38_0_d1 <=  heap_bh43_w38_0;
            heap_bh43_w37_0_d1 <=  heap_bh43_w37_0;
            heap_bh43_w36_0_d1 <=  heap_bh43_w36_0;
            heap_bh43_w35_0_d1 <=  heap_bh43_w35_0;
            heap_bh43_w34_0_d1 <=  heap_bh43_w34_0;
            heap_bh43_w33_0_d1 <=  heap_bh43_w33_0;
            heap_bh43_w32_0_d1 <=  heap_bh43_w32_0;
            heap_bh43_w31_0_d1 <=  heap_bh43_w31_0;
            heap_bh43_w30_0_d1 <=  heap_bh43_w30_0;
            heap_bh43_w29_0_d1 <=  heap_bh43_w29_0;
            heap_bh43_w28_0_d1 <=  heap_bh43_w28_0;
            heap_bh43_w27_0_d1 <=  heap_bh43_w27_0;
            heap_bh43_w26_0_d1 <=  heap_bh43_w26_0;
            heap_bh43_w25_0_d1 <=  heap_bh43_w25_0;
            heap_bh43_w24_0_d1 <=  heap_bh43_w24_0;
            heap_bh43_w23_0_d1 <=  heap_bh43_w23_0;
            heap_bh43_w22_0_d1 <=  heap_bh43_w22_0;
            heap_bh43_w21_0_d1 <=  heap_bh43_w21_0;
            heap_bh43_w20_0_d1 <=  heap_bh43_w20_0;
            heap_bh43_w19_0_d1 <=  heap_bh43_w19_0;
            heap_bh43_w18_0_d1 <=  heap_bh43_w18_0;
            heap_bh43_w17_0_d1 <=  heap_bh43_w17_0;
            heap_bh43_w16_0_d1 <=  heap_bh43_w16_0;
            heap_bh43_w15_0_d1 <=  heap_bh43_w15_0;
            heap_bh43_w14_0_d1 <=  heap_bh43_w14_0;
            heap_bh43_w13_0_d1 <=  heap_bh43_w13_0;
            heap_bh43_w12_0_d1 <=  heap_bh43_w12_0;
            heap_bh43_w11_0_d1 <=  heap_bh43_w11_0;
            heap_bh43_w10_0_d1 <=  heap_bh43_w10_0;
            heap_bh43_w9_0_d1 <=  heap_bh43_w9_0;
            heap_bh43_w8_0_d1 <=  heap_bh43_w8_0;
            heap_bh43_w7_0_d1 <=  heap_bh43_w7_0;
            heap_bh43_w6_0_d1 <=  heap_bh43_w6_0;
            heap_bh43_w5_0_d1 <=  heap_bh43_w5_0;
            heap_bh43_w4_0_d1 <=  heap_bh43_w4_0;
            heap_bh43_w3_0_d1 <=  heap_bh43_w3_0;
            heap_bh43_w2_0_d1 <=  heap_bh43_w2_0;
            heap_bh43_w1_0_d1 <=  heap_bh43_w1_0;
            heap_bh43_w17_1_d1 <=  heap_bh43_w17_1;
            heap_bh43_w16_1_d1 <=  heap_bh43_w16_1;
            heap_bh43_w15_1_d1 <=  heap_bh43_w15_1;
            heap_bh43_w14_1_d1 <=  heap_bh43_w14_1;
            heap_bh43_w13_1_d1 <=  heap_bh43_w13_1;
            heap_bh43_w12_1_d1 <=  heap_bh43_w12_1;
            heap_bh43_w11_1_d1 <=  heap_bh43_w11_1;
            heap_bh43_w10_1_d1 <=  heap_bh43_w10_1;
            heap_bh43_w9_1_d1 <=  heap_bh43_w9_1;
            heap_bh43_w8_1_d1 <=  heap_bh43_w8_1;
            heap_bh43_w7_1_d1 <=  heap_bh43_w7_1;
            heap_bh43_w6_1_d1 <=  heap_bh43_w6_1;
            heap_bh43_w5_1_d1 <=  heap_bh43_w5_1;
            heap_bh43_w4_1_d1 <=  heap_bh43_w4_1;
            heap_bh43_w3_1_d1 <=  heap_bh43_w3_1;
            heap_bh43_w2_1_d1 <=  heap_bh43_w2_1;
            heap_bh43_w1_1_d1 <=  heap_bh43_w1_1;
            tempR_bh43_0_d1 <=  tempR_bh43_0;
         end if;
      end process;
   XX_m42 <= Y ;
   YY_m42 <= X ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh43_ch0_0 <= ("" & XX_m42(35 downto 12) & "") * ("" & YY_m42(5 downto 0) & "00000000000");
   heap_bh43_w41_0 <= DSP_bh43_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w40_0 <= DSP_bh43_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w39_0 <= DSP_bh43_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w38_0 <= DSP_bh43_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w37_0 <= DSP_bh43_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w36_0 <= DSP_bh43_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w35_0 <= DSP_bh43_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w34_0 <= DSP_bh43_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w33_0 <= DSP_bh43_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w32_0 <= DSP_bh43_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w31_0 <= DSP_bh43_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w30_0 <= DSP_bh43_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w29_0 <= DSP_bh43_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w28_0 <= DSP_bh43_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w27_0 <= DSP_bh43_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w26_0 <= DSP_bh43_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w25_0 <= DSP_bh43_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w24_0 <= DSP_bh43_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w23_0 <= DSP_bh43_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w22_0 <= DSP_bh43_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w21_0 <= DSP_bh43_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w20_0 <= DSP_bh43_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w19_0 <= DSP_bh43_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w18_0 <= DSP_bh43_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w17_0 <= DSP_bh43_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w16_0 <= DSP_bh43_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w15_0 <= DSP_bh43_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w14_0 <= DSP_bh43_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w13_0 <= DSP_bh43_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w12_0 <= DSP_bh43_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w11_0 <= DSP_bh43_ch0_0(10); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w10_0 <= DSP_bh43_ch0_0(9); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w9_0 <= DSP_bh43_ch0_0(8); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w8_0 <= DSP_bh43_ch0_0(7); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w7_0 <= DSP_bh43_ch0_0(6); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w6_0 <= DSP_bh43_ch0_0(5); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w5_0 <= DSP_bh43_ch0_0(4); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w4_0 <= DSP_bh43_ch0_0(3); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w3_0 <= DSP_bh43_ch0_0(2); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w2_0 <= DSP_bh43_ch0_0(1); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w1_0 <= DSP_bh43_ch0_0(0); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh43_ch1_0 <= ("" & XX_m42(11 downto 0) & "000000000000") * ("" & YY_m42(5 downto 0) & "00000000000");
   heap_bh43_w17_1 <= DSP_bh43_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w16_1 <= DSP_bh43_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w15_1 <= DSP_bh43_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w14_1 <= DSP_bh43_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w13_1 <= DSP_bh43_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w12_1 <= DSP_bh43_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w11_1 <= DSP_bh43_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w10_1 <= DSP_bh43_ch1_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w9_1 <= DSP_bh43_ch1_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w8_1 <= DSP_bh43_ch1_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w7_1 <= DSP_bh43_ch1_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w6_1 <= DSP_bh43_ch1_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w5_1 <= DSP_bh43_ch1_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w4_1 <= DSP_bh43_ch1_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w3_1 <= DSP_bh43_ch1_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w2_1 <= DSP_bh43_ch1_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w1_1 <= DSP_bh43_ch1_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh43_w0_0 <= DSP_bh43_ch1_0(23); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh43 <= "0" & heap_bh43_w41_0_d1 & heap_bh43_w40_0_d1 & heap_bh43_w39_0_d1 & heap_bh43_w38_0_d1 & heap_bh43_w37_0_d1 & heap_bh43_w36_0_d1 & heap_bh43_w35_0_d1 & heap_bh43_w34_0_d1 & heap_bh43_w33_0_d1 & heap_bh43_w32_0_d1 & heap_bh43_w31_0_d1 & heap_bh43_w30_0_d1 & heap_bh43_w29_0_d1 & heap_bh43_w28_0_d1 & heap_bh43_w27_0_d1 & heap_bh43_w26_0_d1 & heap_bh43_w25_0_d1 & heap_bh43_w24_0_d1 & heap_bh43_w23_0_d1 & heap_bh43_w22_0_d1 & heap_bh43_w21_0_d1 & heap_bh43_w20_0_d1 & heap_bh43_w19_0_d1 & heap_bh43_w18_0_d1 & heap_bh43_w17_1_d1 & heap_bh43_w16_1_d1 & heap_bh43_w15_1_d1 & heap_bh43_w14_1_d1 & heap_bh43_w13_1_d1 & heap_bh43_w12_1_d1 & heap_bh43_w11_1_d1 & heap_bh43_w10_1_d1 & heap_bh43_w9_1_d1 & heap_bh43_w8_1_d1 & heap_bh43_w7_1_d1 & heap_bh43_w6_1_d1 & heap_bh43_w5_1_d1 & heap_bh43_w4_1_d1 & heap_bh43_w3_1_d1 & heap_bh43_w2_1_d1 & heap_bh43_w1_1_d1;
   finalAdderIn1_bh43 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh43_w17_0_d1 & heap_bh43_w16_0_d1 & heap_bh43_w15_0_d1 & heap_bh43_w14_0_d1 & heap_bh43_w13_0_d1 & heap_bh43_w12_0_d1 & heap_bh43_w11_0_d1 & heap_bh43_w10_0_d1 & heap_bh43_w9_0_d1 & heap_bh43_w8_0_d1 & heap_bh43_w7_0_d1 & heap_bh43_w6_0_d1 & heap_bh43_w5_0_d1 & heap_bh43_w4_0_d1 & heap_bh43_w3_0_d1 & heap_bh43_w2_0_d1 & heap_bh43_w1_0_d1;
   finalAdderCin_bh43 <= '0';
   Adder_final43_0: IntAdder_42_f400_uid52  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh43,
                 R => finalAdderOut_bh43   ,
                 X => finalAdderIn0_bh43,
                 Y => finalAdderIn1_bh43);
   ----------------Synchro barrier, entering cycle 0----------------
   tempR_bh43_0 <= heap_bh43_w0_0; -- already compressed
   -- concatenate all the compressed chunks
   ----------------Synchro barrier, entering cycle 1----------------
   CompressionResult43 <= finalAdderOut_bh43 & tempR_bh43_0_d1;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult43(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_43_f400_uid60
--                    (IntAdderAlternative_43_f400_uid64)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_43_f400_uid60 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(42 downto 0);
          Y : in  std_logic_vector(42 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(42 downto 0)   );
end entity;

architecture arch of IntAdder_43_f400_uid60 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(1 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(0 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(1 downto 0);
signal sum_l1_idx1 :  std_logic_vector(0 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(42 downto 42)) + ( "0" & Y(42 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(0 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(1 downto 1);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(0 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(1 downto 1);
   R <= sum_l1_idx1(0 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_43_f400_uid67
--                    (IntAdderAlternative_43_f400_uid71)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_43_f400_uid67 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(42 downto 0);
          Y : in  std_logic_vector(42 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(42 downto 0)   );
end entity;

architecture arch of IntAdder_43_f400_uid67 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(1 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(0 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(1 downto 0);
signal sum_l1_idx1 :  std_logic_vector(0 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(42 downto 42)) + ( "0" & Y(42 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(0 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(1 downto 1);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(0 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(1 downto 1);
   R <= sum_l1_idx1(0 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_40_f400_uid85
--                     (IntAdderClassical_40_f400_uid87)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_40_f400_uid85 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(39 downto 0);
          Y : in  std_logic_vector(39 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(39 downto 0)   );
end entity;

architecture arch of IntAdder_40_f400_uid85 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                IntMultiplier_UsingDSP_7_32_0_unsigned_uid74
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_7_32_0_unsigned_uid74 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          R : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_7_32_0_unsigned_uid74 is
   component IntAdder_40_f400_uid85 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(39 downto 0);
             Y : in  std_logic_vector(39 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(39 downto 0)   );
   end component;

signal XX_m75 :  std_logic_vector(31 downto 0);
signal YY_m75 :  std_logic_vector(6 downto 0);
signal DSP_bh76_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh76_w38_0, heap_bh76_w38_0_d1 : std_logic;
signal heap_bh76_w37_0, heap_bh76_w37_0_d1 : std_logic;
signal heap_bh76_w36_0, heap_bh76_w36_0_d1 : std_logic;
signal heap_bh76_w35_0, heap_bh76_w35_0_d1 : std_logic;
signal heap_bh76_w34_0, heap_bh76_w34_0_d1 : std_logic;
signal heap_bh76_w33_0, heap_bh76_w33_0_d1 : std_logic;
signal heap_bh76_w32_0, heap_bh76_w32_0_d1 : std_logic;
signal heap_bh76_w31_0, heap_bh76_w31_0_d1 : std_logic;
signal heap_bh76_w30_0, heap_bh76_w30_0_d1 : std_logic;
signal heap_bh76_w29_0, heap_bh76_w29_0_d1 : std_logic;
signal heap_bh76_w28_0, heap_bh76_w28_0_d1 : std_logic;
signal heap_bh76_w27_0, heap_bh76_w27_0_d1 : std_logic;
signal heap_bh76_w26_0, heap_bh76_w26_0_d1 : std_logic;
signal heap_bh76_w25_0, heap_bh76_w25_0_d1 : std_logic;
signal heap_bh76_w24_0, heap_bh76_w24_0_d1 : std_logic;
signal heap_bh76_w23_0, heap_bh76_w23_0_d1 : std_logic;
signal heap_bh76_w22_0, heap_bh76_w22_0_d1 : std_logic;
signal heap_bh76_w21_0, heap_bh76_w21_0_d1 : std_logic;
signal heap_bh76_w20_0, heap_bh76_w20_0_d1 : std_logic;
signal heap_bh76_w19_0, heap_bh76_w19_0_d1 : std_logic;
signal heap_bh76_w18_0, heap_bh76_w18_0_d1 : std_logic;
signal heap_bh76_w17_0, heap_bh76_w17_0_d1 : std_logic;
signal heap_bh76_w16_0, heap_bh76_w16_0_d1 : std_logic;
signal heap_bh76_w15_0, heap_bh76_w15_0_d1 : std_logic;
signal heap_bh76_w14_0, heap_bh76_w14_0_d1 : std_logic;
signal heap_bh76_w13_0, heap_bh76_w13_0_d1 : std_logic;
signal heap_bh76_w12_0, heap_bh76_w12_0_d1 : std_logic;
signal heap_bh76_w11_0, heap_bh76_w11_0_d1 : std_logic;
signal heap_bh76_w10_0, heap_bh76_w10_0_d1 : std_logic;
signal heap_bh76_w9_0, heap_bh76_w9_0_d1 : std_logic;
signal heap_bh76_w8_0, heap_bh76_w8_0_d1 : std_logic;
signal heap_bh76_w7_0, heap_bh76_w7_0_d1 : std_logic;
signal heap_bh76_w6_0, heap_bh76_w6_0_d1 : std_logic;
signal heap_bh76_w5_0, heap_bh76_w5_0_d1 : std_logic;
signal heap_bh76_w4_0, heap_bh76_w4_0_d1 : std_logic;
signal heap_bh76_w3_0, heap_bh76_w3_0_d1 : std_logic;
signal heap_bh76_w2_0, heap_bh76_w2_0_d1 : std_logic;
signal heap_bh76_w1_0, heap_bh76_w1_0_d1 : std_logic;
signal heap_bh76_w0_0, heap_bh76_w0_0_d1 : std_logic;
signal DSP_bh76_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh76_w14_1, heap_bh76_w14_1_d1 : std_logic;
signal heap_bh76_w13_1, heap_bh76_w13_1_d1 : std_logic;
signal heap_bh76_w12_1, heap_bh76_w12_1_d1 : std_logic;
signal heap_bh76_w11_1, heap_bh76_w11_1_d1 : std_logic;
signal heap_bh76_w10_1, heap_bh76_w10_1_d1 : std_logic;
signal heap_bh76_w9_1, heap_bh76_w9_1_d1 : std_logic;
signal heap_bh76_w8_1, heap_bh76_w8_1_d1 : std_logic;
signal heap_bh76_w7_1, heap_bh76_w7_1_d1 : std_logic;
signal heap_bh76_w6_1, heap_bh76_w6_1_d1 : std_logic;
signal heap_bh76_w5_1, heap_bh76_w5_1_d1 : std_logic;
signal heap_bh76_w4_1, heap_bh76_w4_1_d1 : std_logic;
signal heap_bh76_w3_1, heap_bh76_w3_1_d1 : std_logic;
signal heap_bh76_w2_1, heap_bh76_w2_1_d1 : std_logic;
signal heap_bh76_w1_1, heap_bh76_w1_1_d1 : std_logic;
signal heap_bh76_w0_1, heap_bh76_w0_1_d1 : std_logic;
signal finalAdderIn0_bh76 :  std_logic_vector(39 downto 0);
signal finalAdderIn1_bh76 :  std_logic_vector(39 downto 0);
signal finalAdderCin_bh76 : std_logic;
signal finalAdderOut_bh76 :  std_logic_vector(39 downto 0);
signal CompressionResult76 :  std_logic_vector(39 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh76_w38_0_d1 <=  heap_bh76_w38_0;
            heap_bh76_w37_0_d1 <=  heap_bh76_w37_0;
            heap_bh76_w36_0_d1 <=  heap_bh76_w36_0;
            heap_bh76_w35_0_d1 <=  heap_bh76_w35_0;
            heap_bh76_w34_0_d1 <=  heap_bh76_w34_0;
            heap_bh76_w33_0_d1 <=  heap_bh76_w33_0;
            heap_bh76_w32_0_d1 <=  heap_bh76_w32_0;
            heap_bh76_w31_0_d1 <=  heap_bh76_w31_0;
            heap_bh76_w30_0_d1 <=  heap_bh76_w30_0;
            heap_bh76_w29_0_d1 <=  heap_bh76_w29_0;
            heap_bh76_w28_0_d1 <=  heap_bh76_w28_0;
            heap_bh76_w27_0_d1 <=  heap_bh76_w27_0;
            heap_bh76_w26_0_d1 <=  heap_bh76_w26_0;
            heap_bh76_w25_0_d1 <=  heap_bh76_w25_0;
            heap_bh76_w24_0_d1 <=  heap_bh76_w24_0;
            heap_bh76_w23_0_d1 <=  heap_bh76_w23_0;
            heap_bh76_w22_0_d1 <=  heap_bh76_w22_0;
            heap_bh76_w21_0_d1 <=  heap_bh76_w21_0;
            heap_bh76_w20_0_d1 <=  heap_bh76_w20_0;
            heap_bh76_w19_0_d1 <=  heap_bh76_w19_0;
            heap_bh76_w18_0_d1 <=  heap_bh76_w18_0;
            heap_bh76_w17_0_d1 <=  heap_bh76_w17_0;
            heap_bh76_w16_0_d1 <=  heap_bh76_w16_0;
            heap_bh76_w15_0_d1 <=  heap_bh76_w15_0;
            heap_bh76_w14_0_d1 <=  heap_bh76_w14_0;
            heap_bh76_w13_0_d1 <=  heap_bh76_w13_0;
            heap_bh76_w12_0_d1 <=  heap_bh76_w12_0;
            heap_bh76_w11_0_d1 <=  heap_bh76_w11_0;
            heap_bh76_w10_0_d1 <=  heap_bh76_w10_0;
            heap_bh76_w9_0_d1 <=  heap_bh76_w9_0;
            heap_bh76_w8_0_d1 <=  heap_bh76_w8_0;
            heap_bh76_w7_0_d1 <=  heap_bh76_w7_0;
            heap_bh76_w6_0_d1 <=  heap_bh76_w6_0;
            heap_bh76_w5_0_d1 <=  heap_bh76_w5_0;
            heap_bh76_w4_0_d1 <=  heap_bh76_w4_0;
            heap_bh76_w3_0_d1 <=  heap_bh76_w3_0;
            heap_bh76_w2_0_d1 <=  heap_bh76_w2_0;
            heap_bh76_w1_0_d1 <=  heap_bh76_w1_0;
            heap_bh76_w0_0_d1 <=  heap_bh76_w0_0;
            heap_bh76_w14_1_d1 <=  heap_bh76_w14_1;
            heap_bh76_w13_1_d1 <=  heap_bh76_w13_1;
            heap_bh76_w12_1_d1 <=  heap_bh76_w12_1;
            heap_bh76_w11_1_d1 <=  heap_bh76_w11_1;
            heap_bh76_w10_1_d1 <=  heap_bh76_w10_1;
            heap_bh76_w9_1_d1 <=  heap_bh76_w9_1;
            heap_bh76_w8_1_d1 <=  heap_bh76_w8_1;
            heap_bh76_w7_1_d1 <=  heap_bh76_w7_1;
            heap_bh76_w6_1_d1 <=  heap_bh76_w6_1;
            heap_bh76_w5_1_d1 <=  heap_bh76_w5_1;
            heap_bh76_w4_1_d1 <=  heap_bh76_w4_1;
            heap_bh76_w3_1_d1 <=  heap_bh76_w3_1;
            heap_bh76_w2_1_d1 <=  heap_bh76_w2_1;
            heap_bh76_w1_1_d1 <=  heap_bh76_w1_1;
            heap_bh76_w0_1_d1 <=  heap_bh76_w0_1;
         end if;
      end process;
   XX_m75 <= Y ;
   YY_m75 <= X ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh76_ch0_0 <= ("" & XX_m75(31 downto 8) & "") * ("" & YY_m75(6 downto 0) & "0000000000");
   heap_bh76_w38_0 <= DSP_bh76_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w37_0 <= DSP_bh76_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w36_0 <= DSP_bh76_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w35_0 <= DSP_bh76_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w34_0 <= DSP_bh76_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w33_0 <= DSP_bh76_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w32_0 <= DSP_bh76_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w31_0 <= DSP_bh76_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w30_0 <= DSP_bh76_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w29_0 <= DSP_bh76_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w28_0 <= DSP_bh76_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w27_0 <= DSP_bh76_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w26_0 <= DSP_bh76_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w25_0 <= DSP_bh76_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w24_0 <= DSP_bh76_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w23_0 <= DSP_bh76_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w22_0 <= DSP_bh76_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w21_0 <= DSP_bh76_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w20_0 <= DSP_bh76_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w19_0 <= DSP_bh76_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w18_0 <= DSP_bh76_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w17_0 <= DSP_bh76_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w16_0 <= DSP_bh76_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w15_0 <= DSP_bh76_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w14_0 <= DSP_bh76_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w13_0 <= DSP_bh76_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w12_0 <= DSP_bh76_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w11_0 <= DSP_bh76_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w10_0 <= DSP_bh76_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w9_0 <= DSP_bh76_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w8_0 <= DSP_bh76_ch0_0(10); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w7_0 <= DSP_bh76_ch0_0(9); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w6_0 <= DSP_bh76_ch0_0(8); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w5_0 <= DSP_bh76_ch0_0(7); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w4_0 <= DSP_bh76_ch0_0(6); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w3_0 <= DSP_bh76_ch0_0(5); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w2_0 <= DSP_bh76_ch0_0(4); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w1_0 <= DSP_bh76_ch0_0(3); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w0_0 <= DSP_bh76_ch0_0(2); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh76_ch1_0 <= ("" & XX_m75(7 downto 0) & "0000000000000000") * ("" & YY_m75(6 downto 0) & "0000000000");
   heap_bh76_w14_1 <= DSP_bh76_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w13_1 <= DSP_bh76_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w12_1 <= DSP_bh76_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w11_1 <= DSP_bh76_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w10_1 <= DSP_bh76_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w9_1 <= DSP_bh76_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w8_1 <= DSP_bh76_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w7_1 <= DSP_bh76_ch1_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w6_1 <= DSP_bh76_ch1_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w5_1 <= DSP_bh76_ch1_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w4_1 <= DSP_bh76_ch1_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w3_1 <= DSP_bh76_ch1_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w2_1 <= DSP_bh76_ch1_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w1_1 <= DSP_bh76_ch1_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh76_w0_1 <= DSP_bh76_ch1_0(26); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh76 <= "0" & heap_bh76_w38_0_d1 & heap_bh76_w37_0_d1 & heap_bh76_w36_0_d1 & heap_bh76_w35_0_d1 & heap_bh76_w34_0_d1 & heap_bh76_w33_0_d1 & heap_bh76_w32_0_d1 & heap_bh76_w31_0_d1 & heap_bh76_w30_0_d1 & heap_bh76_w29_0_d1 & heap_bh76_w28_0_d1 & heap_bh76_w27_0_d1 & heap_bh76_w26_0_d1 & heap_bh76_w25_0_d1 & heap_bh76_w24_0_d1 & heap_bh76_w23_0_d1 & heap_bh76_w22_0_d1 & heap_bh76_w21_0_d1 & heap_bh76_w20_0_d1 & heap_bh76_w19_0_d1 & heap_bh76_w18_0_d1 & heap_bh76_w17_0_d1 & heap_bh76_w16_0_d1 & heap_bh76_w15_0_d1 & heap_bh76_w14_1_d1 & heap_bh76_w13_1_d1 & heap_bh76_w12_1_d1 & heap_bh76_w11_1_d1 & heap_bh76_w10_1_d1 & heap_bh76_w9_1_d1 & heap_bh76_w8_1_d1 & heap_bh76_w7_1_d1 & heap_bh76_w6_1_d1 & heap_bh76_w5_1_d1 & heap_bh76_w4_1_d1 & heap_bh76_w3_1_d1 & heap_bh76_w2_1_d1 & heap_bh76_w1_1_d1 & heap_bh76_w0_1_d1;
   finalAdderIn1_bh76 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh76_w14_0_d1 & heap_bh76_w13_0_d1 & heap_bh76_w12_0_d1 & heap_bh76_w11_0_d1 & heap_bh76_w10_0_d1 & heap_bh76_w9_0_d1 & heap_bh76_w8_0_d1 & heap_bh76_w7_0_d1 & heap_bh76_w6_0_d1 & heap_bh76_w5_0_d1 & heap_bh76_w4_0_d1 & heap_bh76_w3_0_d1 & heap_bh76_w2_0_d1 & heap_bh76_w1_0_d1 & heap_bh76_w0_0_d1;
   finalAdderCin_bh76 <= '0';
   Adder_final76_0: IntAdder_40_f400_uid85  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh76,
                 R => finalAdderOut_bh76   ,
                 X => finalAdderIn0_bh76,
                 Y => finalAdderIn1_bh76);
   -- concatenate all the compressed chunks
   CompressionResult76 <= finalAdderOut_bh76;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult76(38 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_37_f400_uid93
--                     (IntAdderClassical_37_f400_uid95)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_37_f400_uid93 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(36 downto 0);
          Y : in  std_logic_vector(36 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of IntAdder_37_f400_uid93 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_37_f400_uid100
--                     (IntAdderClassical_37_f400_uid102)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_37_f400_uid100 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(36 downto 0);
          Y : in  std_logic_vector(36 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of IntAdder_37_f400_uid100 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                            IntSquarer_22_uid107
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca (2009)
--------------------------------------------------------------------------------
-- Pipeline depth: 4 cycles

library ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
library work;
entity IntSquarer_22_uid107 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(21 downto 0);
          R : out  std_logic_vector(43 downto 0)   );
end entity;

architecture arch of IntSquarer_22_uid107 is
signal x0_16, x0_16_d1 :  std_logic_vector(17 downto 0);
signal x17_32, x17_32_d1, x17_32_d2 :  std_logic_vector(17 downto 0);
signal x17_32_shr, x17_32_shr_d1 :  std_logic_vector(17 downto 0);
signal p0, p0_d1, p0_d2, p0_d3 :  std_logic_vector(35 downto 0);
signal p1_x2, p1_x2_d1 :  std_logic_vector(35 downto 0);
signal s1, s1_d1, s1_d2 :  std_logic_vector(35 downto 0);
signal p2, p2_d1 :  std_logic_vector(35 downto 0);
signal s2, s2_d1 :  std_logic_vector(35 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x0_16_d1 <=  x0_16;
            x17_32_d1 <=  x17_32;
            x17_32_d2 <=  x17_32_d1;
            x17_32_shr_d1 <=  x17_32_shr;
            p0_d1 <=  p0;
            p0_d2 <=  p0_d1;
            p0_d3 <=  p0_d2;
            p1_x2_d1 <=  p1_x2;
            s1_d1 <=  s1;
            s1_d2 <=  s1_d1;
            p2_d1 <=  p2;
            s2_d1 <=  s2;
         end if;
      end process;
   x0_16 <= "0" & X(16 downto 0);
   x17_32 <= "00" & "00000000000" & X(21 downto 17);
   x17_32_shr <= "0" & "00000000000" & X(21 downto 17) & "0";
   ----------------Synchro barrier, entering cycle 1----------------
   p0 <= x0_16_d1 * x0_16_d1;
   p1_x2 <= x17_32_shr_d1 * x0_16_d1;
   ----------------Synchro barrier, entering cycle 2----------------
   s1 <= p1_x2_d1 + ( "00000000000000000" & p0_d1(35 downto 17));
   p2 <= x17_32_d2 * x17_32_d2;
   ----------------Synchro barrier, entering cycle 3----------------
   s2 <= p2_d1 + ( "00000000000000000" & s1_d1(35 downto 17));
   ----------------Synchro barrier, entering cycle 4----------------
   R <= s2_d1(9 downto 0) & s1_d2(16 downto 0) & p0_d3(16 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_37_f400_uid110
--                    (IntAdderAlternative_37_f400_uid114)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_37_f400_uid110 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(36 downto 0);
          Y : in  std_logic_vector(36 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of IntAdder_37_f400_uid110 is
signal s_sum_l0_idx0 :  std_logic_vector(22 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(15 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(21 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(14 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(15 downto 0);
signal sum_l1_idx1 :  std_logic_vector(14 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(21 downto 0)) + ( "0" & Y(21 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(36 downto 22)) + ( "0" & Y(36 downto 22));
   sum_l0_idx0 <= s_sum_l0_idx0(21 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(22 downto 22);
   sum_l0_idx1 <= s_sum_l0_idx1(14 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(15 downto 15);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(14 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(15 downto 15);
   R <= sum_l1_idx1(14 downto 0) & sum_l0_idx0_d1(21 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_0_8_54
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_0_8_54 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          Y : out  std_logic_vector(53 downto 0)   );
end entity;

architecture arch of LogTable_0_8_54 is
   -- Build a 2-D array type for the RoM
   subtype word_t is std_logic_vector(26 downto 0);
   type memory_t is array(0 to 511) of word_t;
   function init_rom
      return memory_t is 
      variable tmp : memory_t := (
   "111111111110111111111100000",
   "111111111110111111111100000",
   "000000001111000001111100010",
   "000000011111000111111110101",
   "000000101111010010000101000",
   "000000111111100000010001100",
   "000001001111110010100110010",
   "000001100000001001000101010",
   "000001110000100011110000101",
   "000010000001000010101010110",
   "000010010001100101110101100",
   "000010100010001101010011011",
   "000010110010111001000110011",
   "000011000011101001010001000",
   "000011010100011101110101011",
   "000011100101010110110101111",
   "000011110110010100010100111",
   "000011110110010100010100111",
   "000100000111010110010100101",
   "000100011000011100110111110",
   "000100101001101000000000101",
   "000100111010110111110001101",
   "000101001100001100001101010",
   "000101011101100101010110010",
   "000101011101100101010110010",
   "000101101111000011001111000",
   "000110000000100101111010001",
   "000110010010001101011010010",
   "000110100011111001110010010",
   "000110110101101011000100100",
   "000110110101101011000100100",
   "000111000111100001010100000",
   "000111011001011100100011011",
   "000111101011011100110101100",
   "000111111101100010001101001",
   "000111111101100010001101001",
   "001000001111101100101101011",
   "001000100001111100011001000",
   "001000110100010001010010111",
   "001000110100010001010010111",
   "001001000110101011011110010",
   "001001011001001010111110000",
   "001001101011101111110101011",
   "001001101011101111110101011",
   "001001111110011010000111010",
   "001010010001001001110111000",
   "001010010001001001110111000",
   "001010100011111111000111110",
   "001010110110111001111100110",
   "001011001001111010011001011",
   "001011001001111010011001011",
   "001011011101000000100001000",
   "001011110000001100010111000",
   "001011110000001100010111000",
   "001100000011011101111110110",
   "001100010110110101011011110",
   "001100010110110101011011110",
   "001100101010010010110001101",
   "001100111101110110000100000",
   "001100111101110110000100000",
   "001101010001011111010110011",
   "001101100101001110101100101",
   "001101100101001110101100101",
   "001101111001000100001010011",
   "001110001100111111110011100",
   "001110001100111111110011100",
   "001110100001000001101011111",
   "001110110101001001110111011",
   "001110110101001001110111011",
   "001111001001011000011001111",
   "001111001001011000011001111",
   "001111011101101101010111110",
   "001111110010001000110100110",
   "001111110010001000110100110",
   "010000000110101010110101010",
   "010000011011010011011101100",
   "010000011011010011011101100",
   "010000110000000010110001100",
   "010000110000000010110001100",
   "010001000100111000110110000",
   "010001000100111000110110000",
   "010001011001110101101111001",
   "010001101110111001100001100",
   "010001101110111001100001100",
   "010010000100000100010001101",
   "010010000100000100010001101",
   "010010011001010110000100010",
   "010010011001010110000100010",
   "010010101110101110111101111",
   "010011000100001111000011100",
   "010011000100001111000011100",
   "010011011001110110011001110",
   "010011011001110110011001110",
   "010011101111100101000101110",
   "010011101111100101000101110",
   "010100000101011011001100100",
   "010100000101011011001100100",
   "010100011011011000110011000",
   "010100011011011000110011000",
   "010100110001011101111110011",
   "010100110001011101111110011",
   "010101000111101010110100000",
   "010101000111101010110100000",
   "010101011101111111011001010",
   "010101011101111111011001010",
   "010101110100011011110011011",
   "010101110100011011110011011",
   "010110001011000000001000001",
   "010110001011000000001000001",
   "010110100001101100011100111",
   "010110100001101100011100111",
   "010110111000100000110111100",
   "010110111000100000110111100",
   "010111001111011101011101110",
   "010111001111011101011101110",
   "010111100110100010010101101",
   "010111100110100010010101101",
   "010111111101101111100100111",
   "010111111101101111100100111",
   "011000010101000101010001110",
   "011000010101000101010001110",
   "011000101100100011100010101",
   "011000101100100011100010101",
   "011001000100001010011101100",
   "011001000100001010011101100",
   "011001000100001010011101100",
   "011001011011111010001001000",
   "011001011011111010001001000",
   "101101011100101010010011101",
   "101101101000101001111011101",
   "101101110100101011110100000",
   "101110000000101111111101100",
   "101110001100110110011000111",
   "101110011000111111000111010",
   "101110011000111111000111010",
   "101110100101001010001001010",
   "101110110001010111011111111",
   "101110111101100111001100000",
   "101111001001111001001110101",
   "101111010110001101101000011",
   "101111100010100100011010100",
   "101111100010100100011010100",
   "101111101110111101100101101",
   "101111111011011001001010111",
   "110000000111110111001011001",
   "110000010100010111100111010",
   "110000010100010111100111010",
   "110000100000111010100000011",
   "110000101101011111110111010",
   "110000111010000111101101001",
   "110001000110110010000010101",
   "110001000110110010000010101",
   "110001010011011110111001001",
   "110001100000001110010001011",
   "110001101101000000001100011",
   "110001111001110100101011010",
   "110001111001110100101011010",
   "110010000110101011101111000",
   "110010010011100101011000110",
   "110010100000100001101001011",
   "110010100000100001101001011",
   "110010101101100000100010000",
   "110010111010100010000011110",
   "110011000111100110001111101",
   "110011000111100110001111101",
   "110011010100101101000110110",
   "110011100001110110101010010",
   "110011101111000010111011001",
   "110011101111000010111011001",
   "110011111100010001111010101",
   "110100001001100011101001111",
   "110100010110111000001010000",
   "110100010110111000001010000",
   "110100100100001111011100001",
   "110100110001101001100001011",
   "110100110001101001100001011",
   "110100111111000110011011000",
   "110101001100100110001010010",
   "110101001100100110001010010",
   "110101011010001000110000001",
   "110101100111101110001110000",
   "110101110101010110100101001",
   "110101110101010110100101001",
   "110110000011000001110110100",
   "110110010000110000000011101",
   "110110010000110000000011101",
   "110110011110100001001101110",
   "110110101100010101010110000",
   "110110101100010101010110000",
   "110110111010001100011101110",
   "110111001000000110100110010",
   "110111001000000110100110010",
   "110111010110000011110000111",
   "110111100100000011111111000",
   "110111100100000011111111000",
   "110111110010000111010001110",
   "111000000000001101101010110",
   "111000000000001101101010110",
   "111000001110010111001011001",
   "111000001110010111001011001",
   "111000011100100011110100100",
   "111000101010110011101000001",
   "111000101010110011101000001",
   "111000111001000110100111011",
   "111001000111011100110011110",
   "111001000111011100110011110",
   "111001010101110110001110110",
   "111001010101110110001110110",
   "111001100100010010111001101",
   "111001110010110010110110001",
   "111001110010110010110110001",
   "111010000001010110000101100",
   "111010000001010110000101100",
   "111010001111111100101001011",
   "111010011110100110100011010",
   "111010011110100110100011010",
   "111010101101010011110100110",
   "111010101101010011110100110",
   "111010111100000100011111010",
   "111011001010111000100100011",
   "111011001010111000100100011",
   "111011011001110000000101110",
   "111011011001110000000101110",
   "111011101000101011000101000",
   "111011101000101011000101000",
   "111011110111101001100011110",
   "111100000110101011100011100",
   "111100000110101011100011100",
   "111100010101110001000110001",
   "111100010101110001000110001",
   "111100100100111010001101010",
   "111100100100111010001101010",
   "111100110100000110111010011",
   "111100110100000110111010011",
   "111101000011010111001111100",
   "111101010010101011001110001",
   "111101010010101011001110001",
   "111101100010000010111000001",
   "111101100010000010111000001",
   "111101110001011110001111001",
   "111101110001011110001111001",
   "111110000000111101010101001",
   "111110000000111101010101001",
   "111110010000100000001011111",
   "111110010000100000001011111",
   "111110100000000110110101001",
   "111110100000000110110101001",
   "111110101111110001010010111",
   "111110101111110001010010111",
   "111110111111011111100110111",
   "111110111111011111100110111",
   "111111001111010001110011000",
   "111111001111010001110011000",
   "111111011111000111111001010",
   "111111011111000111111001010",
   "111111101111000001111011101",
   "000000000000000000000000000",
   "000000000000000000000000000",
   "101011001010110001000101101",
   "011101011000100011011110100",
   "101000111000100010100010101",
   "101100010010011010110000100",
   "010010110011000000011001100",
   "010100011001000110010101100",
   "110110000010100101010101111",
   "001001111001110100010001001",
   "101111100011010001100001000",
   "010100001011011110001111101",
   "110010110101001011101101011",
   "010100100111111011001101000",
   "010000111110110001000100000",
   "001101110111011011001011001",
   "000000000001101011100001111",
   "000000000001101011100001111",
   "101011001111000111010000010",
   "100010100011001010101011010",
   "001000100011100010111101001",
   "001111101000111101110011001",
   "111010010000001111110101011",
   "011011001011110010001110011",
   "011011001011110010001110011",
   "010101110101011000000111000",
   "011110100000011100011111100",
   "111010101100101001001100010",
   "000001011000110111100001110",
   "011011010110101011011000111",
   "011011010110101011011000111",
   "000011011110001001011001001",
   "000111000010001000110101001",
   "000110000101000010001001000",
   "110011101101111010101011001",
   "110011101101111010101011001",
   "010110011110001110100000011",
   "001000100111111001000111110",
   "111000100011111101110010101",
   "111000100011111101110010101",
   "101001001001110000100000001",
   "110010000110100000010011101",
   "000000010101100100000001011",
   "000000010101100100000001011",
   "010110011001001010001100110",
   "001100110011101101010110000",
   "001100110011101101010110000",
   "010010100001101101011000110",
   "101101010100001111011011001",
   "111010001100000100110011101",
   "111010001100000100110011101",
   "101101110101011010101011110",
   "010101000100010011000111010",
   "010101000100010011000111010",
   "010101010001101000111100001",
   "101100111000111111101000110",
   "101100111000111111101000110",
   "110011110111000000011000000",
   "011100001000100101100110010",
   "011100001000100101100110010",
   "110010001010110110011110010",
   "011101011011110011100011110",
   "011101011011110011100011110",
   "100000111011110110001010110",
   "011011110000000011110111000",
   "011011110000000011110111000",
   "001001100101010111100111110",
   "000011010100100010010100000",
   "000011010100100010010100000",
   "111111100111000100000001001",
   "111111100111000100000001001",
   "010011011100111111111110011",
   "110010110011101100110111101",
   "110010110011101100110111101",
   "110001001101100011010010001",
   "000010011010101100001100010",
   "000010011010101100001100010",
   "111011000010110001011100011",
   "111011000010110001011100011",
   "010001001111110010001111100",
   "010001001111110010001111100",
   "011101011001111101101110100",
   "011010110100110101110010101",
   "011010110100110101110010101",
   "101000011101011100011010100",
   "101000011101011100011010100",
   "001001101001101101110010010",
   "001001101001101101110010010",
   "100110111001001001101010100",
   "001110100110101110011100100",
   "001110100110101110011100100",
   "110101111100001000100010111",
   "110101111100001000100010111",
   "111001100110011000110000100",
   "111001100110011000110000100",
   "011110101011110100011000111",
   "011110101011110100011000111",
   "010011100011100010000011011",
   "010011100011100010000011011",
   "110000101110010110000111100",
   "110000101110010110000111100",
   "111001110001010001111011000",
   "111001110001010001111011000",
   "011110010001101000111111011",
   "011110010001101000111111011",
   "111010110010101111100110000",
   "111010110010101111100110000",
   "011001110101010110001001001",
   "011001110101010110001001001",
   "110100111000110101000010100",
   "110100111000110101000010100",
   "110101011110001100101110010",
   "110101011110001100101110010",
   "110110001100111101110110010",
   "110110001100111101110110010",
   "000011111001111101100111110",
   "000011111001111101100111110",
   "011110110000001010100011100",
   "011110110000001010100011100",
   "111011011011100101111111010",
   "111011011011100101111111010",
   "000100010110010110111110100",
   "000100010110010110111110100",
   "011010110111111011010001010",
   "011010110111111011010001010",
   "011010110111111011010001010",
   "011000100110101011010101110",
   "011000100110101011010101110",
   "100001000010111100101011100",
   "111001000001000100111000110",
   "100001100001000011010011010",
   "001101101110101110100001110",
   "110100101100110011000000101",
   "010001010111101110010010000",
   "010001010111101110010010000",
   "100010101000101100111110010",
   "101011011000101011101010000",
   "110010100011011010100101011",
   "000011001010100100010110101",
   "101100011000110111100011100",
   "000001100101010011011110011",
   "000001100101010011011110011",
   "011010010110010111110111111",
   "010010100101010111111100011",
   "001010100001110000011101011",
   "100110110100100001001100100",
   "100110110100100001001100100",
   "010000100011101001101100101",
   "110101010101101001011010000",
   "000111010101000011010001101",
   "111101010100000100111000001",
   "111101010100000100111000001",
   "010010110000010001000110111",
   "000111110110001110100010010",
   "100001100101011001011101111",
   "101001110011111001110100000",
   "101001110011111001110100000",
   "101111010010011100110010101",
   "000101110000010010100101110",
   "000101111111010000000000110",
   "000101111111010000000000110",
   "001101110111110100001110100",
   "000000011101010010101001110",
   "000110000010000001000111110",
   "000110000010000001000111110",
   "001100001011101010010100110",
   "000101110111100100101100000",
   "101011011111001101101111000",
   "101011011111001101101111000",
   "111010111100101010000001010",
   "110111101111001001101111100",
   "101010111111110010000101110",
   "101010111111110010000101110",
   "100011100110001011011110010",
   "110110001101010100101011000",
   "110110001101010100101011000",
   "111101011000011011000010110",
   "011001100111110111110111101",
   "011001100111110111110111101",
   "110001011110010010111100100",
   "110001100101101010100001101",
   "001100110100100000101110001",
   "001100110100100000101110001",
   "111100010011001110011101011",
   "111111100001011100001000110",
   "111111100001011100001000110",
   "011100011011100000000010110",
   "011111100000000010101101110",
   "011111100000000010101101110",
   "011011110101101001010011010",
   "101011010000100110000110000",
   "101011010000100110000110000",
   "101110011000101111010101010",
   "001100101111011100011010011",
   "001100101111011100011010011",
   "110100110101101001101001001",
   "011100010010000010101010001",
   "011100010010000010101010001",
   "111111110111010011101010110",
   "111111110111010011101010110",
   "100011101010100001101000110",
   "010011001001101001100101001",
   "010011001001101001100101001",
   "100001010010000111000101111",
   "101000100111100010010011000",
   "101000100111100010010011000",
   "001011011010100101010100111",
   "001011011010100101010100111",
   "110011101111111001100010101",
   "010011100111001100100111000",
   "010011100111001100100111000",
   "100101000010011101101001101",
   "100101000010011101101001101",
   "101010001101010010100101101",
   "101101100100010101111001111",
   "101101100100010101111001111",
   "000001111100111100111110000",
   "000001111100111100111110000",
   "000010101100110111001000100",
   "010011110010000101110001100",
   "010011110010000101110001100",
   "100001111010111101011111010",
   "100001111010111101011111010",
   "100010101110010000101001000",
   "100010101110010000101001000",
   "010100110011100011011100100",
   "111111111011101001110101011",
   "111111111011101001110101011",
   "110101001001001111010010110",
   "110101001001001111010010110",
   "001110111001101000111001000",
   "001110111001101000111001000",
   "110001001101110001110000011",
   "110001001101110001110000011",
   "001001110011010010001100000",
   "010000001101110001101100101",
   "010000001101110001101100101",
   "000110000000010100001010100",
   "000110000000010100001010100",
   "110110110111000010011010000",
   "110110110111000010011010000",
   "111000110000111110011010000",
   "111000110000111110011010000",
   "101100001010000011011100110",
   "101100001010000011011100110",
   "111100000101010010011101110",
   "111100000101010010011101110",
   "011110010111001010110101001",
   "011110010111001010110101001",
   "010011110000001111111100001",
   "010011110000001111111100001",
   "101000000111111011110101100",
   "101000000111111011110101100",
   "110010100111011111001100010",
   "110010100111011111001100010",
   "010101110101001110111101000",
      others => (others => '0'));
      	begin 
      return tmp;
      end init_rom;
	signal rom : memory_t := init_rom;
   signal Y1 :  std_logic_vector(26 downto 0);
   signal Y0 :  std_logic_vector(26 downto 0);
   signal Z1 :  std_logic_vector(8 downto 0);
   signal Z0 :  std_logic_vector(8 downto 0);
begin
Z0 <= '1' & X;
Z1 <= '0' & X;
	process(clk)
   begin
   if(rising_edge(clk)) then
   	Y1 <= rom(  TO_INTEGER(unsigned(Z1)));
   	Y0 <= rom(  TO_INTEGER(unsigned(Z0)));
   end if;
   end process;
    Y <= Y1 & Y0(26 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_1_6_48
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_1_6_48 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(47 downto 0)   );
end entity;

architecture arch of LogTable_1_6_48 is
begin
  with X select  Y <= 
   "000000100000000000000111111111111101010101010110" when "000000",
   "000001100000000000001000000000000010101010101100" when "000001",
   "000010100000000001001000000001001000000001010001" when "000010",
   "000011100000000011001000000101001101011111000111" when "000011",
   "000100100000000110001000001110010011010000001101" when "000100",
   "000101100000001010001000011110011001100110100111" when "000101",
   "000110100000001111001000110111100000111010010110" when "000110",
   "000111100000010101001001011011101001101001100000" when "000111",
   "001000100000011100001010001100110100011000001011" when "001000",
   "001001100000100100001011001101000001110000100001" when "001001",
   "001010100000101101001100011110010010100010101110" when "001010",
   "001011100000110111001110000010100111100101000001" when "001011",
   "001100100001000010001111111100000001110011101100" when "001100",
   "001101100001001110010010001100100010010001001000" when "001101",
   "001110100001011011010100110110001010000101101110" when "001110",
   "001111100001101001010111111010111010011111111111" when "001111",
   "010000100001111000011011011100110100110100100000" when "010000",
   "010001100010001000011111011101111010011101111100" when "010001",
   "010010100010011001100100000000001100111101000010" when "010010",
   "010011100010101011101001000101101101111000101001" when "010011",
   "010100100010111110101110110000011110111101101111" when "010100",
   "010101100011010010110101000010100001111111011000" when "010101",
   "010110100011100111111011111101111000110110101111" when "010110",
   "010111100011111110000011100100100101100011001000" when "010111",
   "011000100100010101001011111000101010001001111111" when "011000",
   "011001100100101101010100111100001000110110111000" when "011001",
   "011010100101000110011110110001000011111011011110" when "011010",
   "011011100101100000101001011001011101101111101011" when "011011",
   "011100100101111011110100110111011000110001011101" when "011100",
   "011101100110011000000001001100110111100100111110" when "011101",
   "011110100110110101001110011011111100110100100101" when "011110",
   "011111100111010011011100100110101011010000101111" when "011111",
   "100000000111100010111100000011000010110100011000" when "100000",
   "100001001000000010101011101011000100011011110100" when "100001",
   "100010001000100011011100010011110110100100000101" when "100010",
   "100011001001000101001101111111011100010011010100" when "100011",
   "100100001001101000000000101111111000110101110111" when "100100",
   "100101001010001011110100100111001111011110001111" when "100101",
   "100110001010110000101001100111100011100101001101" when "100110",
   "100111001011010110011111110010111000101001101110" when "100111",
   "101000001011111101010111001011010010010000111110" when "101000",
   "101001001100100101001111110010110100000110010111" when "101001",
   "101010001101001110001001101011100001111011100011" when "101010",
   "101011001101111000000100110111011111101000011011" when "101011",
   "101100001110100011000001011000110001001011000110" when "101100",
   "101101001111001110111111010001011010100111111110" when "101101",
   "101110001111111011111110100011100000001001101100" when "101110",
   "101111010000101001111111010001000110000001001010" when "101111",
   "110000010001011001000001011100010000100101100011" when "110000",
   "110001010010001001000101000111000100010100010101" when "110001",
   "110010010010111010001010010011100101110001010000" when "110010",
   "110011010011101100010001000011111001100110010100" when "110011",
   "110100010100011111011001011010000100100011111000" when "110100",
   "110101010101010011100011011000001011100000100011" when "110101",
   "110110010110001000101111000000010011011001010001" when "110110",
   "110111010110111110111100010100100001010001010011" when "110111",
   "111000010111110110001011010110111010010010001101" when "111000",
   "111001011000101110011100001001100011101011111000" when "111001",
   "111010011001100111101110101110100010110100100100" when "111010",
   "111011011010100010000011000111111101001000110101" when "111011",
   "111100011011011101011001010111111000001011100110" when "111100",
   "111101011100011001110001100000011001100110001000" when "111101",
   "111110011101010111001011100011100111001000000100" when "111110",
   "111111011110010101100111100011100110100111011001" when "111111",
   "------------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_54_f400_uid133
--                    (IntAdderAlternative_54_f400_uid137)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_54_f400_uid133 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(53 downto 0);
          Y : in  std_logic_vector(53 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(53 downto 0)   );
end entity;

architecture arch of IntAdder_54_f400_uid133 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(12 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(11 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(12 downto 0);
signal sum_l1_idx1 :  std_logic_vector(11 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(53 downto 42)) + ( "0" & Y(53 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(11 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(12 downto 12);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(11 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(12 downto 12);
   R <= sum_l1_idx1(11 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              LogTable_2_7_43
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity LogTable_2_7_43 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6 downto 0);
          Y : out  std_logic_vector(42 downto 0)   );
end entity;

architecture arch of LogTable_2_7_43 is
begin
  with X select  Y <= 
   "0000000000000000000000000000000001000000000" when "0000000",
   "0000001000000000000000000011100001000000000" when "0000001",
   "0000010000000000000000001111000001000000010" when "0000010",
   "0000011000000000000000100010100001000001000" when "0000011",
   "0000100000000000000000111110000001000010100" when "0000100",
   "0000101000000000000001100001100001000101000" when "0000101",
   "0000110000000000000010001101000001001000110" when "0000110",
   "0000111000000000000011000000100001001101111" when "0000111",
   "0001000000000000000011111100000001010100111" when "0001000",
   "0001001000000000000100111111100001011101110" when "0001001",
   "0001010000000000000110001011000001101000111" when "0001010",
   "0001011000000000000111011110100001110110100" when "0001011",
   "0001100000000000001000111010000010000110111" when "0001100",
   "0001101000000000001010011101100010011010010" when "0001101",
   "0001110000000000001100001001000010110000110" when "0001110",
   "0001111000000000001101111100100011001010111" when "0001111",
   "0010000000000000001111111000000011101000110" when "0010000",
   "0010001000000000010001111011100100001010100" when "0010001",
   "0010010000000000010100000111000100110000100" when "0010010",
   "0010011000000000010110011010100101011011000" when "0010011",
   "0010100000000000011000110110000110001010010" when "0010100",
   "0010101000000000011011011001100110111110100" when "0010101",
   "0010110000000000011110000101000111111000000" when "0010110",
   "0010111000000000100000111000101000110110111" when "0010111",
   "0011000000000000100011110100001001111011100" when "0011000",
   "0011001000000000100110110111101011000110010" when "0011001",
   "0011010000000000101010000011001100010111001" when "0011010",
   "0011011000000000101101010110101101101110100" when "0011011",
   "0011100000000000110000110010001111001100101" when "0011100",
   "0011101000000000110100010101110000110001110" when "0011101",
   "0011110000000000111000000001010010011110001" when "0011110",
   "0011111000000000111011110100110100010001111" when "0011111",
   "0100000000000000111111110000010110001101100" when "0100000",
   "0100001000000001000011110011111000010001000" when "0100001",
   "0100010000000001000111111111011010011100110" when "0100010",
   "0100011000000001001100010010111100110001000" when "0100011",
   "0100100000000001010000101110011111001110000" when "0100100",
   "0100101000000001010101010010000001110100000" when "0100101",
   "0100110000000001011001111101100100100011010" when "0100110",
   "0100111000000001011110110001000111011100000" when "0100111",
   "0101000000000001100011101100101010011110100" when "0101000",
   "0101001000000001101000110000001101101011000" when "0101001",
   "0101010000000001101101111011110001000001101" when "0101010",
   "0101011000000001110011001111010100100010110" when "0101011",
   "0101100000000001111000101010111000001110110" when "0101100",
   "0101101000000001111110001110011100000101100" when "0101101",
   "0101110000000010000011111010000000000111110" when "0101110",
   "0101111000000010001001101101100100010101010" when "0101111",
   "0110000000000010001111101001001000101110101" when "0110000",
   "0110001000000010010101101100101101010100000" when "0110001",
   "0110010000000010011011111000010010000101100" when "0110010",
   "0110011000000010100010001011110111000011101" when "0110011",
   "0110100000000010101000100111011100001110100" when "0110100",
   "0110101000000010101111001011000001100110010" when "0110101",
   "0110110000000010110101110110100111001011010" when "0110110",
   "0110111000000010111100101010001100111101110" when "0110111",
   "0111000000000011000011100101110010111110000" when "0111000",
   "0111001000000011001010101001011001001100010" when "0111001",
   "0111010000000011010001110100111111101000110" when "0111010",
   "0111011000000011011001001000100110010011110" when "0111011",
   "0111100000000011100000100100001101001101100" when "0111100",
   "0111101000000011101000000111110100010110001" when "0111101",
   "0111110000000011101111110011011011101110000" when "0111110",
   "0111111000000011110111100111000011010101100" when "0111111",
   "1000000000000011111111100010101011001100110" when "1000000",
   "1000001000000100000111100110010011010011111" when "1000001",
   "1000010000000100001111110001111011101011010" when "1000010",
   "1000011000000100011000000101100100010011001" when "1000011",
   "1000100000000100100000100001001101001011110" when "1000100",
   "1000101000000100101001000100110110010101011" when "1000101",
   "1000110000000100110001110000011111110000010" when "1000110",
   "1000111000000100111010100100001001011100101" when "1000111",
   "1001000000000101000011011111110011011010110" when "1001000",
   "1001001000000101001100100011011101101010111" when "1001001",
   "1001010000000101010101101111001000001101001" when "1001010",
   "1001011000000101011111000010110011000010000" when "1001011",
   "1001100000000101101000011110011110001001100" when "1001100",
   "1001101000000101110010000010001001100100001" when "1001101",
   "1001110000000101111011101101110101010001111" when "1001110",
   "1001111000000110000101100001100001010011010" when "1001111",
   "1010000000000110001111011101001101101000010" when "1010000",
   "1010001000000110011001100000111010010001010" when "1010001",
   "1010010000000110100011101100100111001110100" when "1010010",
   "1010011000000110101110000000010100100000011" when "1010011",
   "1010100000000110111000011100000010000110111" when "1010100",
   "1010101000000111000010111111110000000010011" when "1010101",
   "1010110000000111001101101011011110010011001" when "1010110",
   "1010111000000111011000011111001100111001011" when "1010111",
   "1011000000000111100011011010111011110101011" when "1011000",
   "1011001000000111101110011110101011000111011" when "1011001",
   "1011010000000111111001101010011010101111101" when "1011010",
   "1011011000001000000100111110001010101110010" when "1011011",
   "1011100000001000010000011001111011000011110" when "1011100",
   "1011101000001000011011111101101011110000010" when "1011101",
   "1011110000001000100111101001011100110100000" when "1011110",
   "1011111000001000110011011101001110001111001" when "1011111",
   "1100000000001000111111011001000000000010001" when "1100000",
   "1100001000001001001011011100110010001101001" when "1100001",
   "1100010000001001010111101000100100110000011" when "1100010",
   "1100011000001001100011111100010111101100000" when "1100011",
   "1100100000001001110000011000001011000000100" when "1100100",
   "1100101000001001111100111011111110101110000" when "1100101",
   "1100110000001010001001100111110010110100101" when "1100110",
   "1100111000001010010110011011100111010100111" when "1100111",
   "1101000000001010100011010111011100001110110" when "1101000",
   "1101001000001010110000011011010001100010110" when "1101001",
   "1101010000001010111101100111000111010001000" when "1101010",
   "1101011000001011001010111010111101011001101" when "1101011",
   "1101100000001011011000010110110011111101001" when "1101100",
   "1101101000001011100101111010101010111011101" when "1101101",
   "1101110000001011110011100110100010010101010" when "1101110",
   "1101111000001100000001011010011010001010100" when "1101111",
   "1110000000001100001111010110010010011011100" when "1110000",
   "1110001000001100011101011010001011001000011" when "1110001",
   "1110010000001100101011100110000100010001101" when "1110010",
   "1110011000001100111001111001111101110111011" when "1110011",
   "1110100000001101001000010101110111111001110" when "1110100",
   "1110101000001101010110111001110010011001010" when "1110101",
   "1110110000001101100101100101101101010110000" when "1110110",
   "1110111000001101110100011001101000110000010" when "1110111",
   "1111000000001110000011010101100100101000010" when "1111000",
   "1111001000001110010010011001100000111110010" when "1111001",
   "1111010000001110100001100101011101110010100" when "1111010",
   "1111011000001110110000111001011011000101010" when "1111011",
   "1111100000001111000000010101011000110110110" when "1111100",
   "1111101000001111001111111001010111000111010" when "1111101",
   "1111110000001111011111100101010101110111000" when "1111110",
   "1111111000001111101111011001010101000110010" when "1111111",
   "-------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_54_f400_uid142
--                    (IntAdderAlternative_54_f400_uid146)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_54_f400_uid142 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(53 downto 0);
          Y : in  std_logic_vector(53 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(53 downto 0)   );
end entity;

architecture arch of IntAdder_54_f400_uid142 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(12 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(11 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(12 downto 0);
signal sum_l1_idx1 :  std_logic_vector(11 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(53 downto 42)) + ( "0" & Y(53 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(11 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(12 downto 12);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(11 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(12 downto 12);
   R <= sum_l1_idx1(11 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_54_f400_uid149
--                    (IntAdderAlternative_54_f400_uid153)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_54_f400_uid149 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(53 downto 0);
          Y : in  std_logic_vector(53 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(53 downto 0)   );
end entity;

architecture arch of IntAdder_54_f400_uid149 is
signal s_sum_l0_idx0 :  std_logic_vector(42 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(12 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(41 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(11 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(12 downto 0);
signal sum_l1_idx1 :  std_logic_vector(11 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(41 downto 0)) + ( "0" & Y(41 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(53 downto 42)) + ( "0" & Y(53 downto 42));
   sum_l0_idx0 <= s_sum_l0_idx0(41 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(42 downto 42);
   sum_l0_idx1 <= s_sum_l0_idx1(11 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(12 downto 12);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(11 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(12 downto 12);
   R <= sum_l1_idx1(11 downto 0) & sum_l0_idx0_d1(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                      KCMTable_6_95265423098_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity KCMTable_6_95265423098_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(42 downto 0)   );
end entity;

architecture arch of KCMTable_6_95265423098_unsigned is
begin
  with X select  Y <= 
   "0000000000000000000000000000000000000000000" when "000000",
   "0000001011000101110010000101111111011111010" when "000001",
   "0000010110001011100100001011111110111110100" when "000010",
   "0000100001010001010110010001111110011101110" when "000011",
   "0000101100010111001000010111111101111101000" when "000100",
   "0000110111011100111010011101111101011100010" when "000101",
   "0001000010100010101100100011111100111011100" when "000110",
   "0001001101101000011110101001111100011010110" when "000111",
   "0001011000101110010000101111111011111010000" when "001000",
   "0001100011110100000010110101111011011001010" when "001001",
   "0001101110111001110100111011111010111000100" when "001010",
   "0001111001111111100111000001111010010111110" when "001011",
   "0010000101000101011001000111111001110111000" when "001100",
   "0010010000001011001011001101111001010110010" when "001101",
   "0010011011010000111101010011111000110101100" when "001110",
   "0010100110010110101111011001111000010100110" when "001111",
   "0010110001011100100001011111110111110100000" when "010000",
   "0010111100100010010011100101110111010011010" when "010001",
   "0011000111101000000101101011110110110010100" when "010010",
   "0011010010101101110111110001110110010001110" when "010011",
   "0011011101110011101001110111110101110001000" when "010100",
   "0011101000111001011011111101110101010000010" when "010101",
   "0011110011111111001110000011110100101111100" when "010110",
   "0011111111000101000000001001110100001110110" when "010111",
   "0100001010001010110010001111110011101110000" when "011000",
   "0100010101010000100100010101110011001101010" when "011001",
   "0100100000010110010110011011110010101100100" when "011010",
   "0100101011011100001000100001110010001011110" when "011011",
   "0100110110100001111010100111110001101011000" when "011100",
   "0101000001100111101100101101110001001010010" when "011101",
   "0101001100101101011110110011110000101001100" when "011110",
   "0101010111110011010000111001110000001000110" when "011111",
   "0101100010111001000010111111101111101000000" when "100000",
   "0101101101111110110101000101101111000111010" when "100001",
   "0101111001000100100111001011101110100110100" when "100010",
   "0110000100001010011001010001101110000101110" when "100011",
   "0110001111010000001011010111101101100101000" when "100100",
   "0110011010010101111101011101101101000100010" when "100101",
   "0110100101011011101111100011101100100011100" when "100110",
   "0110110000100001100001101001101100000010110" when "100111",
   "0110111011100111010011101111101011100010000" when "101000",
   "0111000110101101000101110101101011000001010" when "101001",
   "0111010001110010110111111011101010100000100" when "101010",
   "0111011100111000101010000001101001111111110" when "101011",
   "0111100111111110011100000111101001011111000" when "101100",
   "0111110011000100001110001101101000111110010" when "101101",
   "0111111110001010000000010011101000011101100" when "101110",
   "1000001001001111110010011001100111111100110" when "101111",
   "1000010100010101100100011111100111011100000" when "110000",
   "1000011111011011010110100101100110111011010" when "110001",
   "1000101010100001001000101011100110011010100" when "110010",
   "1000110101100110111010110001100101111001110" when "110011",
   "1001000000101100101100110111100101011001000" when "110100",
   "1001001011110010011110111101100100111000010" when "110101",
   "1001010110111000010001000011100100010111100" when "110110",
   "1001100001111110000011001001100011110110110" when "110111",
   "1001101101000011110101001111100011010110000" when "111000",
   "1001111000001001100111010101100010110101010" when "111001",
   "1010000011001111011001011011100010010100100" when "111010",
   "1010001110010101001011100001100001110011110" when "111011",
   "1010011001011010111101100111100001010011000" when "111100",
   "1010100100100000101111101101100000110010010" when "111101",
   "1010101111100110100001110011100000010001100" when "111110",
   "1010111010101100010011111001011111110000110" when "111111",
   "-------------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                      KCMTable_2_95265423098_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity KCMTable_2_95265423098_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of KCMTable_2_95265423098_unsigned is
begin
  with X select  Y <= 
   "000000000000000000000000000000000000000" when "00",
   "001011000101110010000101111111011111010" when "01",
   "010110001011100100001011111110111110100" when "10",
   "100001010001010110010001111110011101110" when "11",
   "---------------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_39_f400_uid161
--                    (IntAdderAlternative_39_f400_uid165)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_39_f400_uid161 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(38 downto 0);
          Y : in  std_logic_vector(38 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of IntAdder_39_f400_uid161 is
signal s_sum_l0_idx0 :  std_logic_vector(22 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(17 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(21 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(16 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(17 downto 0);
signal sum_l1_idx1 :  std_logic_vector(16 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(21 downto 0)) + ( "0" & Y(21 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(38 downto 22)) + ( "0" & Y(38 downto 22));
   sum_l0_idx0 <= s_sum_l0_idx0(21 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(22 downto 22);
   sum_l0_idx1 <= s_sum_l0_idx1(16 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(17 downto 17);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(16 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(17 downto 17);
   R <= sum_l1_idx1(16 downto 0) & sum_l0_idx0_d1(21 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                      IntIntKCM_8_95265423098_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2009,2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntIntKCM_8_95265423098_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          R : out  std_logic_vector(44 downto 0)   );
end entity;

architecture arch of IntIntKCM_8_95265423098_unsigned is
   component IntAdder_39_f400_uid161 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(38 downto 0);
             Y : in  std_logic_vector(38 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(38 downto 0)   );
   end component;

   component KCMTable_2_95265423098_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(38 downto 0)   );
   end component;

   component KCMTable_6_95265423098_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(42 downto 0)   );
   end component;

signal d0 :  std_logic_vector(5 downto 0);
signal pp0, pp0_d1 :  std_logic_vector(42 downto 0);
signal d1 :  std_logic_vector(1 downto 0);
signal pp1 :  std_logic_vector(38 downto 0);
signal addOp0 :  std_logic_vector(38 downto 0);
signal addOp1 :  std_logic_vector(38 downto 0);
signal OutRes :  std_logic_vector(38 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of KCMTable_2_95265423098_unsigned: component is "yes";
attribute rom_extract of KCMTable_6_95265423098_unsigned: component is "yes";
attribute rom_style of KCMTable_2_95265423098_unsigned: component is "distributed";
attribute rom_style of KCMTable_6_95265423098_unsigned: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            pp0_d1 <=  pp0;
         end if;
      end process;
   d0 <= X(5 downto 0);
   KCMTable_0: KCMTable_6_95265423098_unsigned  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0,
                 Y => pp0);
   d1 <= X(7 downto 6);
   KCMTable_1: KCMTable_2_95265423098_unsigned  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1,
                 Y => pp1);
   addOp0 <= (38 downto 37 => '0') & pp0(42 downto 6) & "";
   addOp1 <= pp1(38 downto 0) & "";
   Result_Adder: IntAdder_39_f400_uid161  -- pipelineDepth=1 maxInDelay=1.3068e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => OutRes,
                 X => addOp0,
                 Y => addOp1);
   ----------------Synchro barrier, entering cycle 1----------------
   R <= OutRes & pp0_d1(5 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_62_f400_uid169
--                    (IntAdderAlternative_62_f400_uid173)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_62_f400_uid169 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(61 downto 0);
          Y : in  std_logic_vector(61 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(61 downto 0)   );
end entity;

architecture arch of IntAdder_62_f400_uid169 is
signal s_sum_l0_idx0 :  std_logic_vector(33 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(29 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(32 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(28 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(29 downto 0);
signal sum_l1_idx1 :  std_logic_vector(28 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(32 downto 0)) + ( "0" & Y(32 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(61 downto 33)) + ( "0" & Y(61 downto 33));
   sum_l0_idx0 <= s_sum_l0_idx0(32 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(33 downto 33);
   sum_l0_idx1 <= s_sum_l0_idx1(28 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(29 downto 29);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(28 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(29 downto 29);
   R <= sum_l1_idx1(28 downto 0) & sum_l0_idx0_d1(32 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                   LZCShifter_62_to_54_counting_64_uid176
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Bogdan Pasca (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 6 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LZCShifter_62_to_54_counting_64_uid176 is
   port ( clk, rst : in std_logic;
          I : in  std_logic_vector(61 downto 0);
          Count : out  std_logic_vector(5 downto 0);
          O : out  std_logic_vector(53 downto 0)   );
end entity;

architecture arch of LZCShifter_62_to_54_counting_64_uid176 is
signal level6, level6_d1 :  std_logic_vector(61 downto 0);
signal count5, count5_d1, count5_d2, count5_d3, count5_d4, count5_d5 : std_logic;
signal level5, level5_d1 :  std_logic_vector(61 downto 0);
signal count4, count4_d1, count4_d2, count4_d3, count4_d4 : std_logic;
signal level4, level4_d1 :  std_logic_vector(61 downto 0);
signal count3, count3_d1, count3_d2, count3_d3 : std_logic;
signal level3, level3_d1 :  std_logic_vector(60 downto 0);
signal count2, count2_d1, count2_d2 : std_logic;
signal level2, level2_d1 :  std_logic_vector(56 downto 0);
signal count1, count1_d1 : std_logic;
signal level1, level1_d1 :  std_logic_vector(54 downto 0);
signal count0 : std_logic;
signal level0 :  std_logic_vector(53 downto 0);
signal sCount :  std_logic_vector(5 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level6_d1 <=  level6;
            count5_d1 <=  count5;
            count5_d2 <=  count5_d1;
            count5_d3 <=  count5_d2;
            count5_d4 <=  count5_d3;
            count5_d5 <=  count5_d4;
            level5_d1 <=  level5;
            count4_d1 <=  count4;
            count4_d2 <=  count4_d1;
            count4_d3 <=  count4_d2;
            count4_d4 <=  count4_d3;
            level4_d1 <=  level4;
            count3_d1 <=  count3;
            count3_d2 <=  count3_d1;
            count3_d3 <=  count3_d2;
            level3_d1 <=  level3;
            count2_d1 <=  count2;
            count2_d2 <=  count2_d1;
            level2_d1 <=  level2;
            count1_d1 <=  count1;
            level1_d1 <=  level1;
         end if;
      end process;
   level6 <= I ;
   ----------------Synchro barrier, entering cycle 1----------------
   count5<= '1' when level6_d1(61 downto 30) = (61 downto 30=>'0') else '0';
   level5<= level6_d1(61 downto 0) when count5='0' else level6_d1(29 downto 0) & (31 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 2----------------
   count4<= '1' when level5_d1(61 downto 46) = (61 downto 46=>'0') else '0';
   level4<= level5_d1(61 downto 0) when count4='0' else level5_d1(45 downto 0) & (15 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 3----------------
   count3<= '1' when level4_d1(61 downto 54) = (61 downto 54=>'0') else '0';
   level3<= level4_d1(61 downto 1) when count3='0' else level4_d1(53 downto 0) & (6 downto 0 => '0');

   ----------------Synchro barrier, entering cycle 4----------------
   count2<= '1' when level3_d1(60 downto 57) = (60 downto 57=>'0') else '0';
   level2<= level3_d1(60 downto 4) when count2='0' else level3_d1(56 downto 0);

   ----------------Synchro barrier, entering cycle 5----------------
   count1<= '1' when level2_d1(56 downto 55) = (56 downto 55=>'0') else '0';
   level1<= level2_d1(56 downto 2) when count1='0' else level2_d1(54 downto 0);

   ----------------Synchro barrier, entering cycle 6----------------
   count0<= '1' when level1_d1(54 downto 54) = (54 downto 54=>'0') else '0';
   level0<= level1_d1(54 downto 1) when count0='0' else level1_d1(53 downto 0);

   O <= level0;
   sCount <= count5_d5 & count4_d4 & count3_d3 & count2_d2 & count1_d1 & count0;
   Count <= sCount;
end architecture;

--------------------------------------------------------------------------------
--                      RightShifter_22_by_max_21_uid179
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity RightShifter_22_by_max_21_uid179 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(21 downto 0);
          S : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(42 downto 0)   );
end entity;

architecture arch of RightShifter_22_by_max_21_uid179 is
signal level0, level0_d1 :  std_logic_vector(21 downto 0);
signal ps, ps_d1 :  std_logic_vector(4 downto 0);
signal level1 :  std_logic_vector(22 downto 0);
signal level2 :  std_logic_vector(24 downto 0);
signal level3 :  std_logic_vector(28 downto 0);
signal level4 :  std_logic_vector(36 downto 0);
signal level5 :  std_logic_vector(52 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level0_d1 <=  level0;
            ps_d1 <=  ps;
         end if;
      end process;
   level0<= X;
   ps<= S;
   ----------------Synchro barrier, entering cycle 1----------------
   level1<=  (0 downto 0 => '0') & level0_d1 when ps_d1(0) = '1' else    level0_d1 & (0 downto 0 => '0');
   level2<=  (1 downto 0 => '0') & level1 when ps_d1(1) = '1' else    level1 & (1 downto 0 => '0');
   level3<=  (3 downto 0 => '0') & level2 when ps_d1(2) = '1' else    level2 & (3 downto 0 => '0');
   level4<=  (7 downto 0 => '0') & level3 when ps_d1(3) = '1' else    level3 & (7 downto 0 => '0');
   level5<=  (15 downto 0 => '0') & level4 when ps_d1(4) = '1' else    level4 & (15 downto 0 => '0');
   R <= level5(52 downto 10);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_39_f400_uid182
--                     (IntAdderClassical_39_f400_uid184)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_39_f400_uid182 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(38 downto 0);
          Y : in  std_logic_vector(38 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(38 downto 0)   );
end entity;

architecture arch of IntAdder_39_f400_uid182 is
signal X_d1 :  std_logic_vector(38 downto 0);
signal Y_d1 :  std_logic_vector(38 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_41_f400_uid189
--                    (IntAdderAlternative_41_f400_uid193)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_41_f400_uid189 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(40 downto 0);
          Y : in  std_logic_vector(40 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(40 downto 0)   );
end entity;

architecture arch of IntAdder_41_f400_uid189 is
signal s_sum_l0_idx0 :  std_logic_vector(38 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(3 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(37 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(2 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(3 downto 0);
signal sum_l1_idx1 :  std_logic_vector(2 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(37 downto 0)) + ( "0" & Y(37 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(40 downto 38)) + ( "0" & Y(40 downto 38));
   sum_l0_idx0 <= s_sum_l0_idx0(37 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(38 downto 38);
   sum_l0_idx1 <= s_sum_l0_idx1(2 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(3 downto 3);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(2 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(3 downto 3);
   R <= sum_l1_idx1(2 downto 0) & sum_l0_idx0_d1(37 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              FPLog_8_33_9_400
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, C. Klein  (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 23 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPLog_8_33_9_400 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8+33+2 downto 0);
          R : out  std_logic_vector(8+33+2 downto 0)   );
end entity;

architecture arch of FPLog_8_33_9_400 is
   component IntAdder_37_f400_uid100 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(36 downto 0);
             Y : in  std_logic_vector(36 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(36 downto 0)   );
   end component;

   component IntAdder_37_f400_uid110 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(36 downto 0);
             Y : in  std_logic_vector(36 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(36 downto 0)   );
   end component;

   component IntAdder_37_f400_uid93 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(36 downto 0);
             Y : in  std_logic_vector(36 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(36 downto 0)   );
   end component;

   component IntAdder_39_f400_uid182 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(38 downto 0);
             Y : in  std_logic_vector(38 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(38 downto 0)   );
   end component;

   component IntAdder_41_f400_uid189 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(40 downto 0);
             Y : in  std_logic_vector(40 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(40 downto 0)   );
   end component;

   component IntAdder_43_f400_uid60 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(42 downto 0);
             Y : in  std_logic_vector(42 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(42 downto 0)   );
   end component;

   component IntAdder_43_f400_uid67 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(42 downto 0);
             Y : in  std_logic_vector(42 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(42 downto 0)   );
   end component;

   component IntAdder_54_f400_uid133 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(53 downto 0);
             Y : in  std_logic_vector(53 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(53 downto 0)   );
   end component;

   component IntAdder_54_f400_uid142 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(53 downto 0);
             Y : in  std_logic_vector(53 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(53 downto 0)   );
   end component;

   component IntAdder_54_f400_uid149 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(53 downto 0);
             Y : in  std_logic_vector(53 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(53 downto 0)   );
   end component;

   component IntAdder_62_f400_uid169 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(61 downto 0);
             Y : in  std_logic_vector(61 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(61 downto 0)   );
   end component;

   component IntIntKCM_8_95265423098_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             R : out  std_logic_vector(44 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_6_36_0_unsigned_uid41 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : in  std_logic_vector(35 downto 0);
             R : out  std_logic_vector(41 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_7_32_0_unsigned_uid74 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(6 downto 0);
             Y : in  std_logic_vector(31 downto 0);
             R : out  std_logic_vector(38 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_9_35_0_unsigned_uid22 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8 downto 0);
             Y : in  std_logic_vector(34 downto 0);
             R : out  std_logic_vector(43 downto 0)   );
   end component;

   component IntSquarer_22_uid107 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(21 downto 0);
             R : out  std_logic_vector(43 downto 0)   );
   end component;

   component InvTable_0_8_9 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             Y : out  std_logic_vector(8 downto 0)   );
   end component;

   component LZCShifter_62_to_54_counting_64_uid176 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(61 downto 0);
             Count : out  std_logic_vector(5 downto 0);
             O : out  std_logic_vector(53 downto 0)   );
   end component;

   component LZOC_33_6_uid14 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(32 downto 0);
             OZB : in std_logic;
             O : out  std_logic_vector(5 downto 0)   );
   end component;

   component LeftShifter_18_by_max_18_uid17 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(17 downto 0);
             S : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(35 downto 0)   );
   end component;

   component LogTable_0_8_54 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             Y : out  std_logic_vector(53 downto 0)   );
   end component;

   component LogTable_1_6_48 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(47 downto 0)   );
   end component;

   component LogTable_2_7_43 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(6 downto 0);
             Y : out  std_logic_vector(42 downto 0)   );
   end component;

   component RightShifter_22_by_max_21_uid179 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(21 downto 0);
             S : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(42 downto 0)   );
   end component;

signal XExnSgn, XExnSgn_d1, XExnSgn_d2, XExnSgn_d3, XExnSgn_d4, XExnSgn_d5, XExnSgn_d6, XExnSgn_d7, XExnSgn_d8, XExnSgn_d9, XExnSgn_d10, XExnSgn_d11, XExnSgn_d12, XExnSgn_d13, XExnSgn_d14, XExnSgn_d15, XExnSgn_d16, XExnSgn_d17, XExnSgn_d18, XExnSgn_d19, XExnSgn_d20, XExnSgn_d21, XExnSgn_d22, XExnSgn_d23 :  std_logic_vector(2 downto 0);
signal FirstBit : std_logic;
signal Y0, Y0_d1, Y0_d2 :  std_logic_vector(34 downto 0);
signal Y0h :  std_logic_vector(32 downto 0);
signal sR, sR_d1, sR_d2, sR_d3, sR_d4, sR_d5, sR_d6, sR_d7, sR_d8, sR_d9, sR_d10, sR_d11, sR_d12, sR_d13, sR_d14, sR_d15, sR_d16, sR_d17, sR_d18, sR_d19, sR_d20, sR_d21, sR_d22, sR_d23 : std_logic;
signal absZ0, absZ0_d1, absZ0_d2, absZ0_d3, absZ0_d4, absZ0_d5 :  std_logic_vector(17 downto 0);
signal E, E_d1 :  std_logic_vector(7 downto 0);
signal absE, absE_d1, absE_d2, absE_d3, absE_d4, absE_d5, absE_d6, absE_d7, absE_d8, absE_d9, absE_d10, absE_d11, absE_d12 :  std_logic_vector(7 downto 0);
signal EeqZero, EeqZero_d1, EeqZero_d2, EeqZero_d3, EeqZero_d4 : std_logic;
signal lzo, lzo_d1, lzo_d2, lzo_d3, lzo_d4, lzo_d5, lzo_d6, lzo_d7, lzo_d8, lzo_d9, lzo_d10, lzo_d11, lzo_d12, lzo_d13, lzo_d14 :  std_logic_vector(5 downto 0);
signal pfinal_s :  std_logic_vector(5 downto 0);
signal shiftval :  std_logic_vector(6 downto 0);
signal shiftvalinL :  std_logic_vector(4 downto 0);
signal shiftvalinR, shiftvalinR_d1, shiftvalinR_d2, shiftvalinR_d3, shiftvalinR_d4, shiftvalinR_d5, shiftvalinR_d6, shiftvalinR_d7, shiftvalinR_d8, shiftvalinR_d9, shiftvalinR_d10 :  std_logic_vector(4 downto 0);
signal doRR, doRR_d1, doRR_d2 : std_logic;
signal small, small_d1, small_d2, small_d3, small_d4, small_d5, small_d6, small_d7, small_d8, small_d9, small_d10, small_d11, small_d12, small_d13, small_d14, small_d15, small_d16, small_d17, small_d18 : std_logic;
signal small_absZ0_normd_full :  std_logic_vector(35 downto 0);
signal small_absZ0_normd, small_absZ0_normd_d1, small_absZ0_normd_d2, small_absZ0_normd_d3, small_absZ0_normd_d4, small_absZ0_normd_d5, small_absZ0_normd_d6, small_absZ0_normd_d7, small_absZ0_normd_d8, small_absZ0_normd_d9, small_absZ0_normd_d10 :  std_logic_vector(17 downto 0);
signal A0, A0_d1, A0_d2, A0_d3, A0_d4 :  std_logic_vector(7 downto 0);
signal InvA0 :  std_logic_vector(8 downto 0);
signal P0 :  std_logic_vector(43 downto 0);
signal Z1 :  std_logic_vector(35 downto 0);
signal A1, A1_d1, A1_d2 :  std_logic_vector(5 downto 0);
signal B1 :  std_logic_vector(29 downto 0);
signal ZM1 :  std_logic_vector(35 downto 0);
signal P1 :  std_logic_vector(41 downto 0);
signal Y1 :  std_logic_vector(42 downto 0);
signal EiY1 :  std_logic_vector(42 downto 0);
signal addXIter1 :  std_logic_vector(42 downto 0);
signal EiYPB1 :  std_logic_vector(42 downto 0);
signal Pp1 :  std_logic_vector(42 downto 0);
signal Z2 :  std_logic_vector(42 downto 0);
signal A2, A2_d1, A2_d2, A2_d3, A2_d4 :  std_logic_vector(6 downto 0);
signal B2 :  std_logic_vector(35 downto 0);
signal ZM2 :  std_logic_vector(31 downto 0);
signal P2 :  std_logic_vector(38 downto 0);
signal Y2 :  std_logic_vector(54 downto 0);
signal EiY2 :  std_logic_vector(36 downto 0);
signal addXIter2 :  std_logic_vector(36 downto 0);
signal EiYPB2, EiYPB2_d1 :  std_logic_vector(36 downto 0);
signal Pp2 :  std_logic_vector(36 downto 0);
signal Z3 :  std_logic_vector(36 downto 0);
signal Zfinal, Zfinal_d1, Zfinal_d2, Zfinal_d3, Zfinal_d4, Zfinal_d5 :  std_logic_vector(36 downto 0);
signal squarerIn :  std_logic_vector(21 downto 0);
signal Z2o2_full :  std_logic_vector(43 downto 0);
signal Z2o2_full_dummy, Z2o2_full_dummy_d1, Z2o2_full_dummy_d2, Z2o2_full_dummy_d3, Z2o2_full_dummy_d4 :  std_logic_vector(43 downto 0);
signal Z2o2_normal :  std_logic_vector(18 downto 0);
signal addFinalLog1pY :  std_logic_vector(36 downto 0);
signal Log1p_normal, Log1p_normal_d1 :  std_logic_vector(36 downto 0);
signal L0 :  std_logic_vector(53 downto 0);
signal S1, S1_d1, S1_d2 :  std_logic_vector(53 downto 0);
signal L1 :  std_logic_vector(47 downto 0);
signal sopX1, sopX1_d1, sopX1_d2 :  std_logic_vector(53 downto 0);
signal S2, S2_d1, S2_d2, S2_d3 :  std_logic_vector(53 downto 0);
signal L2 :  std_logic_vector(42 downto 0);
signal sopX2, sopX2_d1, sopX2_d2 :  std_logic_vector(53 downto 0);
signal S3, S3_d1 :  std_logic_vector(53 downto 0);
signal almostLog :  std_logic_vector(53 downto 0);
signal adderLogF_normalY :  std_logic_vector(53 downto 0);
signal LogF_normal :  std_logic_vector(53 downto 0);
signal absELog2 :  std_logic_vector(44 downto 0);
signal absELog2_pad :  std_logic_vector(61 downto 0);
signal LogF_normal_pad :  std_logic_vector(61 downto 0);
signal lnaddX :  std_logic_vector(61 downto 0);
signal lnaddY :  std_logic_vector(61 downto 0);
signal Log_normal :  std_logic_vector(61 downto 0);
signal E_normal, E_normal_d1 :  std_logic_vector(5 downto 0);
signal Log_normal_normd, Log_normal_normd_d1, Log_normal_normd_d2 :  std_logic_vector(53 downto 0);
signal Z2o2_small_bs :  std_logic_vector(21 downto 0);
signal Z2o2_small_s :  std_logic_vector(42 downto 0);
signal Z2o2_small :  std_logic_vector(38 downto 0);
signal Z_small :  std_logic_vector(38 downto 0);
signal Log_smallY :  std_logic_vector(38 downto 0);
signal nsRCin : std_logic;
signal Log_small, Log_small_d1 :  std_logic_vector(38 downto 0);
signal E0_sub :  std_logic_vector(1 downto 0);
signal ufl, ufl_d1, ufl_d2, ufl_d3, ufl_d4, ufl_d5 : std_logic;
signal E_small, E_small_d1, E_small_d2, E_small_d3, E_small_d4 :  std_logic_vector(7 downto 0);
signal Log_small_normd, Log_small_normd_d1, Log_small_normd_d2, Log_small_normd_d3, Log_small_normd_d4, Log_small_normd_d5 :  std_logic_vector(36 downto 0);
signal E0offset :  std_logic_vector(7 downto 0);
signal ER :  std_logic_vector(7 downto 0);
signal Log_g, Log_g_d1 :  std_logic_vector(36 downto 0);
signal round, round_d1 : std_logic;
signal fraX :  std_logic_vector(40 downto 0);
signal fraY :  std_logic_vector(40 downto 0);
signal EFR :  std_logic_vector(40 downto 0);
constant g: positive := 4;
constant log2wF: positive := 6;
constant pfinal: positive := 17;
constant sfinal: positive := 37;
constant targetprec: positive := 54;
constant wE: positive := 8;
constant wF: positive := 33;
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of InvTable_0_8_9: component is "yes";
attribute rom_style of InvTable_0_8_9: component is "block";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            XExnSgn_d1 <=  XExnSgn;
            XExnSgn_d2 <=  XExnSgn_d1;
            XExnSgn_d3 <=  XExnSgn_d2;
            XExnSgn_d4 <=  XExnSgn_d3;
            XExnSgn_d5 <=  XExnSgn_d4;
            XExnSgn_d6 <=  XExnSgn_d5;
            XExnSgn_d7 <=  XExnSgn_d6;
            XExnSgn_d8 <=  XExnSgn_d7;
            XExnSgn_d9 <=  XExnSgn_d8;
            XExnSgn_d10 <=  XExnSgn_d9;
            XExnSgn_d11 <=  XExnSgn_d10;
            XExnSgn_d12 <=  XExnSgn_d11;
            XExnSgn_d13 <=  XExnSgn_d12;
            XExnSgn_d14 <=  XExnSgn_d13;
            XExnSgn_d15 <=  XExnSgn_d14;
            XExnSgn_d16 <=  XExnSgn_d15;
            XExnSgn_d17 <=  XExnSgn_d16;
            XExnSgn_d18 <=  XExnSgn_d17;
            XExnSgn_d19 <=  XExnSgn_d18;
            XExnSgn_d20 <=  XExnSgn_d19;
            XExnSgn_d21 <=  XExnSgn_d20;
            XExnSgn_d22 <=  XExnSgn_d21;
            XExnSgn_d23 <=  XExnSgn_d22;
            Y0_d1 <=  Y0;
            Y0_d2 <=  Y0_d1;
            sR_d1 <=  sR;
            sR_d2 <=  sR_d1;
            sR_d3 <=  sR_d2;
            sR_d4 <=  sR_d3;
            sR_d5 <=  sR_d4;
            sR_d6 <=  sR_d5;
            sR_d7 <=  sR_d6;
            sR_d8 <=  sR_d7;
            sR_d9 <=  sR_d8;
            sR_d10 <=  sR_d9;
            sR_d11 <=  sR_d10;
            sR_d12 <=  sR_d11;
            sR_d13 <=  sR_d12;
            sR_d14 <=  sR_d13;
            sR_d15 <=  sR_d14;
            sR_d16 <=  sR_d15;
            sR_d17 <=  sR_d16;
            sR_d18 <=  sR_d17;
            sR_d19 <=  sR_d18;
            sR_d20 <=  sR_d19;
            sR_d21 <=  sR_d20;
            sR_d22 <=  sR_d21;
            sR_d23 <=  sR_d22;
            absZ0_d1 <=  absZ0;
            absZ0_d2 <=  absZ0_d1;
            absZ0_d3 <=  absZ0_d2;
            absZ0_d4 <=  absZ0_d3;
            absZ0_d5 <=  absZ0_d4;
            E_d1 <=  E;
            absE_d1 <=  absE;
            absE_d2 <=  absE_d1;
            absE_d3 <=  absE_d2;
            absE_d4 <=  absE_d3;
            absE_d5 <=  absE_d4;
            absE_d6 <=  absE_d5;
            absE_d7 <=  absE_d6;
            absE_d8 <=  absE_d7;
            absE_d9 <=  absE_d8;
            absE_d10 <=  absE_d9;
            absE_d11 <=  absE_d10;
            absE_d12 <=  absE_d11;
            EeqZero_d1 <=  EeqZero;
            EeqZero_d2 <=  EeqZero_d1;
            EeqZero_d3 <=  EeqZero_d2;
            EeqZero_d4 <=  EeqZero_d3;
            lzo_d1 <=  lzo;
            lzo_d2 <=  lzo_d1;
            lzo_d3 <=  lzo_d2;
            lzo_d4 <=  lzo_d3;
            lzo_d5 <=  lzo_d4;
            lzo_d6 <=  lzo_d5;
            lzo_d7 <=  lzo_d6;
            lzo_d8 <=  lzo_d7;
            lzo_d9 <=  lzo_d8;
            lzo_d10 <=  lzo_d9;
            lzo_d11 <=  lzo_d10;
            lzo_d12 <=  lzo_d11;
            lzo_d13 <=  lzo_d12;
            lzo_d14 <=  lzo_d13;
            shiftvalinR_d1 <=  shiftvalinR;
            shiftvalinR_d2 <=  shiftvalinR_d1;
            shiftvalinR_d3 <=  shiftvalinR_d2;
            shiftvalinR_d4 <=  shiftvalinR_d3;
            shiftvalinR_d5 <=  shiftvalinR_d4;
            shiftvalinR_d6 <=  shiftvalinR_d5;
            shiftvalinR_d7 <=  shiftvalinR_d6;
            shiftvalinR_d8 <=  shiftvalinR_d7;
            shiftvalinR_d9 <=  shiftvalinR_d8;
            shiftvalinR_d10 <=  shiftvalinR_d9;
            doRR_d1 <=  doRR;
            doRR_d2 <=  doRR_d1;
            small_d1 <=  small;
            small_d2 <=  small_d1;
            small_d3 <=  small_d2;
            small_d4 <=  small_d3;
            small_d5 <=  small_d4;
            small_d6 <=  small_d5;
            small_d7 <=  small_d6;
            small_d8 <=  small_d7;
            small_d9 <=  small_d8;
            small_d10 <=  small_d9;
            small_d11 <=  small_d10;
            small_d12 <=  small_d11;
            small_d13 <=  small_d12;
            small_d14 <=  small_d13;
            small_d15 <=  small_d14;
            small_d16 <=  small_d15;
            small_d17 <=  small_d16;
            small_d18 <=  small_d17;
            small_absZ0_normd_d1 <=  small_absZ0_normd;
            small_absZ0_normd_d2 <=  small_absZ0_normd_d1;
            small_absZ0_normd_d3 <=  small_absZ0_normd_d2;
            small_absZ0_normd_d4 <=  small_absZ0_normd_d3;
            small_absZ0_normd_d5 <=  small_absZ0_normd_d4;
            small_absZ0_normd_d6 <=  small_absZ0_normd_d5;
            small_absZ0_normd_d7 <=  small_absZ0_normd_d6;
            small_absZ0_normd_d8 <=  small_absZ0_normd_d7;
            small_absZ0_normd_d9 <=  small_absZ0_normd_d8;
            small_absZ0_normd_d10 <=  small_absZ0_normd_d9;
            A0_d1 <=  A0;
            A0_d2 <=  A0_d1;
            A0_d3 <=  A0_d2;
            A0_d4 <=  A0_d3;
            A1_d1 <=  A1;
            A1_d2 <=  A1_d1;
            A2_d1 <=  A2;
            A2_d2 <=  A2_d1;
            A2_d3 <=  A2_d2;
            A2_d4 <=  A2_d3;
            EiYPB2_d1 <=  EiYPB2;
            Zfinal_d1 <=  Zfinal;
            Zfinal_d2 <=  Zfinal_d1;
            Zfinal_d3 <=  Zfinal_d2;
            Zfinal_d4 <=  Zfinal_d3;
            Zfinal_d5 <=  Zfinal_d4;
            Z2o2_full_dummy_d1 <=  Z2o2_full_dummy;
            Z2o2_full_dummy_d2 <=  Z2o2_full_dummy_d1;
            Z2o2_full_dummy_d3 <=  Z2o2_full_dummy_d2;
            Z2o2_full_dummy_d4 <=  Z2o2_full_dummy_d3;
            Log1p_normal_d1 <=  Log1p_normal;
            S1_d1 <=  S1;
            S1_d2 <=  S1_d1;
            sopX1_d1 <=  sopX1;
            sopX1_d2 <=  sopX1_d1;
            S2_d1 <=  S2;
            S2_d2 <=  S2_d1;
            S2_d3 <=  S2_d2;
            sopX2_d1 <=  sopX2;
            sopX2_d2 <=  sopX2_d1;
            S3_d1 <=  S3;
            E_normal_d1 <=  E_normal;
            Log_normal_normd_d1 <=  Log_normal_normd;
            Log_normal_normd_d2 <=  Log_normal_normd_d1;
            Log_small_d1 <=  Log_small;
            ufl_d1 <=  ufl;
            ufl_d2 <=  ufl_d1;
            ufl_d3 <=  ufl_d2;
            ufl_d4 <=  ufl_d3;
            ufl_d5 <=  ufl_d4;
            E_small_d1 <=  E_small;
            E_small_d2 <=  E_small_d1;
            E_small_d3 <=  E_small_d2;
            E_small_d4 <=  E_small_d3;
            Log_small_normd_d1 <=  Log_small_normd;
            Log_small_normd_d2 <=  Log_small_normd_d1;
            Log_small_normd_d3 <=  Log_small_normd_d2;
            Log_small_normd_d4 <=  Log_small_normd_d3;
            Log_small_normd_d5 <=  Log_small_normd_d4;
            Log_g_d1 <=  Log_g;
            round_d1 <=  round;
         end if;
      end process;
   XExnSgn <=  X(wE+wF+2 downto wE+wF);
   FirstBit <=  X(wF-1);
   Y0 <= "1" & X(wF-1 downto 0) & "0" when FirstBit = '0' else "01" & X(wF-1 downto 0);
   Y0h <= Y0(wF downto 1);
   -- Sign of the result;
   sR <= '0'   when  (X(wE+wF-1 downto wF) = ('0' & (wE-2 downto 0 => '1')))  -- binade [1..2)
     else not X(wE+wF-1);                -- MSB of exponent
   absZ0 <=   Y0(wF-pfinal+1 downto 0)          when (sR='0') else
             ((wF-pfinal+1 downto 0 => '0') - Y0(wF-pfinal+1 downto 0));
   E <= (X(wE+wF-1 downto wF)) - ("0" & (wE-2 downto 1 => '1') & (not FirstBit));
   ----------------Synchro barrier, entering cycle 1----------------
   absE <= ((wE-1 downto 0 => '0') - E_d1)   when sR_d1 = '1' else E_d1;
   EeqZero <= '1' when E_d1=(wE-1 downto 0 => '0') else '0';
   ---------------- cycle 0----------------
   lzoc1: LZOC_33_6_uid14  -- pipelineDepth=4 maxInDelay=5.46e-10
      port map ( clk  => clk,
                 rst  => rst,
                 I => Y0h,
                 O => lzo,
                 OZB => FirstBit);
   ---------------- cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   pfinal_s <= "010001";
   shiftval <= ('0' & lzo_d1) - ('0' & pfinal_s); 
   shiftvalinL <= shiftval(4 downto 0);
   shiftvalinR <= shiftval(4 downto 0);
   doRR <= shiftval(log2wF); -- sign of the result
   small <= EeqZero_d4 and not(doRR);
   ---------------- cycle 5----------------
   -- The left shifter for the 'small' case
   small_lshift: LeftShifter_18_by_max_18_uid17  -- pipelineDepth=1 maxInDelay=1.27372e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => small_absZ0_normd_full,
                 S => shiftvalinL,
                 X => absZ0_d5);
   ----------------Synchro barrier, entering cycle 6----------------
   small_absZ0_normd <= small_absZ0_normd_full(17 downto 0); -- get rid of leading zeroes
   ----------------Synchro barrier, entering cycle 0----------------
   ---------------- The range reduction box ---------------
   A0 <= X(32 downto 25);
   ----------------Synchro barrier, entering cycle 1----------------
   -- First inv table
   itO: InvTable_0_8_9  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A0_d1,
                 Y => InvA0);
   ----------------Synchro barrier, entering cycle 2----------------
   p0_mult: IntMultiplier_UsingDSP_9_35_0_unsigned_uid22  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => P0,
                 X => InvA0,
                 Y => Y0_d2);
   ----------------Synchro barrier, entering cycle 3----------------
   Z1 <= P0(35 downto 0);

   A1 <= Z1(35 downto 30);
   B1 <= Z1(29 downto 0);
   ZM1 <= Z1;
   p1_mult: IntMultiplier_UsingDSP_6_36_0_unsigned_uid41  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => P1,
                 X => A1,
                 Y => ZM1);

   ----------------Synchro barrier, entering cycle 4----------------
    -- delay at multiplier output is 0
   ---------------- cycle 3----------------
   Y1 <= "1" & (5 downto 0 => '0') & Z1;
   EiY1 <= Y1  when A1(5) = '1'
     else  "0" & Y1(42 downto 1);
   addXIter1 <= "0" & B1 & (11 downto 0 => '0');
   addIter1_1: IntAdder_43_f400_uid60  -- pipelineDepth=1 maxInDelay=8.6e-11
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB1,
                 X => addXIter1,
                 Y => EiY1);

   ----------------Synchro barrier, entering cycle 4----------------
   Pp1 <= (0 downto 0 => '1') & not(P1);
   addIter2_1: IntAdder_43_f400_uid67  -- pipelineDepth=1 maxInDelay=6.91e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z2,
                 X => EiYPB1,
                 Y => Pp1);

   ----------------Synchro barrier, entering cycle 5----------------
 -- the critical path at the adder output = 6.91e-10

   A2 <= Z2(42 downto 36);
   B2 <= Z2(35 downto 0);
   ZM2 <= Z2(42 downto 11);
   p2_mult: IntMultiplier_UsingDSP_7_32_0_unsigned_uid74  -- pipelineDepth=1 maxInDelay=6.91e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => P2,
                 X => A2,
                 Y => ZM2);

   ----------------Synchro barrier, entering cycle 6----------------
    -- delay at multiplier output is 0
   ---------------- cycle 5----------------
   Y2 <= "1" & (10 downto 0 => '0') & Z2;
   EiY2 <= (3 downto 0 => '0') & Y2(54 downto 22);
   addXIter2 <= "0" & B2;
   addIter1_2: IntAdder_37_f400_uid93  -- pipelineDepth=0 maxInDelay=6.91e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0',
                 R => EiYPB2,
                 X => addXIter2,
                 Y => EiY2);

   ----------------Synchro barrier, entering cycle 6----------------
   Pp2 <= (4 downto 0 => '1') & not(P2(38 downto 7));
   addIter2_2: IntAdder_37_f400_uid100  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Z3,
                 X => EiYPB2_d1,
                 Y => Pp2);

 -- the critical path at the adder output = 2.38172e-09
   Zfinal <= Z3;
   --  Synchro between RR box and case almost 1
   ----------------Synchro barrier, entering cycle 7----------------
   squarerIn <= Zfinal_d1(sfinal-1 downto sfinal-22) when doRR_d2='1'
                    else (small_absZ0_normd_d1 & (3 downto 0 => '0'));  
   squarer: IntSquarer_22_uid107  -- pipelineDepth=4 maxInDelay=9.6672e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => Z2o2_full,
                 X => squarerIn);
   ----------------Synchro barrier, entering cycle 11----------------
   Z2o2_full_dummy <= Z2o2_full;
   Z2o2_normal <= Z2o2_full_dummy (43  downto 25);
   addFinalLog1pY <= (pfinal downto 0  => '1') & not(Z2o2_normal);
   addFinalLog1p_normalAdder: IntAdder_37_f400_uid110  -- pipelineDepth=1 maxInDelay=1.31672e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1',
                 R => Log1p_normal,
                 X => Zfinal_d5,
                 Y => addFinalLog1pY);
   ----------------Synchro barrier, entering cycle 12----------------

   -- Now the log tables, as late as possible
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 6----------------
   ----------------Synchro barrier, entering cycle 7----------------
   ----------------Synchro barrier, entering cycle 8----------------
   ----------------Synchro barrier, entering cycle 4----------------
   -- First log table
   ltO: LogTable_0_8_54  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A0_d4,
                 Y => L0);
   ----------------Synchro barrier, entering cycle 5----------------
   S1 <= L0;
   lt1: LogTable_1_6_48  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A1_d2,
                 Y => L1);
   sopX1 <= ((53 downto 48 => '0') & L1);
   ----------------Synchro barrier, entering cycle 6----------------
   ----------------Synchro barrier, entering cycle 7----------------
   adderS1: IntAdder_54_f400_uid133  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S2,
                 X => S1_d2,
                 Y => sopX1_d2);

   ----------------Synchro barrier, entering cycle 8----------------
   ----------------Synchro barrier, entering cycle 9----------------
   lt2: LogTable_2_7_43  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => A2_d4,
                 Y => L2);
   sopX2 <= ((53 downto 43 => '0') & L2);
   ----------------Synchro barrier, entering cycle 10----------------
   ----------------Synchro barrier, entering cycle 11----------------
   adderS2: IntAdder_54_f400_uid142  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => S3,
                 X => S2_d3,
                 Y => sopX2_d2);

   ----------------Synchro barrier, entering cycle 12----------------
   ----------------Synchro barrier, entering cycle 13----------------
   almostLog <= S3_d1;
   adderLogF_normalY <= ((targetprec-1 downto sfinal => '0') & Log1p_normal_d1);
   adderLogF_normal: IntAdder_54_f400_uid149  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => LogF_normal,
                 X => almostLog,
                 Y => adderLogF_normalY);
   ----------------Synchro barrier, entering cycle 14----------------
   ----------------Synchro barrier, entering cycle 13----------------
   Log2KCM: IntIntKCM_8_95265423098_unsigned  -- pipelineDepth=1 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absELog2,
                 X => absE_d12);
   ----------------Synchro barrier, entering cycle 14----------------
   absELog2_pad <=   absELog2 & (targetprec-wF-g-1 downto 0 => '0');       
   LogF_normal_pad <= (wE-1  downto 0 => LogF_normal(targetprec-1))  & LogF_normal;
   lnaddX <= absELog2_pad;
   lnaddY <= LogF_normal_pad when sR_d14='0' else not(LogF_normal_pad); 
   lnadder: IntAdder_62_f400_uid169  -- pipelineDepth=1 maxInDelay=1.059e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => sR_d14,
                 R => Log_normal,
                 X => lnaddX,
                 Y => lnaddY);

   ----------------Synchro barrier, entering cycle 15----------------
   final_norm: LZCShifter_62_to_54_counting_64_uid176  -- pipelineDepth=6 maxInDelay=1.77972e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Count => E_normal,
                 I => Log_normal,
                 O => Log_normal_normd);
   Z2o2_small_bs <= Z2o2_full_dummy_d4(43 downto 22);
   ao_rshift: RightShifter_22_by_max_21_uid179  -- pipelineDepth=1 maxInDelay=1.38944e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => Z2o2_small_s,
                 S => shiftvalinR_d10,
                 X => Z2o2_small_bs);
   ---------------- cycle 16----------------
   -- output delay at shifter output is 1.4364e-09
     -- send the MSB to position pfinal
   Z2o2_small <=  (pfinal-1 downto 0  => '0') & Z2o2_small_s(42 downto 21);
   -- mantissa will be either Y0-z^2/2  or  -Y0+z^2/2,  depending on sR  
   Z_small <= small_absZ0_normd_d10 & (20 downto 0 => '0');
   Log_smallY <= Z2o2_small when sR_d16='1' else not(Z2o2_small);
   nsRCin <= not ( sR_d16 );
   log_small_adder: IntAdder_39_f400_uid182  -- pipelineDepth=1 maxInDelay=2.41184e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => nsRCin,
                 R => Log_small,
                 X => Z_small,
                 Y => Log_smallY);

   ----------------Synchro barrier, entering cycle 17----------------
 -- critical path here is 1.565e-09
   ----------------Synchro barrier, entering cycle 18----------------
   -- Possibly subtract 1 or 2 to the exponent, depending on the LZC of Log_small
   E0_sub <=   "11" when Log_small_d1(wF+g+1) = '1'
          else "10" when Log_small_d1(wF+g+1 downto wF+g) = "01"
          else "01" ;
   -- The smallest log will be log(1+2^{-wF}) \approx 2^{-wF}  = 2^-33
   -- The smallest representable number is 2^{1-2^(wE-1)} = 2^-127
   -- No underflow possible
   ufl <= '0';
   E_small <=  ("0" & (wE-2 downto 2 => '1') & E0_sub)  -  ((wE-1 downto 6 => '0') & lzo_d14) ;
   Log_small_normd <= Log_small_d1(wF+g+1 downto 2) when Log_small_d1(wF+g+1)='1'
           else Log_small_d1(wF+g downto 1)  when Log_small_d1(wF+g)='1'  -- remove the first zero
           else Log_small_d1(wF+g-1 downto 0)  ; -- remove two zeroes (extremely rare, 001000000 only)
   ----------------Synchro barrier, entering cycle 21----------------
   ----------------Synchro barrier, entering cycle 22----------------
   E0offset <= "10000110"; -- E0 + wE 
   ER <= E_small_d4(7 downto 0) when small_d17='1'
      else E0offset - ((7 downto 6 => '0') & E_normal_d1);
   ---------------- cycle 21----------------
   Log_g <=  Log_small_normd_d3(wF+g-2 downto 0) & "0" when small_d16='1'           -- remove implicit 1
      else Log_normal_normd(targetprec-2 downto targetprec-wF-g-1 );  -- remove implicit 1
   round <= Log_g(g-1) ; -- sticky is always 1 for a transcendental function 
   -- if round leads to a change of binade, the carry propagation magically updates both mantissa and exponent
   ----------------Synchro barrier, entering cycle 22----------------
   fraX <= (ER & Log_g_d1(wF+g-1 downto g)) ; 
   fraY <= ((wE+wF-1 downto 1 => '0') & round_d1); 
   finalRoundAdder: IntAdder_41_f400_uid189  -- pipelineDepth=1 maxInDelay=9.38e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => EFR,
                 X => fraX,
                 Y => fraY);
   ----------------Synchro barrier, entering cycle 23----------------
   R(wE+wF+2 downto wE+wF) <= "110" when ((XExnSgn_d23(2) and (XExnSgn_d23(1) or XExnSgn_d23(0))) or (XExnSgn_d23(1) and XExnSgn_d23(0))) = '1' else
                              "101" when XExnSgn_d23(2 downto 1) = "00"  else
                              "100" when XExnSgn_d23(2 downto 1) = "10"  else
                              "00" & sR_d23 when (((Log_normal_normd_d2(targetprec-1)='0') and (small_d18='0')) or ( (Log_small_normd_d5 (wF+g-1)='0') and (small_d18='1'))) or (ufl_d5 = '1') else
                               "01" & sR_d23;
   R(wE+wF-1 downto 0) <=  EFR;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_42_f400_uid221
--                     (IntAdderClassical_42_f400_uid223)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_42_f400_uid221 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(41 downto 0);
          Y : in  std_logic_vector(41 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(41 downto 0)   );
end entity;

architecture arch of IntAdder_42_f400_uid221 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--              IntMultiplier_UsingDSP_34_24_37_unsigned_uid199
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 5 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_34_24_37_unsigned_uid199 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          Y : in  std_logic_vector(23 downto 0);
          R : out  std_logic_vector(36 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_34_24_37_unsigned_uid199 is
   component Compressor_13_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component IntAdder_42_f400_uid221 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(41 downto 0);
             Y : in  std_logic_vector(41 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(41 downto 0)   );
   end component;

signal XX_m200 :  std_logic_vector(33 downto 0);
signal YY_m200 :  std_logic_vector(23 downto 0);
signal DSP_bh201_ch0_0 :  std_logic_vector(40 downto 0);
signal heap_bh201_w41_0, heap_bh201_w41_0_d1, heap_bh201_w41_0_d2, heap_bh201_w41_0_d3, heap_bh201_w41_0_d4, heap_bh201_w41_0_d5 : std_logic;
signal heap_bh201_w40_0, heap_bh201_w40_0_d1, heap_bh201_w40_0_d2, heap_bh201_w40_0_d3, heap_bh201_w40_0_d4, heap_bh201_w40_0_d5 : std_logic;
signal heap_bh201_w39_0, heap_bh201_w39_0_d1, heap_bh201_w39_0_d2, heap_bh201_w39_0_d3, heap_bh201_w39_0_d4, heap_bh201_w39_0_d5 : std_logic;
signal heap_bh201_w38_0, heap_bh201_w38_0_d1, heap_bh201_w38_0_d2, heap_bh201_w38_0_d3, heap_bh201_w38_0_d4, heap_bh201_w38_0_d5 : std_logic;
signal heap_bh201_w37_0, heap_bh201_w37_0_d1, heap_bh201_w37_0_d2, heap_bh201_w37_0_d3, heap_bh201_w37_0_d4, heap_bh201_w37_0_d5 : std_logic;
signal heap_bh201_w36_0, heap_bh201_w36_0_d1, heap_bh201_w36_0_d2, heap_bh201_w36_0_d3, heap_bh201_w36_0_d4, heap_bh201_w36_0_d5 : std_logic;
signal heap_bh201_w35_0, heap_bh201_w35_0_d1, heap_bh201_w35_0_d2, heap_bh201_w35_0_d3, heap_bh201_w35_0_d4, heap_bh201_w35_0_d5 : std_logic;
signal heap_bh201_w34_0, heap_bh201_w34_0_d1, heap_bh201_w34_0_d2, heap_bh201_w34_0_d3, heap_bh201_w34_0_d4, heap_bh201_w34_0_d5 : std_logic;
signal heap_bh201_w33_0, heap_bh201_w33_0_d1, heap_bh201_w33_0_d2, heap_bh201_w33_0_d3, heap_bh201_w33_0_d4, heap_bh201_w33_0_d5 : std_logic;
signal heap_bh201_w32_0, heap_bh201_w32_0_d1, heap_bh201_w32_0_d2, heap_bh201_w32_0_d3, heap_bh201_w32_0_d4, heap_bh201_w32_0_d5 : std_logic;
signal heap_bh201_w31_0, heap_bh201_w31_0_d1, heap_bh201_w31_0_d2, heap_bh201_w31_0_d3, heap_bh201_w31_0_d4, heap_bh201_w31_0_d5 : std_logic;
signal heap_bh201_w30_0, heap_bh201_w30_0_d1, heap_bh201_w30_0_d2, heap_bh201_w30_0_d3, heap_bh201_w30_0_d4, heap_bh201_w30_0_d5 : std_logic;
signal heap_bh201_w29_0, heap_bh201_w29_0_d1, heap_bh201_w29_0_d2, heap_bh201_w29_0_d3, heap_bh201_w29_0_d4, heap_bh201_w29_0_d5 : std_logic;
signal heap_bh201_w28_0, heap_bh201_w28_0_d1, heap_bh201_w28_0_d2, heap_bh201_w28_0_d3, heap_bh201_w28_0_d4, heap_bh201_w28_0_d5 : std_logic;
signal heap_bh201_w27_0, heap_bh201_w27_0_d1, heap_bh201_w27_0_d2, heap_bh201_w27_0_d3, heap_bh201_w27_0_d4, heap_bh201_w27_0_d5 : std_logic;
signal heap_bh201_w26_0, heap_bh201_w26_0_d1, heap_bh201_w26_0_d2, heap_bh201_w26_0_d3, heap_bh201_w26_0_d4, heap_bh201_w26_0_d5 : std_logic;
signal heap_bh201_w25_0, heap_bh201_w25_0_d1, heap_bh201_w25_0_d2, heap_bh201_w25_0_d3, heap_bh201_w25_0_d4 : std_logic;
signal heap_bh201_w24_0, heap_bh201_w24_0_d1, heap_bh201_w24_0_d2, heap_bh201_w24_0_d3, heap_bh201_w24_0_d4 : std_logic;
signal heap_bh201_w23_0, heap_bh201_w23_0_d1, heap_bh201_w23_0_d2, heap_bh201_w23_0_d3, heap_bh201_w23_0_d4 : std_logic;
signal heap_bh201_w22_0, heap_bh201_w22_0_d1, heap_bh201_w22_0_d2, heap_bh201_w22_0_d3, heap_bh201_w22_0_d4 : std_logic;
signal heap_bh201_w21_0, heap_bh201_w21_0_d1, heap_bh201_w21_0_d2, heap_bh201_w21_0_d3 : std_logic;
signal heap_bh201_w20_0, heap_bh201_w20_0_d1, heap_bh201_w20_0_d2, heap_bh201_w20_0_d3 : std_logic;
signal heap_bh201_w19_0, heap_bh201_w19_0_d1, heap_bh201_w19_0_d2, heap_bh201_w19_0_d3 : std_logic;
signal heap_bh201_w18_0, heap_bh201_w18_0_d1, heap_bh201_w18_0_d2, heap_bh201_w18_0_d3 : std_logic;
signal heap_bh201_w17_0, heap_bh201_w17_0_d1, heap_bh201_w17_0_d2, heap_bh201_w17_0_d3 : std_logic;
signal heap_bh201_w16_0, heap_bh201_w16_0_d1, heap_bh201_w16_0_d2, heap_bh201_w16_0_d3 : std_logic;
signal heap_bh201_w15_0, heap_bh201_w15_0_d1, heap_bh201_w15_0_d2 : std_logic;
signal heap_bh201_w14_0, heap_bh201_w14_0_d1, heap_bh201_w14_0_d2 : std_logic;
signal heap_bh201_w13_0, heap_bh201_w13_0_d1, heap_bh201_w13_0_d2 : std_logic;
signal heap_bh201_w12_0, heap_bh201_w12_0_d1, heap_bh201_w12_0_d2 : std_logic;
signal heap_bh201_w11_0, heap_bh201_w11_0_d1, heap_bh201_w11_0_d2 : std_logic;
signal heap_bh201_w10_0, heap_bh201_w10_0_d1, heap_bh201_w10_0_d2 : std_logic;
signal heap_bh201_w9_0, heap_bh201_w9_0_d1 : std_logic;
signal heap_bh201_w8_0, heap_bh201_w8_0_d1 : std_logic;
signal heap_bh201_w7_0, heap_bh201_w7_0_d1 : std_logic;
signal heap_bh201_w6_0, heap_bh201_w6_0_d1 : std_logic;
signal heap_bh201_w5_0, heap_bh201_w5_0_d1 : std_logic;
signal heap_bh201_w4_0, heap_bh201_w4_0_d1 : std_logic;
signal heap_bh201_w3_0, heap_bh201_w3_0_d1, heap_bh201_w3_0_d2, heap_bh201_w3_0_d3, heap_bh201_w3_0_d4, heap_bh201_w3_0_d5 : std_logic;
signal heap_bh201_w2_0, heap_bh201_w2_0_d1, heap_bh201_w2_0_d2, heap_bh201_w2_0_d3, heap_bh201_w2_0_d4, heap_bh201_w2_0_d5 : std_logic;
signal heap_bh201_w1_0, heap_bh201_w1_0_d1, heap_bh201_w1_0_d2, heap_bh201_w1_0_d3, heap_bh201_w1_0_d4, heap_bh201_w1_0_d5 : std_logic;
signal DSP_bh201_ch1_0 :  std_logic_vector(40 downto 0);
signal heap_bh201_w24_1, heap_bh201_w24_1_d1, heap_bh201_w24_1_d2, heap_bh201_w24_1_d3, heap_bh201_w24_1_d4 : std_logic;
signal heap_bh201_w23_1, heap_bh201_w23_1_d1, heap_bh201_w23_1_d2, heap_bh201_w23_1_d3, heap_bh201_w23_1_d4 : std_logic;
signal heap_bh201_w22_1, heap_bh201_w22_1_d1, heap_bh201_w22_1_d2, heap_bh201_w22_1_d3, heap_bh201_w22_1_d4 : std_logic;
signal heap_bh201_w21_1, heap_bh201_w21_1_d1, heap_bh201_w21_1_d2, heap_bh201_w21_1_d3 : std_logic;
signal heap_bh201_w20_1, heap_bh201_w20_1_d1, heap_bh201_w20_1_d2, heap_bh201_w20_1_d3 : std_logic;
signal heap_bh201_w19_1, heap_bh201_w19_1_d1, heap_bh201_w19_1_d2, heap_bh201_w19_1_d3 : std_logic;
signal heap_bh201_w18_1, heap_bh201_w18_1_d1, heap_bh201_w18_1_d2, heap_bh201_w18_1_d3 : std_logic;
signal heap_bh201_w17_1, heap_bh201_w17_1_d1, heap_bh201_w17_1_d2, heap_bh201_w17_1_d3 : std_logic;
signal heap_bh201_w16_1, heap_bh201_w16_1_d1, heap_bh201_w16_1_d2, heap_bh201_w16_1_d3 : std_logic;
signal heap_bh201_w15_1, heap_bh201_w15_1_d1, heap_bh201_w15_1_d2 : std_logic;
signal heap_bh201_w14_1, heap_bh201_w14_1_d1, heap_bh201_w14_1_d2 : std_logic;
signal heap_bh201_w13_1, heap_bh201_w13_1_d1, heap_bh201_w13_1_d2 : std_logic;
signal heap_bh201_w12_1, heap_bh201_w12_1_d1, heap_bh201_w12_1_d2 : std_logic;
signal heap_bh201_w11_1, heap_bh201_w11_1_d1, heap_bh201_w11_1_d2 : std_logic;
signal heap_bh201_w10_1, heap_bh201_w10_1_d1, heap_bh201_w10_1_d2 : std_logic;
signal heap_bh201_w9_1, heap_bh201_w9_1_d1 : std_logic;
signal heap_bh201_w8_1, heap_bh201_w8_1_d1 : std_logic;
signal heap_bh201_w7_1, heap_bh201_w7_1_d1 : std_logic;
signal heap_bh201_w6_1, heap_bh201_w6_1_d1 : std_logic;
signal heap_bh201_w5_1, heap_bh201_w5_1_d1 : std_logic;
signal heap_bh201_w4_1, heap_bh201_w4_1_d1 : std_logic;
signal heap_bh201_w3_1, heap_bh201_w3_1_d1, heap_bh201_w3_1_d2, heap_bh201_w3_1_d3, heap_bh201_w3_1_d4, heap_bh201_w3_1_d5 : std_logic;
signal heap_bh201_w2_1, heap_bh201_w2_1_d1, heap_bh201_w2_1_d2, heap_bh201_w2_1_d3, heap_bh201_w2_1_d4, heap_bh201_w2_1_d5 : std_logic;
signal heap_bh201_w1_1, heap_bh201_w1_1_d1, heap_bh201_w1_1_d2, heap_bh201_w1_1_d3, heap_bh201_w1_1_d4, heap_bh201_w1_1_d5 : std_logic;
signal heap_bh201_w0_0 : std_logic;
signal heap_bh201_w4_2, heap_bh201_w4_2_d1 : std_logic;
signal tempR_bh201_0, tempR_bh201_0_d1, tempR_bh201_0_d2, tempR_bh201_0_d3, tempR_bh201_0_d4, tempR_bh201_0_d5 : std_logic;
signal CompressorIn_bh201_0_0 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_0_1 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh201_w4_3, heap_bh201_w4_3_d1, heap_bh201_w4_3_d2, heap_bh201_w4_3_d3, heap_bh201_w4_3_d4 : std_logic;
signal heap_bh201_w5_2, heap_bh201_w5_2_d1, heap_bh201_w5_2_d2, heap_bh201_w5_2_d3, heap_bh201_w5_2_d4 : std_logic;
signal heap_bh201_w6_2 : std_logic;
signal CompressorIn_bh201_1_2 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_1_3 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh201_w6_3, heap_bh201_w6_3_d1, heap_bh201_w6_3_d2, heap_bh201_w6_3_d3, heap_bh201_w6_3_d4 : std_logic;
signal heap_bh201_w7_2, heap_bh201_w7_2_d1, heap_bh201_w7_2_d2, heap_bh201_w7_2_d3, heap_bh201_w7_2_d4 : std_logic;
signal heap_bh201_w8_2 : std_logic;
signal CompressorIn_bh201_2_4 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_2_5 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh201_w8_3, heap_bh201_w8_3_d1, heap_bh201_w8_3_d2, heap_bh201_w8_3_d3, heap_bh201_w8_3_d4 : std_logic;
signal heap_bh201_w9_2, heap_bh201_w9_2_d1, heap_bh201_w9_2_d2, heap_bh201_w9_2_d3, heap_bh201_w9_2_d4 : std_logic;
signal heap_bh201_w10_2, heap_bh201_w10_2_d1 : std_logic;
signal CompressorIn_bh201_3_6 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_3_7 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh201_w10_3, heap_bh201_w10_3_d1, heap_bh201_w10_3_d2, heap_bh201_w10_3_d3 : std_logic;
signal heap_bh201_w11_2, heap_bh201_w11_2_d1, heap_bh201_w11_2_d2, heap_bh201_w11_2_d3 : std_logic;
signal heap_bh201_w12_2 : std_logic;
signal CompressorIn_bh201_4_8 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_4_9 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_4_4 :  std_logic_vector(2 downto 0);
signal heap_bh201_w12_3, heap_bh201_w12_3_d1, heap_bh201_w12_3_d2, heap_bh201_w12_3_d3 : std_logic;
signal heap_bh201_w13_2, heap_bh201_w13_2_d1, heap_bh201_w13_2_d2, heap_bh201_w13_2_d3 : std_logic;
signal heap_bh201_w14_2 : std_logic;
signal CompressorIn_bh201_5_10 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_5_11 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_5_5 :  std_logic_vector(2 downto 0);
signal heap_bh201_w14_3, heap_bh201_w14_3_d1, heap_bh201_w14_3_d2, heap_bh201_w14_3_d3 : std_logic;
signal heap_bh201_w15_2, heap_bh201_w15_2_d1, heap_bh201_w15_2_d2, heap_bh201_w15_2_d3 : std_logic;
signal heap_bh201_w16_2, heap_bh201_w16_2_d1 : std_logic;
signal CompressorIn_bh201_6_12 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_6_13 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_6_6 :  std_logic_vector(2 downto 0);
signal heap_bh201_w16_3, heap_bh201_w16_3_d1, heap_bh201_w16_3_d2 : std_logic;
signal heap_bh201_w17_2, heap_bh201_w17_2_d1, heap_bh201_w17_2_d2 : std_logic;
signal heap_bh201_w18_2 : std_logic;
signal CompressorIn_bh201_7_14 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_7_15 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_7_7 :  std_logic_vector(2 downto 0);
signal heap_bh201_w18_3, heap_bh201_w18_3_d1, heap_bh201_w18_3_d2 : std_logic;
signal heap_bh201_w19_2, heap_bh201_w19_2_d1, heap_bh201_w19_2_d2 : std_logic;
signal heap_bh201_w20_2 : std_logic;
signal CompressorIn_bh201_8_16 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_8_17 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_8_8 :  std_logic_vector(2 downto 0);
signal heap_bh201_w20_3, heap_bh201_w20_3_d1, heap_bh201_w20_3_d2 : std_logic;
signal heap_bh201_w21_2, heap_bh201_w21_2_d1, heap_bh201_w21_2_d2 : std_logic;
signal heap_bh201_w22_2, heap_bh201_w22_2_d1 : std_logic;
signal CompressorIn_bh201_9_18 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_9_19 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh201_9_9 :  std_logic_vector(2 downto 0);
signal heap_bh201_w22_3, heap_bh201_w22_3_d1 : std_logic;
signal heap_bh201_w23_2, heap_bh201_w23_2_d1 : std_logic;
signal heap_bh201_w24_2 : std_logic;
signal CompressorIn_bh201_10_20 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh201_10_21 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh201_10_10 :  std_logic_vector(2 downto 0);
signal heap_bh201_w24_3, heap_bh201_w24_3_d1 : std_logic;
signal heap_bh201_w25_1, heap_bh201_w25_1_d1 : std_logic;
signal heap_bh201_w26_1, heap_bh201_w26_1_d1 : std_logic;
signal finalAdderIn0_bh201 :  std_logic_vector(41 downto 0);
signal finalAdderIn1_bh201 :  std_logic_vector(41 downto 0);
signal finalAdderCin_bh201 : std_logic;
signal finalAdderOut_bh201 :  std_logic_vector(41 downto 0);
signal CompressionResult201 :  std_logic_vector(42 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh201_w41_0_d1 <=  heap_bh201_w41_0;
            heap_bh201_w41_0_d2 <=  heap_bh201_w41_0_d1;
            heap_bh201_w41_0_d3 <=  heap_bh201_w41_0_d2;
            heap_bh201_w41_0_d4 <=  heap_bh201_w41_0_d3;
            heap_bh201_w41_0_d5 <=  heap_bh201_w41_0_d4;
            heap_bh201_w40_0_d1 <=  heap_bh201_w40_0;
            heap_bh201_w40_0_d2 <=  heap_bh201_w40_0_d1;
            heap_bh201_w40_0_d3 <=  heap_bh201_w40_0_d2;
            heap_bh201_w40_0_d4 <=  heap_bh201_w40_0_d3;
            heap_bh201_w40_0_d5 <=  heap_bh201_w40_0_d4;
            heap_bh201_w39_0_d1 <=  heap_bh201_w39_0;
            heap_bh201_w39_0_d2 <=  heap_bh201_w39_0_d1;
            heap_bh201_w39_0_d3 <=  heap_bh201_w39_0_d2;
            heap_bh201_w39_0_d4 <=  heap_bh201_w39_0_d3;
            heap_bh201_w39_0_d5 <=  heap_bh201_w39_0_d4;
            heap_bh201_w38_0_d1 <=  heap_bh201_w38_0;
            heap_bh201_w38_0_d2 <=  heap_bh201_w38_0_d1;
            heap_bh201_w38_0_d3 <=  heap_bh201_w38_0_d2;
            heap_bh201_w38_0_d4 <=  heap_bh201_w38_0_d3;
            heap_bh201_w38_0_d5 <=  heap_bh201_w38_0_d4;
            heap_bh201_w37_0_d1 <=  heap_bh201_w37_0;
            heap_bh201_w37_0_d2 <=  heap_bh201_w37_0_d1;
            heap_bh201_w37_0_d3 <=  heap_bh201_w37_0_d2;
            heap_bh201_w37_0_d4 <=  heap_bh201_w37_0_d3;
            heap_bh201_w37_0_d5 <=  heap_bh201_w37_0_d4;
            heap_bh201_w36_0_d1 <=  heap_bh201_w36_0;
            heap_bh201_w36_0_d2 <=  heap_bh201_w36_0_d1;
            heap_bh201_w36_0_d3 <=  heap_bh201_w36_0_d2;
            heap_bh201_w36_0_d4 <=  heap_bh201_w36_0_d3;
            heap_bh201_w36_0_d5 <=  heap_bh201_w36_0_d4;
            heap_bh201_w35_0_d1 <=  heap_bh201_w35_0;
            heap_bh201_w35_0_d2 <=  heap_bh201_w35_0_d1;
            heap_bh201_w35_0_d3 <=  heap_bh201_w35_0_d2;
            heap_bh201_w35_0_d4 <=  heap_bh201_w35_0_d3;
            heap_bh201_w35_0_d5 <=  heap_bh201_w35_0_d4;
            heap_bh201_w34_0_d1 <=  heap_bh201_w34_0;
            heap_bh201_w34_0_d2 <=  heap_bh201_w34_0_d1;
            heap_bh201_w34_0_d3 <=  heap_bh201_w34_0_d2;
            heap_bh201_w34_0_d4 <=  heap_bh201_w34_0_d3;
            heap_bh201_w34_0_d5 <=  heap_bh201_w34_0_d4;
            heap_bh201_w33_0_d1 <=  heap_bh201_w33_0;
            heap_bh201_w33_0_d2 <=  heap_bh201_w33_0_d1;
            heap_bh201_w33_0_d3 <=  heap_bh201_w33_0_d2;
            heap_bh201_w33_0_d4 <=  heap_bh201_w33_0_d3;
            heap_bh201_w33_0_d5 <=  heap_bh201_w33_0_d4;
            heap_bh201_w32_0_d1 <=  heap_bh201_w32_0;
            heap_bh201_w32_0_d2 <=  heap_bh201_w32_0_d1;
            heap_bh201_w32_0_d3 <=  heap_bh201_w32_0_d2;
            heap_bh201_w32_0_d4 <=  heap_bh201_w32_0_d3;
            heap_bh201_w32_0_d5 <=  heap_bh201_w32_0_d4;
            heap_bh201_w31_0_d1 <=  heap_bh201_w31_0;
            heap_bh201_w31_0_d2 <=  heap_bh201_w31_0_d1;
            heap_bh201_w31_0_d3 <=  heap_bh201_w31_0_d2;
            heap_bh201_w31_0_d4 <=  heap_bh201_w31_0_d3;
            heap_bh201_w31_0_d5 <=  heap_bh201_w31_0_d4;
            heap_bh201_w30_0_d1 <=  heap_bh201_w30_0;
            heap_bh201_w30_0_d2 <=  heap_bh201_w30_0_d1;
            heap_bh201_w30_0_d3 <=  heap_bh201_w30_0_d2;
            heap_bh201_w30_0_d4 <=  heap_bh201_w30_0_d3;
            heap_bh201_w30_0_d5 <=  heap_bh201_w30_0_d4;
            heap_bh201_w29_0_d1 <=  heap_bh201_w29_0;
            heap_bh201_w29_0_d2 <=  heap_bh201_w29_0_d1;
            heap_bh201_w29_0_d3 <=  heap_bh201_w29_0_d2;
            heap_bh201_w29_0_d4 <=  heap_bh201_w29_0_d3;
            heap_bh201_w29_0_d5 <=  heap_bh201_w29_0_d4;
            heap_bh201_w28_0_d1 <=  heap_bh201_w28_0;
            heap_bh201_w28_0_d2 <=  heap_bh201_w28_0_d1;
            heap_bh201_w28_0_d3 <=  heap_bh201_w28_0_d2;
            heap_bh201_w28_0_d4 <=  heap_bh201_w28_0_d3;
            heap_bh201_w28_0_d5 <=  heap_bh201_w28_0_d4;
            heap_bh201_w27_0_d1 <=  heap_bh201_w27_0;
            heap_bh201_w27_0_d2 <=  heap_bh201_w27_0_d1;
            heap_bh201_w27_0_d3 <=  heap_bh201_w27_0_d2;
            heap_bh201_w27_0_d4 <=  heap_bh201_w27_0_d3;
            heap_bh201_w27_0_d5 <=  heap_bh201_w27_0_d4;
            heap_bh201_w26_0_d1 <=  heap_bh201_w26_0;
            heap_bh201_w26_0_d2 <=  heap_bh201_w26_0_d1;
            heap_bh201_w26_0_d3 <=  heap_bh201_w26_0_d2;
            heap_bh201_w26_0_d4 <=  heap_bh201_w26_0_d3;
            heap_bh201_w26_0_d5 <=  heap_bh201_w26_0_d4;
            heap_bh201_w25_0_d1 <=  heap_bh201_w25_0;
            heap_bh201_w25_0_d2 <=  heap_bh201_w25_0_d1;
            heap_bh201_w25_0_d3 <=  heap_bh201_w25_0_d2;
            heap_bh201_w25_0_d4 <=  heap_bh201_w25_0_d3;
            heap_bh201_w24_0_d1 <=  heap_bh201_w24_0;
            heap_bh201_w24_0_d2 <=  heap_bh201_w24_0_d1;
            heap_bh201_w24_0_d3 <=  heap_bh201_w24_0_d2;
            heap_bh201_w24_0_d4 <=  heap_bh201_w24_0_d3;
            heap_bh201_w23_0_d1 <=  heap_bh201_w23_0;
            heap_bh201_w23_0_d2 <=  heap_bh201_w23_0_d1;
            heap_bh201_w23_0_d3 <=  heap_bh201_w23_0_d2;
            heap_bh201_w23_0_d4 <=  heap_bh201_w23_0_d3;
            heap_bh201_w22_0_d1 <=  heap_bh201_w22_0;
            heap_bh201_w22_0_d2 <=  heap_bh201_w22_0_d1;
            heap_bh201_w22_0_d3 <=  heap_bh201_w22_0_d2;
            heap_bh201_w22_0_d4 <=  heap_bh201_w22_0_d3;
            heap_bh201_w21_0_d1 <=  heap_bh201_w21_0;
            heap_bh201_w21_0_d2 <=  heap_bh201_w21_0_d1;
            heap_bh201_w21_0_d3 <=  heap_bh201_w21_0_d2;
            heap_bh201_w20_0_d1 <=  heap_bh201_w20_0;
            heap_bh201_w20_0_d2 <=  heap_bh201_w20_0_d1;
            heap_bh201_w20_0_d3 <=  heap_bh201_w20_0_d2;
            heap_bh201_w19_0_d1 <=  heap_bh201_w19_0;
            heap_bh201_w19_0_d2 <=  heap_bh201_w19_0_d1;
            heap_bh201_w19_0_d3 <=  heap_bh201_w19_0_d2;
            heap_bh201_w18_0_d1 <=  heap_bh201_w18_0;
            heap_bh201_w18_0_d2 <=  heap_bh201_w18_0_d1;
            heap_bh201_w18_0_d3 <=  heap_bh201_w18_0_d2;
            heap_bh201_w17_0_d1 <=  heap_bh201_w17_0;
            heap_bh201_w17_0_d2 <=  heap_bh201_w17_0_d1;
            heap_bh201_w17_0_d3 <=  heap_bh201_w17_0_d2;
            heap_bh201_w16_0_d1 <=  heap_bh201_w16_0;
            heap_bh201_w16_0_d2 <=  heap_bh201_w16_0_d1;
            heap_bh201_w16_0_d3 <=  heap_bh201_w16_0_d2;
            heap_bh201_w15_0_d1 <=  heap_bh201_w15_0;
            heap_bh201_w15_0_d2 <=  heap_bh201_w15_0_d1;
            heap_bh201_w14_0_d1 <=  heap_bh201_w14_0;
            heap_bh201_w14_0_d2 <=  heap_bh201_w14_0_d1;
            heap_bh201_w13_0_d1 <=  heap_bh201_w13_0;
            heap_bh201_w13_0_d2 <=  heap_bh201_w13_0_d1;
            heap_bh201_w12_0_d1 <=  heap_bh201_w12_0;
            heap_bh201_w12_0_d2 <=  heap_bh201_w12_0_d1;
            heap_bh201_w11_0_d1 <=  heap_bh201_w11_0;
            heap_bh201_w11_0_d2 <=  heap_bh201_w11_0_d1;
            heap_bh201_w10_0_d1 <=  heap_bh201_w10_0;
            heap_bh201_w10_0_d2 <=  heap_bh201_w10_0_d1;
            heap_bh201_w9_0_d1 <=  heap_bh201_w9_0;
            heap_bh201_w8_0_d1 <=  heap_bh201_w8_0;
            heap_bh201_w7_0_d1 <=  heap_bh201_w7_0;
            heap_bh201_w6_0_d1 <=  heap_bh201_w6_0;
            heap_bh201_w5_0_d1 <=  heap_bh201_w5_0;
            heap_bh201_w4_0_d1 <=  heap_bh201_w4_0;
            heap_bh201_w3_0_d1 <=  heap_bh201_w3_0;
            heap_bh201_w3_0_d2 <=  heap_bh201_w3_0_d1;
            heap_bh201_w3_0_d3 <=  heap_bh201_w3_0_d2;
            heap_bh201_w3_0_d4 <=  heap_bh201_w3_0_d3;
            heap_bh201_w3_0_d5 <=  heap_bh201_w3_0_d4;
            heap_bh201_w2_0_d1 <=  heap_bh201_w2_0;
            heap_bh201_w2_0_d2 <=  heap_bh201_w2_0_d1;
            heap_bh201_w2_0_d3 <=  heap_bh201_w2_0_d2;
            heap_bh201_w2_0_d4 <=  heap_bh201_w2_0_d3;
            heap_bh201_w2_0_d5 <=  heap_bh201_w2_0_d4;
            heap_bh201_w1_0_d1 <=  heap_bh201_w1_0;
            heap_bh201_w1_0_d2 <=  heap_bh201_w1_0_d1;
            heap_bh201_w1_0_d3 <=  heap_bh201_w1_0_d2;
            heap_bh201_w1_0_d4 <=  heap_bh201_w1_0_d3;
            heap_bh201_w1_0_d5 <=  heap_bh201_w1_0_d4;
            heap_bh201_w24_1_d1 <=  heap_bh201_w24_1;
            heap_bh201_w24_1_d2 <=  heap_bh201_w24_1_d1;
            heap_bh201_w24_1_d3 <=  heap_bh201_w24_1_d2;
            heap_bh201_w24_1_d4 <=  heap_bh201_w24_1_d3;
            heap_bh201_w23_1_d1 <=  heap_bh201_w23_1;
            heap_bh201_w23_1_d2 <=  heap_bh201_w23_1_d1;
            heap_bh201_w23_1_d3 <=  heap_bh201_w23_1_d2;
            heap_bh201_w23_1_d4 <=  heap_bh201_w23_1_d3;
            heap_bh201_w22_1_d1 <=  heap_bh201_w22_1;
            heap_bh201_w22_1_d2 <=  heap_bh201_w22_1_d1;
            heap_bh201_w22_1_d3 <=  heap_bh201_w22_1_d2;
            heap_bh201_w22_1_d4 <=  heap_bh201_w22_1_d3;
            heap_bh201_w21_1_d1 <=  heap_bh201_w21_1;
            heap_bh201_w21_1_d2 <=  heap_bh201_w21_1_d1;
            heap_bh201_w21_1_d3 <=  heap_bh201_w21_1_d2;
            heap_bh201_w20_1_d1 <=  heap_bh201_w20_1;
            heap_bh201_w20_1_d2 <=  heap_bh201_w20_1_d1;
            heap_bh201_w20_1_d3 <=  heap_bh201_w20_1_d2;
            heap_bh201_w19_1_d1 <=  heap_bh201_w19_1;
            heap_bh201_w19_1_d2 <=  heap_bh201_w19_1_d1;
            heap_bh201_w19_1_d3 <=  heap_bh201_w19_1_d2;
            heap_bh201_w18_1_d1 <=  heap_bh201_w18_1;
            heap_bh201_w18_1_d2 <=  heap_bh201_w18_1_d1;
            heap_bh201_w18_1_d3 <=  heap_bh201_w18_1_d2;
            heap_bh201_w17_1_d1 <=  heap_bh201_w17_1;
            heap_bh201_w17_1_d2 <=  heap_bh201_w17_1_d1;
            heap_bh201_w17_1_d3 <=  heap_bh201_w17_1_d2;
            heap_bh201_w16_1_d1 <=  heap_bh201_w16_1;
            heap_bh201_w16_1_d2 <=  heap_bh201_w16_1_d1;
            heap_bh201_w16_1_d3 <=  heap_bh201_w16_1_d2;
            heap_bh201_w15_1_d1 <=  heap_bh201_w15_1;
            heap_bh201_w15_1_d2 <=  heap_bh201_w15_1_d1;
            heap_bh201_w14_1_d1 <=  heap_bh201_w14_1;
            heap_bh201_w14_1_d2 <=  heap_bh201_w14_1_d1;
            heap_bh201_w13_1_d1 <=  heap_bh201_w13_1;
            heap_bh201_w13_1_d2 <=  heap_bh201_w13_1_d1;
            heap_bh201_w12_1_d1 <=  heap_bh201_w12_1;
            heap_bh201_w12_1_d2 <=  heap_bh201_w12_1_d1;
            heap_bh201_w11_1_d1 <=  heap_bh201_w11_1;
            heap_bh201_w11_1_d2 <=  heap_bh201_w11_1_d1;
            heap_bh201_w10_1_d1 <=  heap_bh201_w10_1;
            heap_bh201_w10_1_d2 <=  heap_bh201_w10_1_d1;
            heap_bh201_w9_1_d1 <=  heap_bh201_w9_1;
            heap_bh201_w8_1_d1 <=  heap_bh201_w8_1;
            heap_bh201_w7_1_d1 <=  heap_bh201_w7_1;
            heap_bh201_w6_1_d1 <=  heap_bh201_w6_1;
            heap_bh201_w5_1_d1 <=  heap_bh201_w5_1;
            heap_bh201_w4_1_d1 <=  heap_bh201_w4_1;
            heap_bh201_w3_1_d1 <=  heap_bh201_w3_1;
            heap_bh201_w3_1_d2 <=  heap_bh201_w3_1_d1;
            heap_bh201_w3_1_d3 <=  heap_bh201_w3_1_d2;
            heap_bh201_w3_1_d4 <=  heap_bh201_w3_1_d3;
            heap_bh201_w3_1_d5 <=  heap_bh201_w3_1_d4;
            heap_bh201_w2_1_d1 <=  heap_bh201_w2_1;
            heap_bh201_w2_1_d2 <=  heap_bh201_w2_1_d1;
            heap_bh201_w2_1_d3 <=  heap_bh201_w2_1_d2;
            heap_bh201_w2_1_d4 <=  heap_bh201_w2_1_d3;
            heap_bh201_w2_1_d5 <=  heap_bh201_w2_1_d4;
            heap_bh201_w1_1_d1 <=  heap_bh201_w1_1;
            heap_bh201_w1_1_d2 <=  heap_bh201_w1_1_d1;
            heap_bh201_w1_1_d3 <=  heap_bh201_w1_1_d2;
            heap_bh201_w1_1_d4 <=  heap_bh201_w1_1_d3;
            heap_bh201_w1_1_d5 <=  heap_bh201_w1_1_d4;
            heap_bh201_w4_2_d1 <=  heap_bh201_w4_2;
            tempR_bh201_0_d1 <=  tempR_bh201_0;
            tempR_bh201_0_d2 <=  tempR_bh201_0_d1;
            tempR_bh201_0_d3 <=  tempR_bh201_0_d2;
            tempR_bh201_0_d4 <=  tempR_bh201_0_d3;
            tempR_bh201_0_d5 <=  tempR_bh201_0_d4;
            heap_bh201_w4_3_d1 <=  heap_bh201_w4_3;
            heap_bh201_w4_3_d2 <=  heap_bh201_w4_3_d1;
            heap_bh201_w4_3_d3 <=  heap_bh201_w4_3_d2;
            heap_bh201_w4_3_d4 <=  heap_bh201_w4_3_d3;
            heap_bh201_w5_2_d1 <=  heap_bh201_w5_2;
            heap_bh201_w5_2_d2 <=  heap_bh201_w5_2_d1;
            heap_bh201_w5_2_d3 <=  heap_bh201_w5_2_d2;
            heap_bh201_w5_2_d4 <=  heap_bh201_w5_2_d3;
            heap_bh201_w6_3_d1 <=  heap_bh201_w6_3;
            heap_bh201_w6_3_d2 <=  heap_bh201_w6_3_d1;
            heap_bh201_w6_3_d3 <=  heap_bh201_w6_3_d2;
            heap_bh201_w6_3_d4 <=  heap_bh201_w6_3_d3;
            heap_bh201_w7_2_d1 <=  heap_bh201_w7_2;
            heap_bh201_w7_2_d2 <=  heap_bh201_w7_2_d1;
            heap_bh201_w7_2_d3 <=  heap_bh201_w7_2_d2;
            heap_bh201_w7_2_d4 <=  heap_bh201_w7_2_d3;
            heap_bh201_w8_3_d1 <=  heap_bh201_w8_3;
            heap_bh201_w8_3_d2 <=  heap_bh201_w8_3_d1;
            heap_bh201_w8_3_d3 <=  heap_bh201_w8_3_d2;
            heap_bh201_w8_3_d4 <=  heap_bh201_w8_3_d3;
            heap_bh201_w9_2_d1 <=  heap_bh201_w9_2;
            heap_bh201_w9_2_d2 <=  heap_bh201_w9_2_d1;
            heap_bh201_w9_2_d3 <=  heap_bh201_w9_2_d2;
            heap_bh201_w9_2_d4 <=  heap_bh201_w9_2_d3;
            heap_bh201_w10_2_d1 <=  heap_bh201_w10_2;
            heap_bh201_w10_3_d1 <=  heap_bh201_w10_3;
            heap_bh201_w10_3_d2 <=  heap_bh201_w10_3_d1;
            heap_bh201_w10_3_d3 <=  heap_bh201_w10_3_d2;
            heap_bh201_w11_2_d1 <=  heap_bh201_w11_2;
            heap_bh201_w11_2_d2 <=  heap_bh201_w11_2_d1;
            heap_bh201_w11_2_d3 <=  heap_bh201_w11_2_d2;
            heap_bh201_w12_3_d1 <=  heap_bh201_w12_3;
            heap_bh201_w12_3_d2 <=  heap_bh201_w12_3_d1;
            heap_bh201_w12_3_d3 <=  heap_bh201_w12_3_d2;
            heap_bh201_w13_2_d1 <=  heap_bh201_w13_2;
            heap_bh201_w13_2_d2 <=  heap_bh201_w13_2_d1;
            heap_bh201_w13_2_d3 <=  heap_bh201_w13_2_d2;
            heap_bh201_w14_3_d1 <=  heap_bh201_w14_3;
            heap_bh201_w14_3_d2 <=  heap_bh201_w14_3_d1;
            heap_bh201_w14_3_d3 <=  heap_bh201_w14_3_d2;
            heap_bh201_w15_2_d1 <=  heap_bh201_w15_2;
            heap_bh201_w15_2_d2 <=  heap_bh201_w15_2_d1;
            heap_bh201_w15_2_d3 <=  heap_bh201_w15_2_d2;
            heap_bh201_w16_2_d1 <=  heap_bh201_w16_2;
            heap_bh201_w16_3_d1 <=  heap_bh201_w16_3;
            heap_bh201_w16_3_d2 <=  heap_bh201_w16_3_d1;
            heap_bh201_w17_2_d1 <=  heap_bh201_w17_2;
            heap_bh201_w17_2_d2 <=  heap_bh201_w17_2_d1;
            heap_bh201_w18_3_d1 <=  heap_bh201_w18_3;
            heap_bh201_w18_3_d2 <=  heap_bh201_w18_3_d1;
            heap_bh201_w19_2_d1 <=  heap_bh201_w19_2;
            heap_bh201_w19_2_d2 <=  heap_bh201_w19_2_d1;
            heap_bh201_w20_3_d1 <=  heap_bh201_w20_3;
            heap_bh201_w20_3_d2 <=  heap_bh201_w20_3_d1;
            heap_bh201_w21_2_d1 <=  heap_bh201_w21_2;
            heap_bh201_w21_2_d2 <=  heap_bh201_w21_2_d1;
            heap_bh201_w22_2_d1 <=  heap_bh201_w22_2;
            heap_bh201_w22_3_d1 <=  heap_bh201_w22_3;
            heap_bh201_w23_2_d1 <=  heap_bh201_w23_2;
            heap_bh201_w24_3_d1 <=  heap_bh201_w24_3;
            heap_bh201_w25_1_d1 <=  heap_bh201_w25_1;
            heap_bh201_w26_1_d1 <=  heap_bh201_w26_1;
         end if;
      end process;
   XX_m200 <= X ;
   YY_m200 <= Y ;
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh201_ch0_0 <= ("" & XX_m200(33 downto 17) & "") * ("" & YY_m200(23 downto 0) & "");
   heap_bh201_w41_0 <= DSP_bh201_ch0_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w40_0 <= DSP_bh201_ch0_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w39_0 <= DSP_bh201_ch0_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w38_0 <= DSP_bh201_ch0_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w37_0 <= DSP_bh201_ch0_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w36_0 <= DSP_bh201_ch0_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w35_0 <= DSP_bh201_ch0_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w34_0 <= DSP_bh201_ch0_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w33_0 <= DSP_bh201_ch0_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w32_0 <= DSP_bh201_ch0_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w31_0 <= DSP_bh201_ch0_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w30_0 <= DSP_bh201_ch0_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w29_0 <= DSP_bh201_ch0_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w28_0 <= DSP_bh201_ch0_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w27_0 <= DSP_bh201_ch0_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w26_0 <= DSP_bh201_ch0_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w25_0 <= DSP_bh201_ch0_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w24_0 <= DSP_bh201_ch0_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w23_0 <= DSP_bh201_ch0_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w22_0 <= DSP_bh201_ch0_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w21_0 <= DSP_bh201_ch0_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w20_0 <= DSP_bh201_ch0_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w19_0 <= DSP_bh201_ch0_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w18_0 <= DSP_bh201_ch0_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w17_0 <= DSP_bh201_ch0_0(16); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w16_0 <= DSP_bh201_ch0_0(15); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w15_0 <= DSP_bh201_ch0_0(14); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w14_0 <= DSP_bh201_ch0_0(13); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w13_0 <= DSP_bh201_ch0_0(12); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w12_0 <= DSP_bh201_ch0_0(11); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w11_0 <= DSP_bh201_ch0_0(10); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w10_0 <= DSP_bh201_ch0_0(9); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w9_0 <= DSP_bh201_ch0_0(8); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w8_0 <= DSP_bh201_ch0_0(7); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w7_0 <= DSP_bh201_ch0_0(6); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w6_0 <= DSP_bh201_ch0_0(5); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w5_0 <= DSP_bh201_ch0_0(4); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w4_0 <= DSP_bh201_ch0_0(3); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w3_0 <= DSP_bh201_ch0_0(2); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w2_0 <= DSP_bh201_ch0_0(1); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w1_0 <= DSP_bh201_ch0_0(0); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------
   DSP_bh201_ch1_0 <= ("" & XX_m200(16 downto 0) & "") * ("" & YY_m200(23 downto 0) & "");
   heap_bh201_w24_1 <= DSP_bh201_ch1_0(40); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w23_1 <= DSP_bh201_ch1_0(39); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w22_1 <= DSP_bh201_ch1_0(38); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w21_1 <= DSP_bh201_ch1_0(37); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w20_1 <= DSP_bh201_ch1_0(36); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w19_1 <= DSP_bh201_ch1_0(35); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w18_1 <= DSP_bh201_ch1_0(34); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w17_1 <= DSP_bh201_ch1_0(33); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w16_1 <= DSP_bh201_ch1_0(32); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w15_1 <= DSP_bh201_ch1_0(31); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w14_1 <= DSP_bh201_ch1_0(30); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w13_1 <= DSP_bh201_ch1_0(29); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w12_1 <= DSP_bh201_ch1_0(28); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w11_1 <= DSP_bh201_ch1_0(27); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w10_1 <= DSP_bh201_ch1_0(26); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w9_1 <= DSP_bh201_ch1_0(25); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w8_1 <= DSP_bh201_ch1_0(24); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w7_1 <= DSP_bh201_ch1_0(23); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w6_1 <= DSP_bh201_ch1_0(22); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w5_1 <= DSP_bh201_ch1_0(21); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w4_1 <= DSP_bh201_ch1_0(20); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w3_1 <= DSP_bh201_ch1_0(19); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w2_1 <= DSP_bh201_ch1_0(18); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w1_1 <= DSP_bh201_ch1_0(17); -- cycle= 0 cp= 2.387e-09
   heap_bh201_w0_0 <= DSP_bh201_ch1_0(16); -- cycle= 0 cp= 2.387e-09
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh201_w4_2 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   tempR_bh201_0 <= heap_bh201_w0_0; -- already compressed

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh201_0_0 <= heap_bh201_w4_2_d1 & heap_bh201_w4_1_d1 & heap_bh201_w4_0_d1;
   CompressorIn_bh201_0_1 <= heap_bh201_w5_1_d1 & heap_bh201_w5_0_d1;
   Compressor_bh201_0: Compressor_23_3
      port map ( R => CompressorOut_bh201_0_0   ,
                 X0 => CompressorIn_bh201_0_0,
                 X1 => CompressorIn_bh201_0_1);
   heap_bh201_w4_3 <= CompressorOut_bh201_0_0(0); -- cycle= 1 cp= 5.3072e-10
   heap_bh201_w5_2 <= CompressorOut_bh201_0_0(1); -- cycle= 1 cp= 5.3072e-10
   heap_bh201_w6_2 <= CompressorOut_bh201_0_0(2); -- cycle= 1 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh201_1_2 <= heap_bh201_w6_1_d1 & heap_bh201_w6_0_d1 & heap_bh201_w6_2;
   CompressorIn_bh201_1_3 <= heap_bh201_w7_1_d1 & heap_bh201_w7_0_d1;
   Compressor_bh201_1: Compressor_23_3
      port map ( R => CompressorOut_bh201_1_1   ,
                 X0 => CompressorIn_bh201_1_2,
                 X1 => CompressorIn_bh201_1_3);
   heap_bh201_w6_3 <= CompressorOut_bh201_1_1(0); -- cycle= 1 cp= 1.06144e-09
   heap_bh201_w7_2 <= CompressorOut_bh201_1_1(1); -- cycle= 1 cp= 1.06144e-09
   heap_bh201_w8_2 <= CompressorOut_bh201_1_1(2); -- cycle= 1 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 1----------------
   CompressorIn_bh201_2_4 <= heap_bh201_w8_1_d1 & heap_bh201_w8_0_d1 & heap_bh201_w8_2;
   CompressorIn_bh201_2_5 <= heap_bh201_w9_1_d1 & heap_bh201_w9_0_d1;
   Compressor_bh201_2: Compressor_23_3
      port map ( R => CompressorOut_bh201_2_2   ,
                 X0 => CompressorIn_bh201_2_4,
                 X1 => CompressorIn_bh201_2_5);
   heap_bh201_w8_3 <= CompressorOut_bh201_2_2(0); -- cycle= 1 cp= 1.59216e-09
   heap_bh201_w9_2 <= CompressorOut_bh201_2_2(1); -- cycle= 1 cp= 1.59216e-09
   heap_bh201_w10_2 <= CompressorOut_bh201_2_2(2); -- cycle= 1 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 1----------------
   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh201_3_6 <= heap_bh201_w10_1_d2 & heap_bh201_w10_0_d2 & heap_bh201_w10_2_d1;
   CompressorIn_bh201_3_7 <= heap_bh201_w11_1_d2 & heap_bh201_w11_0_d2;
   Compressor_bh201_3: Compressor_23_3
      port map ( R => CompressorOut_bh201_3_3   ,
                 X0 => CompressorIn_bh201_3_6,
                 X1 => CompressorIn_bh201_3_7);
   heap_bh201_w10_3 <= CompressorOut_bh201_3_3(0); -- cycle= 2 cp= 5.3072e-10
   heap_bh201_w11_2 <= CompressorOut_bh201_3_3(1); -- cycle= 2 cp= 5.3072e-10
   heap_bh201_w12_2 <= CompressorOut_bh201_3_3(2); -- cycle= 2 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh201_4_8 <= heap_bh201_w12_1_d2 & heap_bh201_w12_0_d2 & heap_bh201_w12_2;
   CompressorIn_bh201_4_9 <= heap_bh201_w13_1_d2 & heap_bh201_w13_0_d2;
   Compressor_bh201_4: Compressor_23_3
      port map ( R => CompressorOut_bh201_4_4   ,
                 X0 => CompressorIn_bh201_4_8,
                 X1 => CompressorIn_bh201_4_9);
   heap_bh201_w12_3 <= CompressorOut_bh201_4_4(0); -- cycle= 2 cp= 1.06144e-09
   heap_bh201_w13_2 <= CompressorOut_bh201_4_4(1); -- cycle= 2 cp= 1.06144e-09
   heap_bh201_w14_2 <= CompressorOut_bh201_4_4(2); -- cycle= 2 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 2----------------
   CompressorIn_bh201_5_10 <= heap_bh201_w14_1_d2 & heap_bh201_w14_0_d2 & heap_bh201_w14_2;
   CompressorIn_bh201_5_11 <= heap_bh201_w15_1_d2 & heap_bh201_w15_0_d2;
   Compressor_bh201_5: Compressor_23_3
      port map ( R => CompressorOut_bh201_5_5   ,
                 X0 => CompressorIn_bh201_5_10,
                 X1 => CompressorIn_bh201_5_11);
   heap_bh201_w14_3 <= CompressorOut_bh201_5_5(0); -- cycle= 2 cp= 1.59216e-09
   heap_bh201_w15_2 <= CompressorOut_bh201_5_5(1); -- cycle= 2 cp= 1.59216e-09
   heap_bh201_w16_2 <= CompressorOut_bh201_5_5(2); -- cycle= 2 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh201_6_12 <= heap_bh201_w16_1_d3 & heap_bh201_w16_0_d3 & heap_bh201_w16_2_d1;
   CompressorIn_bh201_6_13 <= heap_bh201_w17_1_d3 & heap_bh201_w17_0_d3;
   Compressor_bh201_6: Compressor_23_3
      port map ( R => CompressorOut_bh201_6_6   ,
                 X0 => CompressorIn_bh201_6_12,
                 X1 => CompressorIn_bh201_6_13);
   heap_bh201_w16_3 <= CompressorOut_bh201_6_6(0); -- cycle= 3 cp= 5.3072e-10
   heap_bh201_w17_2 <= CompressorOut_bh201_6_6(1); -- cycle= 3 cp= 5.3072e-10
   heap_bh201_w18_2 <= CompressorOut_bh201_6_6(2); -- cycle= 3 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh201_7_14 <= heap_bh201_w18_1_d3 & heap_bh201_w18_0_d3 & heap_bh201_w18_2;
   CompressorIn_bh201_7_15 <= heap_bh201_w19_1_d3 & heap_bh201_w19_0_d3;
   Compressor_bh201_7: Compressor_23_3
      port map ( R => CompressorOut_bh201_7_7   ,
                 X0 => CompressorIn_bh201_7_14,
                 X1 => CompressorIn_bh201_7_15);
   heap_bh201_w18_3 <= CompressorOut_bh201_7_7(0); -- cycle= 3 cp= 1.06144e-09
   heap_bh201_w19_2 <= CompressorOut_bh201_7_7(1); -- cycle= 3 cp= 1.06144e-09
   heap_bh201_w20_2 <= CompressorOut_bh201_7_7(2); -- cycle= 3 cp= 1.06144e-09

   ----------------Synchro barrier, entering cycle 3----------------
   CompressorIn_bh201_8_16 <= heap_bh201_w20_1_d3 & heap_bh201_w20_0_d3 & heap_bh201_w20_2;
   CompressorIn_bh201_8_17 <= heap_bh201_w21_1_d3 & heap_bh201_w21_0_d3;
   Compressor_bh201_8: Compressor_23_3
      port map ( R => CompressorOut_bh201_8_8   ,
                 X0 => CompressorIn_bh201_8_16,
                 X1 => CompressorIn_bh201_8_17);
   heap_bh201_w20_3 <= CompressorOut_bh201_8_8(0); -- cycle= 3 cp= 1.59216e-09
   heap_bh201_w21_2 <= CompressorOut_bh201_8_8(1); -- cycle= 3 cp= 1.59216e-09
   heap_bh201_w22_2 <= CompressorOut_bh201_8_8(2); -- cycle= 3 cp= 1.59216e-09

   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh201_9_18 <= heap_bh201_w22_1_d4 & heap_bh201_w22_0_d4 & heap_bh201_w22_2_d1;
   CompressorIn_bh201_9_19 <= heap_bh201_w23_1_d4 & heap_bh201_w23_0_d4;
   Compressor_bh201_9: Compressor_23_3
      port map ( R => CompressorOut_bh201_9_9   ,
                 X0 => CompressorIn_bh201_9_18,
                 X1 => CompressorIn_bh201_9_19);
   heap_bh201_w22_3 <= CompressorOut_bh201_9_9(0); -- cycle= 4 cp= 5.3072e-10
   heap_bh201_w23_2 <= CompressorOut_bh201_9_9(1); -- cycle= 4 cp= 5.3072e-10
   heap_bh201_w24_2 <= CompressorOut_bh201_9_9(2); -- cycle= 4 cp= 5.3072e-10

   ----------------Synchro barrier, entering cycle 4----------------
   CompressorIn_bh201_10_20 <= heap_bh201_w24_1_d4 & heap_bh201_w24_0_d4 & heap_bh201_w24_2;
   CompressorIn_bh201_10_21(0) <= heap_bh201_w25_0_d4;
   Compressor_bh201_10: Compressor_13_3
      port map ( R => CompressorOut_bh201_10_10   ,
                 X0 => CompressorIn_bh201_10_20,
                 X1 => CompressorIn_bh201_10_21);
   heap_bh201_w24_3 <= CompressorOut_bh201_10_10(0); -- cycle= 4 cp= 1.06144e-09
   heap_bh201_w25_1 <= CompressorOut_bh201_10_10(1); -- cycle= 4 cp= 1.06144e-09
   heap_bh201_w26_1 <= CompressorOut_bh201_10_10(2); -- cycle= 4 cp= 1.06144e-09
   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   finalAdderIn0_bh201 <= "0" & heap_bh201_w41_0_d5 & heap_bh201_w40_0_d5 & heap_bh201_w39_0_d5 & heap_bh201_w38_0_d5 & heap_bh201_w37_0_d5 & heap_bh201_w36_0_d5 & heap_bh201_w35_0_d5 & heap_bh201_w34_0_d5 & heap_bh201_w33_0_d5 & heap_bh201_w32_0_d5 & heap_bh201_w31_0_d5 & heap_bh201_w30_0_d5 & heap_bh201_w29_0_d5 & heap_bh201_w28_0_d5 & heap_bh201_w27_0_d5 & heap_bh201_w26_0_d5 & heap_bh201_w25_1_d1 & heap_bh201_w24_3_d1 & heap_bh201_w23_2_d1 & heap_bh201_w22_3_d1 & heap_bh201_w21_2_d2 & heap_bh201_w20_3_d2 & heap_bh201_w19_2_d2 & heap_bh201_w18_3_d2 & heap_bh201_w17_2_d2 & heap_bh201_w16_3_d2 & heap_bh201_w15_2_d3 & heap_bh201_w14_3_d3 & heap_bh201_w13_2_d3 & heap_bh201_w12_3_d3 & heap_bh201_w11_2_d3 & heap_bh201_w10_3_d3 & heap_bh201_w9_2_d4 & heap_bh201_w8_3_d4 & heap_bh201_w7_2_d4 & heap_bh201_w6_3_d4 & heap_bh201_w5_2_d4 & heap_bh201_w4_3_d4 & heap_bh201_w3_1_d5 & heap_bh201_w2_1_d5 & heap_bh201_w1_1_d5;
   finalAdderIn1_bh201 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh201_w26_1_d1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh201_w3_0_d5 & heap_bh201_w2_0_d5 & heap_bh201_w1_0_d5;
   finalAdderCin_bh201 <= '0';
   Adder_final201_0: IntAdder_42_f400_uid221  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh201,
                 R => finalAdderOut_bh201   ,
                 X => finalAdderIn0_bh201,
                 Y => finalAdderIn1_bh201);
   -- concatenate all the compressed chunks
   CompressionResult201 <= finalAdderOut_bh201 & tempR_bh201_0_d5;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult201(41 downto 5);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_44_f400_uid229
--                     (IntAdderClassical_44_f400_uid231)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_44_f400_uid229 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(43 downto 0);
          Y : in  std_logic_vector(43 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(43 downto 0)   );
end entity;

architecture arch of IntAdder_44_f400_uid229 is
signal x0 :  std_logic_vector(20 downto 0);
signal y0 :  std_logic_vector(20 downto 0);
signal x1, x1_d1 :  std_logic_vector(22 downto 0);
signal y1, y1_d1 :  std_logic_vector(22 downto 0);
signal sum0, sum0_d1 :  std_logic_vector(21 downto 0);
signal sum1 :  std_logic_vector(23 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            x1_d1 <=  x1;
            y1_d1 <=  y1;
            sum0_d1 <=  sum0;
         end if;
      end process;
   --Classical
   x0 <= X(20 downto 0);
   y0 <= Y(20 downto 0);
   x1 <= X(43 downto 21);
   y1 <= Y(43 downto 21);
   sum0 <= ( "0" & x0) + ( "0" & y0)  + Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   sum1 <= ( "0" & x1_d1) + ( "0" & y1_d1)  + sum0_d1(21);
   R <= sum1(22 downto 0) & sum0_d1(20 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                     FPMultiplier_8_33_8_23_8_34_uid197
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin 2008-2011
--------------------------------------------------------------------------------
-- Pipeline depth: 6 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPMultiplier_8_33_8_23_8_34_uid197 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8+33+2 downto 0);
          Y : in  std_logic_vector(8+23+2 downto 0);
          R : out  std_logic_vector(8+34+2 downto 0)   );
end entity;

architecture arch of FPMultiplier_8_33_8_23_8_34_uid197 is
   component IntAdder_44_f400_uid229 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(43 downto 0);
             Y : in  std_logic_vector(43 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(43 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_34_24_37_unsigned_uid199 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(33 downto 0);
             Y : in  std_logic_vector(23 downto 0);
             R : out  std_logic_vector(36 downto 0)   );
   end component;

signal sign, sign_d1, sign_d2, sign_d3, sign_d4, sign_d5, sign_d6 : std_logic;
signal expX :  std_logic_vector(7 downto 0);
signal expY :  std_logic_vector(7 downto 0);
signal expSumPreSub, expSumPreSub_d1 :  std_logic_vector(9 downto 0);
signal bias, bias_d1 :  std_logic_vector(9 downto 0);
signal expSum, expSum_d1, expSum_d2, expSum_d3, expSum_d4 :  std_logic_vector(9 downto 0);
signal sigX :  std_logic_vector(33 downto 0);
signal sigY :  std_logic_vector(23 downto 0);
signal sigProd :  std_logic_vector(36 downto 0);
signal excSel :  std_logic_vector(3 downto 0);
signal exc, exc_d1, exc_d2, exc_d3, exc_d4, exc_d5, exc_d6 :  std_logic_vector(1 downto 0);
signal norm : std_logic;
signal expPostNorm :  std_logic_vector(9 downto 0);
signal sigProdExt :  std_logic_vector(36 downto 0);
signal expSig :  std_logic_vector(43 downto 0);
signal round : std_logic;
signal expSigPostRound :  std_logic_vector(43 downto 0);
signal excPostNorm :  std_logic_vector(1 downto 0);
signal finalExc :  std_logic_vector(1 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            sign_d1 <=  sign;
            sign_d2 <=  sign_d1;
            sign_d3 <=  sign_d2;
            sign_d4 <=  sign_d3;
            sign_d5 <=  sign_d4;
            sign_d6 <=  sign_d5;
            expSumPreSub_d1 <=  expSumPreSub;
            bias_d1 <=  bias;
            expSum_d1 <=  expSum;
            expSum_d2 <=  expSum_d1;
            expSum_d3 <=  expSum_d2;
            expSum_d4 <=  expSum_d3;
            exc_d1 <=  exc;
            exc_d2 <=  exc_d1;
            exc_d3 <=  exc_d2;
            exc_d4 <=  exc_d3;
            exc_d5 <=  exc_d4;
            exc_d6 <=  exc_d5;
         end if;
      end process;
   sign <= X(41) xor Y(31);
   expX <= X(40 downto 33);
   expY <= Y(30 downto 23);
   expSumPreSub <= ("00" & expX) + ("00" & expY);
   bias <= CONV_STD_LOGIC_VECTOR(127,10);
   ----------------Synchro barrier, entering cycle 1----------------
   expSum <= expSumPreSub_d1 - bias_d1;
   ----------------Synchro barrier, entering cycle 0----------------
   sigX <= "1" & X(32 downto 0);
   sigY <= "1" & Y(22 downto 0);
   SignificandMultiplication: IntMultiplier_UsingDSP_34_24_37_unsigned_uid199  -- pipelineDepth=5 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => sigProd,
                 X => sigX,
                 Y => sigY);
   ----------------Synchro barrier, entering cycle 5----------------
   ----------------Synchro barrier, entering cycle 0----------------
   excSel <= X(43 downto 42) & Y(33 downto 32);
   with excSel select 
   exc <= "00" when  "0000" | "0001" | "0100", 
          "01" when "0101",
          "10" when "0110" | "1001" | "1010" ,
          "11" when others;
   ----------------Synchro barrier, entering cycle 5----------------
   norm <= sigProd(36);
   -- exponent update
   expPostNorm <= expSum_d4 + ("000000000" & norm);
   ----------------Synchro barrier, entering cycle 5----------------
   -- significand normalization shift
   sigProdExt <= sigProd(35 downto 0) & "0" when norm='1' else
                         sigProd(34 downto 0) & "00";
   expSig <= expPostNorm & sigProdExt(36 downto 3);
   round <= '1' ;
   RoundingAdder: IntAdder_44_f400_uid229  -- pipelineDepth=1 maxInDelay=1.34272e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => round,
                 R => expSigPostRound   ,
                 X => expSig,
                 Y => "00000000000000000000000000000000000000000000");
   ----------------Synchro barrier, entering cycle 6----------------
   with expSigPostRound(43 downto 42) select
   excPostNorm <=  "01"  when  "00",
                               "10"             when "01", 
                               "00"             when "11"|"10",
                               "11"             when others;
   with exc_d6 select 
   finalExc <= exc_d6 when  "11"|"10"|"00",
                       excPostNorm when others; 
   R <= finalExc & sign_d6 & expSigPostRound(41 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                      LeftShifter_35_by_max_33_uid238
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_35_by_max_33_uid238 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(34 downto 0);
          S : in  std_logic_vector(5 downto 0);
          R : out  std_logic_vector(67 downto 0)   );
end entity;

architecture arch of LeftShifter_35_by_max_33_uid238 is
signal level0, level0_d1 :  std_logic_vector(34 downto 0);
signal ps, ps_d1, ps_d2 :  std_logic_vector(5 downto 0);
signal level1 :  std_logic_vector(35 downto 0);
signal level2 :  std_logic_vector(37 downto 0);
signal level3 :  std_logic_vector(41 downto 0);
signal level4 :  std_logic_vector(49 downto 0);
signal level5, level5_d1 :  std_logic_vector(65 downto 0);
signal level6 :  std_logic_vector(97 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            level0_d1 <=  level0;
            ps_d1 <=  ps;
            ps_d2 <=  ps_d1;
            level5_d1 <=  level5;
         end if;
      end process;
   level0<= X;
   ps<= S;
   ----------------Synchro barrier, entering cycle 1----------------
   level1<= level0_d1 & (0 downto 0 => '0') when ps_d1(0)= '1' else     (0 downto 0 => '0') & level0_d1;
   level2<= level1 & (1 downto 0 => '0') when ps_d1(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps_d1(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps_d1(3)= '1' else     (7 downto 0 => '0') & level3;
   level5<= level4 & (15 downto 0 => '0') when ps_d1(4)= '1' else     (15 downto 0 => '0') & level4;
   ----------------Synchro barrier, entering cycle 2----------------
   level6<= level5_d1 & (31 downto 0 => '0') when ps_d2(5)= '1' else     (31 downto 0 => '0') & level5_d1;
   R <= level6(67 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(3 downto 0);
          Y : out  std_logic_vector(11 downto 0)   );
end entity;

architecture arch of FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1 is
begin
  with X select  Y <= 
   "000000001000" when "0000",
   "000011000001" when "0001",
   "000101111001" when "0010",
   "001000110010" when "0011",
   "001011101011" when "0100",
   "001110100011" when "0101",
   "010001011100" when "0110",
   "010100010101" when "0111",
   "010111001101" when "1000",
   "011010000110" when "1001",
   "011100111111" when "1010",
   "011111110111" when "1011",
   "100010110000" when "1100",
   "100101101001" when "1101",
   "101000100001" when "1110",
   "101011011010" when "1111",
   "------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(7 downto 0)   );
end entity;

architecture arch of FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0 is
begin
  with X select  Y <= 
   "00000000" when "000000",
   "00000011" when "000001",
   "00000110" when "000010",
   "00001001" when "000011",
   "00001100" when "000100",
   "00001110" when "000101",
   "00010001" when "000110",
   "00010100" when "000111",
   "00010111" when "001000",
   "00011010" when "001001",
   "00011101" when "001010",
   "00100000" when "001011",
   "00100011" when "001100",
   "00100110" when "001101",
   "00101000" when "001110",
   "00101011" when "001111",
   "00101110" when "010000",
   "00110001" when "010001",
   "00110100" when "010010",
   "00110111" when "010011",
   "00111010" when "010100",
   "00111101" when "010101",
   "00111111" when "010110",
   "01000010" when "010111",
   "01000101" when "011000",
   "01001000" when "011001",
   "01001011" when "011010",
   "01001110" when "011011",
   "01010001" when "011100",
   "01010100" when "011101",
   "01010111" when "011110",
   "01011001" when "011111",
   "01011100" when "100000",
   "01011111" when "100001",
   "01100010" when "100010",
   "01100101" when "100011",
   "01101000" when "100100",
   "01101011" when "100101",
   "01101110" when "100110",
   "01110001" when "100111",
   "01110011" when "101000",
   "01110110" when "101001",
   "01111001" when "101010",
   "01111100" when "101011",
   "01111111" when "101100",
   "10000010" when "101101",
   "10000101" when "101110",
   "10001000" when "101111",
   "10001010" when "110000",
   "10001101" when "110001",
   "10010000" when "110010",
   "10010011" when "110011",
   "10010110" when "110100",
   "10011001" when "110101",
   "10011100" when "110110",
   "10011111" when "110111",
   "10100010" when "111000",
   "10100100" when "111001",
   "10100111" when "111010",
   "10101010" when "111011",
   "10101101" when "111100",
   "10110000" when "111101",
   "10110011" when "111110",
   "10110110" when "111111",
   "--------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_12_f400_uid246
--                     (IntAdderClassical_12_f400_uid248)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_12_f400_uid246 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(11 downto 0);
          Y : in  std_logic_vector(11 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(11 downto 0)   );
end entity;

architecture arch of IntAdder_12_f400_uid246 is
signal X_d1 :  std_logic_vector(11 downto 0);
signal Y_d1 :  std_logic_vector(11 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                     FixRealKCM_M3_6_0_1_log_2_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: 
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_M3_6_0_1_log_2_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(9 downto 0);
          R : out  std_logic_vector(7 downto 0)   );
end entity;

architecture arch of FixRealKCM_M3_6_0_1_log_2_unsigned is
   component FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(7 downto 0)   );
   end component;

   component FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(3 downto 0);
             Y : out  std_logic_vector(11 downto 0)   );
   end component;

   component IntAdder_12_f400_uid246 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(11 downto 0);
             Y : in  std_logic_vector(11 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(11 downto 0)   );
   end component;

signal d1 :  std_logic_vector(3 downto 0);
signal d0 :  std_logic_vector(5 downto 0);
signal pp0, pp0_d1 :  std_logic_vector(7 downto 0);
signal pp1, pp1_d1 :  std_logic_vector(11 downto 0);
signal addOp0 :  std_logic_vector(11 downto 0);
signal OutRes :  std_logic_vector(11 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0: component is "yes";
attribute rom_extract of FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1: component is "yes";
attribute rom_style of FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0: component is "distributed";
attribute rom_style of FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            pp0_d1 <=  pp0;
            pp1_d1 <=  pp1;
         end if;
      end process;
   d1 <= X(9 downto 6);
   d0 <= X(5 downto 0);
   KCMTable_0: FixRealKCM_M3_6_0_1_log_2_unsigned_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0,
                 Y => pp0);
   KCMTable_1: FixRealKCM_M3_6_0_1_log_2_unsigned_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1,
                 Y => pp1);
   ----------------Synchro barrier, entering cycle 1----------------
   addOp0 <= (11 downto 8 => '0') & pp0_d1;
   Result_Adder: IntAdder_12_f400_uid246  -- pipelineDepth=1 maxInDelay=2.01536e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => OutRes,
                 X => addOp0,
                 Y => pp1_d1);
   ----------------Synchro barrier, entering cycle 2----------------
   R <= OutRes(11 downto 4);
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_0_7_M26_log_2_unsigned_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_0_7_M26_log_2_unsigned_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of FixRealKCM_0_7_M26_log_2_unsigned_Table_1 is
begin
  with X select  Y <= 
   "0000000000000000000000000000000000" when "00",
   "0010110001011100100001011111111000" when "01",
   "0101100010111001000010111111110000" when "10",
   "1000010100010101100100011111100111" when "11",
   "----------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                 FixRealKCM_0_7_M26_log_2_unsigned_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
entity FixRealKCM_0_7_M26_log_2_unsigned_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of FixRealKCM_0_7_M26_log_2_unsigned_Table_0 is
begin
  with X select  Y <= 
   "00000000000000000000000000000000" when "000000",
   "00000010110001011100100001100000" when "000001",
   "00000101100010111001000011000000" when "000010",
   "00001000010100010101100100100000" when "000011",
   "00001011000101110010000101111111" when "000100",
   "00001101110111001110100111011111" when "000101",
   "00010000101000101011001000111111" when "000110",
   "00010011011010000111101010011111" when "000111",
   "00010110001011100100001011111111" when "001000",
   "00011000111101000000101101011111" when "001001",
   "00011011101110011101001110111111" when "001010",
   "00011110011111111001110000011111" when "001011",
   "00100001010001010110010001111110" when "001100",
   "00100100000010110010110011011110" when "001101",
   "00100110110100001111010100111110" when "001110",
   "00101001100101101011110110011110" when "001111",
   "00101100010111001000010111111110" when "010000",
   "00101111001000100100111001011110" when "010001",
   "00110001111010000001011010111110" when "010010",
   "00110100101011011101111100011110" when "010011",
   "00110111011100111010011101111101" when "010100",
   "00111010001110010110111111011101" when "010101",
   "00111100111111110011100000111101" when "010110",
   "00111111110001010000000010011101" when "010111",
   "01000010100010101100100011111101" when "011000",
   "01000101010100001001000101011101" when "011001",
   "01001000000101100101100110111101" when "011010",
   "01001010110111000010001000011101" when "011011",
   "01001101101000011110101001111100" when "011100",
   "01010000011001111011001011011100" when "011101",
   "01010011001011010111101100111100" when "011110",
   "01010101111100110100001110011100" when "011111",
   "01011000101110010000101111111100" when "100000",
   "01011011011111101101010001011100" when "100001",
   "01011110010001001001110010111100" when "100010",
   "01100001000010100110010100011100" when "100011",
   "01100011110100000010110101111011" when "100100",
   "01100110100101011111010111011011" when "100101",
   "01101001010110111011111000111011" when "100110",
   "01101100001000011000011010011011" when "100111",
   "01101110111001110100111011111011" when "101000",
   "01110001101011010001011101011011" when "101001",
   "01110100011100101101111110111011" when "101010",
   "01110111001110001010100000011011" when "101011",
   "01111001111111100111000001111010" when "101100",
   "01111100110001000011100011011010" when "101101",
   "01111111100010100000000100111010" when "101110",
   "10000010010011111100100110011010" when "101111",
   "10000101000101011001000111111010" when "110000",
   "10000111110110110101101001011010" when "110001",
   "10001010101000010010001010111010" when "110010",
   "10001101011001101110101100011001" when "110011",
   "10010000001011001011001101111001" when "110100",
   "10010010111100100111101111011001" when "110101",
   "10010101101110000100010000111001" when "110110",
   "10011000011111100000110010011001" when "110111",
   "10011011010000111101010011111001" when "111000",
   "10011110000010011001110101011001" when "111001",
   "10100000110011110110010110111001" when "111010",
   "10100011100101010010111000011000" when "111011",
   "10100110010110101111011001111000" when "111100",
   "10101001001000001011111011011000" when "111101",
   "10101011111001101000011100111000" when "111110",
   "10101110101011000100111110011000" when "111111",
   "--------------------------------" when others;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_34_f400_uid259
--                     (IntAdderClassical_34_f400_uid261)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_34_f400_uid259 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          Y : in  std_logic_vector(33 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of IntAdder_34_f400_uid259 is
signal X_d1 :  std_logic_vector(33 downto 0);
signal Y_d1 :  std_logic_vector(33 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   ----------------Synchro barrier, entering cycle 1----------------
   --Classical
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                     FixRealKCM_0_7_M26_log_2_unsigned
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: 
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_0_7_M26_log_2_unsigned is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          R : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of FixRealKCM_0_7_M26_log_2_unsigned is
   component FixRealKCM_0_7_M26_log_2_unsigned_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(31 downto 0)   );
   end component;

   component FixRealKCM_0_7_M26_log_2_unsigned_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(33 downto 0)   );
   end component;

   component IntAdder_34_f400_uid259 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(33 downto 0);
             Y : in  std_logic_vector(33 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(33 downto 0)   );
   end component;

signal d1 :  std_logic_vector(1 downto 0);
signal d0 :  std_logic_vector(5 downto 0);
signal pp0, pp0_d1 :  std_logic_vector(31 downto 0);
signal pp1, pp1_d1 :  std_logic_vector(33 downto 0);
signal addOp0 :  std_logic_vector(33 downto 0);
signal OutRes :  std_logic_vector(33 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of FixRealKCM_0_7_M26_log_2_unsigned_Table_0: component is "yes";
attribute rom_extract of FixRealKCM_0_7_M26_log_2_unsigned_Table_1: component is "yes";
attribute rom_style of FixRealKCM_0_7_M26_log_2_unsigned_Table_0: component is "distributed";
attribute rom_style of FixRealKCM_0_7_M26_log_2_unsigned_Table_1: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            pp0_d1 <=  pp0;
            pp1_d1 <=  pp1;
         end if;
      end process;
   d1 <= X(7 downto 6);
   d0 <= X(5 downto 0);
   KCMTable_0: FixRealKCM_0_7_M26_log_2_unsigned_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d0,
                 Y => pp0);
   KCMTable_1: FixRealKCM_0_7_M26_log_2_unsigned_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => d1,
                 Y => pp1);
   ----------------Synchro barrier, entering cycle 1----------------
   addOp0 <= (33 downto 32 => '0') & pp0_d1;
   Result_Adder: IntAdder_34_f400_uid259  -- pipelineDepth=1 maxInDelay=2.7132e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => OutRes,
                 X => addOp0,
                 Y => pp1_d1);
   ----------------Synchro barrier, entering cycle 2----------------
   R <= OutRes(33 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_26_f484_uid267
--                    (IntAdderAlternative_26_f484_uid271)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_26_f484_uid267 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(25 downto 0);
          Y : in  std_logic_vector(25 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(25 downto 0)   );
end entity;

architecture arch of IntAdder_26_f484_uid267 is
signal s_sum_l0_idx0 :  std_logic_vector(18 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(8 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(17 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(7 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(8 downto 0);
signal sum_l1_idx1 :  std_logic_vector(7 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(17 downto 0)) + ( "0" & Y(17 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(25 downto 18)) + ( "0" & Y(25 downto 18));
   sum_l0_idx0 <= s_sum_l0_idx0(17 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(18 downto 18);
   sum_l0_idx1 <= s_sum_l0_idx1(7 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(8 downto 8);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(7 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(8 downto 8);
   R <= sum_l1_idx1(7 downto 0) & sum_l0_idx0_d1(17 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                              MagicSPExpTable
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Radu Tudoran, Florent de Dinechin (2009)
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity MagicSPExpTable is
   port ( X1 : in  std_logic_vector(8 downto 0);
          Y1 : out  std_logic_vector(35 downto 0);
          X2 : in  std_logic_vector(8 downto 0);
          Y2 : out  std_logic_vector(35 downto 0)   );
end entity;

architecture arch of MagicSPExpTable is
type ROMContent is array (0 to 511) of std_logic_vector(35 downto 0);
constant memVar: ROMContent :=    ( 
      "100000000000000000000000000000000000",       "100000000100000000010000000000000000",       "100000001000000001000000001000000000",       "100000001100000010010000010000000000", 
      "100000010000000100000000101000000000",       "100000010100000110010001010000000000",       "100000011000001001000010010000000000",       "100000011100001100010011101000000000", 
      "100000100000010000000101011000000000",       "100000100100010100010111101000000000",       "100000101000011001001010100000000000",       "100000101100011110011110000000000000", 
      "100000110000100100010010001000000000",       "100000110100101010100111000000000000",       "100000111000110001011100110000000000",       "100000111100111000110011011000000000", 
      "100001000001000000101011000000000000",       "100001000101001001000011101000000000",       "100001001001010001111101010000000000",       "100001001101011011011000001000000000", 
      "100001010001100101010100001000000000",       "100001010101101111110001100000000000",       "100001011001111010110000001000000000",       "100001011110000110010000001000000000", 
      "100001100010010010010001110000000000",       "100001100110011110110100110000000000",       "100001101010101011111001011000000000",       "100001101110111001011111110000000000", 
      "100001110011000111100111111000000000",       "100001110111010110010001110000000000",       "100001111011100101011101011000000000",       "100001111111110101001011001000000000", 
      "100010000100000101011010110000000001",       "100010001000010110001100100000000001",       "100010001100100111100000010000000001",       "100010010000111001010110011000000001", 
      "100010010101001011101110101000000001",       "100010011001011110101001010000000001",       "100010011101110010000110011000000001",       "100010100010000110000101111000000001", 
      "100010100110011010100111111000000001",       "100010101010101111101100100000000001",       "100010101111000101010011111000000001",       "100010110011011011011101111000000001", 
      "100010110111110010001010110000000001",       "100010111100001001011010100000000001",       "100011000000100001001101001000000001",       "100011000100111001100010110000000001", 
      "100011001001010010011011100000000001",       "100011001101101011110111011000000001",       "100011010010000101110110100000000001",       "100011010110100000011000111000000001", 
      "100011011010111011011110101000000001",       "100011011111010111000111110000000001",       "100011100011110011010100011000000001",       "100011101000010000000100101000000001", 
      "100011101100101101011000100000000010",       "100011110001001011010000000000000010",       "100011110101101001101011010000000010",       "100011111010001000101010100000000010", 
      "100011111110101000001101100000000010",       "100100000011001000010100100000000010",       "100100000111101000111111101000000010",       "100100001100001010001110110000000010", 
      "100100010000101100000010001000000010",       "100100010101001110011001111000000010",       "100100011001110001010101110000000010",       "100100011110010100110110001000000010", 
      "100100100010111000111011000000000010",       "100100100111011101100100100000000010",       "100100101100000010110010100000000010",       "100100110000101000100101001000000010", 
      "100100110101001110111100101000000011",       "100100111001110101111000111000000011",       "100100111110011101011010001000000011",       "100101000011000101100000010000000011", 
      "100101000111101110001011100000000011",       "100101001100010111011011111000000011",       "100101010001000001010001011000000011",       "100101010101101011101100010000000011", 
      "100101011010010110101100011000000011",       "100101011111000010010001111000000011",       "100101100011101110011101000000000011",       "100101101000011011001101100000000011", 
      "100101101101001000100011110000000011",       "100101110001110110011111110000000100",       "100101110110100101000001011000000100",       "100101111011010100001001000000000100", 
      "100110000000000011110110100000000100",       "100110000100110100001010000000000100",       "100110001001100101000011100000000100",       "100110001110010110100011010000000100", 
      "100110010011001000101001010000000100",       "100110010111111011010101100000000100",       "100110011100101110101000001000000100",       "100110100001100010100001001000000100", 
      "100110100110010111000000110000000101",       "100110101011001100000110111000000101",       "100110110000000001110011110000000101",       "100110110100111000000111011000000101", 
      "100110111001101111000001111000000101",       "100110111110100110100011001000000101",       "100111000011011110101011100000000101",       "100111001000010111011010111000000101", 
      "100111001101010000110001011000000101",       "100111010010001010101111001000000101",       "100111010111000101010100001000000101",       "100111011100000000100000011000000110", 
      "100111100000111100010100010000000110",       "100111100101111000101111100000000110",       "100111101010110101110010100000000110",       "100111101111110011011101000000000110", 
      "100111110100110001101111011000000110",       "100111111001110000101001100000000110",       "100111111110110000001011100000000110",       "101000000011110000010101100000000110", 
      "101000001000110001000111101000000111",       "101000001101110010100001111000000111",       "101000010010110100100100010000000111",       "101000010111110111001111000000000111", 
      "101000011100111010100010001000000111",       "101000100001111110011101101000000111",       "101000100111000011000001110000000111",       "101000101100001000001110100000000111", 
      "101000110001001110000011111000001000",       "101000110110010100100010000000001000",       "101000111011011011101001000000001000",       "101001000000100011011001000000001000", 
      "101001000101101011110001111000001000",       "101001001010110100110011111000001000",       "101001001111111110011111001000001000",       "101001010101001000110011100000001000", 
      "101001011010010011110001011000001001",       "101001011111011111011000100000001001",       "101001100100101011101001010000001001",       "101001101001111000100011100000001001", 
      "101001101111000110000111011000001001",       "101001110100010100010101000000001001",       "101001111001100011001100100000001001",       "101001111110110010101101111000001001", 
      "101010000100000010111001010000001010",       "101010001001010011101110101000001010",       "101010001110100101001110001000001010",       "101010010011110111010111111000001010", 
      "101010011001001010001100000000001010",       "101010011110011101101010100000001010",       "101010100011110001110011100000001010",       "101010101001000110100111000000001011", 
      "101010101110011100000101001000001011",       "101010110011110010001110000000001011",       "101010111001001001000001110000001011",       "101010111110100000100000011000001011", 
      "101011000011111000101001111000001011",       "101011001001010001011110100000001011",       "101011001110101010111110010000001100",       "101011010100000101001001010000001100", 
      "101011011001011111111111101000001100",       "101011011110111011100001010000001100",       "101011100100010111101110100000001100",       "101011101001110100100111010000001100", 
      "101011101111010010001011110000001101",       "101011110100110000011011111000001101",       "101011111010001111010111111000001101",       "101011111111101110111111110000001101", 
      "101100000101001111010011101000001101",       "101100001010110000010011101000001101",       "101100010000010001111111110000001101",       "101100010101110100011000001000001110", 
      "101100011011010111011100111000001110",       "101100100000111011001110000000001110",       "101100100110011111101011101000001110",       "101100101100000100110110000000001110", 
      "101100110001101010101100111000001110",       "101100110111010001010000101000001111",       "101100111100111000100001010000001111",       "101101000010100000011110110000001111", 
      "101101001000001001001001011000001111",       "101101001101110010100001001000001111",       "101101010011011100100110000000001111",       "101101011001000111011000010000010000", 
      "101101011110110010110111111000010000",       "101101100100011111000101000000010000",       "101101101010001011111111110000010000",       "101101101111111001101000001000010000", 
      "101101110101100111111110001000010001",       "101101111011010111000010001000010001",       "101110000001000110110100000000010001",       "101110000110110111010011111000010001", 
      "101110001100101000100001111000010001",       "101110010010011010011110000000010001",       "101110011000001101001000100000010010",       "101110011110000000100001010000010010", 
      "101110100011110100101000101000010010",       "101110101001101001011110100000010010",       "101110101111011111000011000000010010",       "101110110101010101010110010000010011", 
      "101110111011001100011000011000010011",       "101111000001000100001001011000010011",       "101111000110111100101001100000010011",       "101111001100110101111000101000010011", 
      "101111010010101111110111000000010100",       "101111011000101010100100101000010100",       "101111011110100110000001101000010100",       "101111100100100010001110001000010100", 
      "101111101010011111001010010000010100",       "101111110000011100110110000000010101",       "101111110110011011010001100000010101",       "101111111100011010011100110000010101", 
      "110000000010011010011000001000010101",       "110000001000011011000011011000010101",       "110000001110011100011110111000010110",       "110000010100011110101010101000010110", 
      "110000011010100001100110101000010110",       "110000100000100101010011000000010110",       "110000100110101001110000000000010110",       "110000101100101110111101100000010111", 
      "110000110010110100111011110000010111",       "110000111000111011101010110000010111",       "110000111111000011001010101000010111",       "110001000101001011011011101000010111", 
      "110001001011010100011101100000011000",       "110001010001011110010000110000011000",       "110001010111101000110101001000011000",       "110001011101110100001011000000011000", 
      "110001100100000000010010010000011001",       "110001101010001101001011001000011001",       "110001110000011010110101100000011001",       "110001110110101001010001110000011001", 
      "110001111100111000011111111000011001",       "110010000011001000100000000000011010",       "110010001001011001010010001000011010",       "110010001111101010110110011000011010", 
      "110010010101111101001100111000011010",       "110010011100010000010101101000011011",       "110010100010100100010000111000011011",       "110010101000111000111110110000011011", 
      "110010101111001110011111010000011011",       "110010110101100100110010011000011011",       "110010111011111011111000100000011100",       "110011000010010011110001011000011100", 
      "110011001000101100011101011000011100",       "110011001111000101111100100000011100",       "110011010101100000001110111000011101",       "110011011011111011010100101000011101", 
      "110011100010010111001101110000011101",       "110011101000110011111010100000011101",       "110011101111010001011010110000011110",       "110011110101101111101110111000011110", 
      "110011111100001110110110110000011110",       "110100000010101110110010101000011110",       "110100001001001111100010100000011111",       "110100001111110001000110100000011111", 
      "110100010110010011011110111000011111",       "110100011100110110101011100000011111",       "110100100011011010101100100000100000",       "110100101001111111100010001000100000", 
      "010011011010001011001100000000100000",       "010011011100100110100111000000100000",       "010011011111000010010101101000100001",       "010011100001011110010111101000100001", 
      "010011100011111010101101010000100001",       "010011100110010111010110011000100001",       "010011101000110100010011001000100010",       "010011101011010001100011011000100010", 
      "010011101101101111000111100000100010",       "010011110000001100111111010000100010",       "010011110010101011001010110000100011",       "010011110101001001101010000000100011", 
      "010011110111101000011101001000100011",       "010011111010000111100100001000100011",       "010011111100100110111111000000100100",       "010011111111000110101101111000100100", 
      "010100000001100110110000110000100100",       "010100000100000111000111101000100100",       "010100000110100111110010100000100101",       "010100001001001000110001101000100101", 
      "010100001011101010000100110000100101",       "010100001110001011101100001000100101",       "010100010000101101100111101000100110",       "010100010011001111110111100000100110", 
      "010100010101110010011011101000100110",       "010100011000010101010100001000100111",       "010100011010111000100001000000100111",       "010100011101011100000010010000100111", 
      "010100011111111111111000000000100111",       "010100100010100100000010010000101000",       "010100100101001000100001000000101000",       "010100100111101101010100011000101000", 
      "010100101010010010011100011000101001",       "010100101100110111111001000000101001",       "010100101111011101101010011000101001",       "010100110010000011110000100000101001", 
      "010100110100101010001011011000101010",       "010100110111010000111011000000101010",       "010100111001110111111111101000101010",       "010100111100011111011001000000101011", 
      "010100111111000111000111100000101011",       "010101000001101111001010111000101011",       "010101000100010111100011010000101011",       "010101000111000000010000110000101100", 
      "010101001001101001010011011000101100",       "010101001100010010101011001000101100",       "010101001110111100011000000000101101",       "010101010001100110011010001000101101", 
      "010101010100010000110001101000101101",       "010101010110111011011110011000101101",       "010101011001100110100000100000101110",       "010101011100010001111000000000101110", 
      "010101011110111101100101000000101110",       "010101100001101001100111011000101111",       "010101100100010101111111011000101111",       "010101100111000010101101000000101111", 
      "010101101001101111110000001000110000",       "010101101100011101001000111000110000",       "010101101111001010110111011000110000",       "010101110001111000111011101000110000", 
      "010101110100100111010101101000110001",       "010101110111010110000101100000110001",       "010101111010000101001011001000110001",       "010101111100110100100110110000110010", 
      "010101111111100100011000011000110010",       "010110000010010100011111111000110010",       "010110000101000100111101100000110011",       "010110000111110101110001001000110011", 
      "010110001010100110111011000000110011",       "010110001101011000011010111000110100",       "010110010000001010010001000000110100",       "010110010010111100011101100000110100", 
      "010110010101101111000000001000110101",       "010110011000100001111001010000110101",       "010110011011010101001000101000110101",       "010110011110001000101110100000110110", 
      "010110100000111100101010111000110110",       "010110100011110000111101110000110110",       "010110100110100101100111001000110110",       "010110101001011010100111001000110111", 
      "010110101100001111111101110000110111",       "010110101111000101101011001000110111",       "010110110001111011101111010000111000",       "010110110100110010001010001000111000", 
      "010110110111101000111011110000111000",       "010110111010100000000100011000111001",       "010110111101010111100011111000111001",       "010111000000001111011010010000111001", 
      "010111000011000111100111101000111010",       "010111000110000000001100001000111010",       "010111001000111001000111110000111010",       "010111001011110010011010100000111011", 
      "010111001110101100000100011000111011",       "010111010001100110000101100000111011",       "010111010100100000011101111000111100",       "010111010111011011001101101000111100", 
      "010111011010010110010100110000111101",       "010111011101010001110011010000111101",       "010111100000001101101001001000111101",       "010111100011001001110110101000111110", 
      "010111100110000110011011101000111110",       "010111101001000011011000010000111110",       "010111101100000000101100100000111111",       "010111101110111110011000100000111111", 
      "010111110001111100011100001000111111",       "010111110100111010110111101001000000",       "010111110111111001101010111001000000",       "010111111010111000110110000001000000", 
      "010111111101111000011001001001000001",       "011000000000111000010100001001000001",       "011000000011111000100111001001000001",       "011000000110111001010010010001000010", 
      "011000001001111010010101100001000010",       "011000001100111011110000111001000011",       "011000001111111101100100100001000011",       "011000010010111111110000010001000011", 
      "011000010110000010010100011001000100",       "011000011001000101010000111001000100",       "011000011100001000100101110001000100",       "011000011111001100010011001001000101", 
      "011000100010010000011000111001000101",       "011000100101010100110111001001000101",       "011000101000011001101110001001000110",       "011000101011011110111101101001000110", 
      "011000101110100100100101111001000111",       "011000110001101010100110110001000111",       "011000110100110001000000100001000111",       "011000110111110111110011000001001000", 
      "011000111010111110111110100001001000",       "011000111110000110100010111001001000",       "011001000001001110100000001001001001",       "011001000100010110110110100001001001", 
      "011001000111011111100101111001001010",       "011001001010101000101110011001001010",       "011001001101110010010000000001001010",       "011001010000111100001011000001001011", 
      "011001010100000110011111001001001011",       "011001010111010001001100101001001011",       "011001011010011100010011011001001100",       "011001011101100111110011101001001100", 
      "011001100000110011101101011001001101",       "011001100100000000000000101001001101",       "011001100111001100101101011001001101",       "011001101010011001110011111001001110", 
      "011001101101100111010011111001001110",       "011001110000110101001101101001001111",       "011001110100000011100001010001001111",       "011001110111010010001110101001001111", 
      "011001111010100001010101110001010000",       "011001111101110000110110111001010000",       "011010000001000000110010000001010001",       "011010000100010001000111001001010001", 
      "011010000111100001110110010001010001",       "011010001010110010111111101001010010",       "011010001110000100100011001001010010",       "011010010001010110100000110001010011", 
      "011010010100101000111000110001010011",       "011010010111111011101011000001010011",       "011010011011001110110111101001010100",       "011010011110100010011110110001010100", 
      "011010100001110110100000010001010101",       "011010100101001010111100011001010101",       "011010101000011111110011000001010101",       "011010101011110101000100011001010110", 
      "011010101111001010110000011001010110",       "011010110010100000110111000001010111",       "011010110101110111011000101001010111",       "011010111001001110010100111001010111", 
      "011010111100100101101100001001011000",       "011010111111111101011110011001011000",       "011011000011010101101011100001011001",       "011011000110101110010011110001011001", 
      "011011001010000111010111001001011001",       "011011001101100000110101101001011010",       "011011010000111010101111011001011010",       "011011010100010101000100011001011011", 
      "011011010111101111110100101001011011",       "011011011011001011000000011001011100",       "011011011110100110100111011001011100",       "011011100010000010101010000001011100", 
      "011011100101011111001000001001011101",       "011011101000111100000001110001011101",       "011011101100011001010111001001011110",       "011011101111110111001000001001011110", 
      "011011110011010101010100111001011111",       "011011110110110011111101100001011111",       "011011111010010011000010000001011111",       "011011111101110010100010010001100000", 
      "011100000001010010011110101001100000",       "011100000100110010110110111001100001",       "011100001000010011101011011001100001",       "011100001011110100111011111001100010", 
      "011100001111010110101000101001100010",       "011100010010111000110001100001100010",       "011100010110011011010110110001100011",       "011100011001111110011000011001100011", 
      "011100011101100001110110011001100100",       "011100100001000101110000111001100100",       "011100100100101010000111111001100101",       "011100101000001110111011011001100101", 
      "011100101011110100001011101001100110",       "011100101111011001111000100001100110",       "011100110011000000000010001001100110",       "011100110110100110101000100001100111", 
      "011100111010001101101011110001100111",       "011100111101110101001100000001101000",       "011101000001011101001001001001101000",       "011101000101000101100011010001101001", 
      "011101001000101110011010100001101001",       "011101001100010111101110111001101010",       "011101010000000001100000100001101010",       "011101010011101011101111010001101011", 
      "011101010111010110011011011001101011",       "011101011011000001100100111001101011",       "011101011110101101001011111001101100",       "011101100010011001010000010001101100", 
      "011101100110000101110010001001101101",       "011101101001110010110001101001101101",       "011101101101100000001110111001101110",       "011101110001001110001001110001101110", 
      "011101110100111100100010011001101111",       "011101111000101011011000111001101111",       "011101111100011010101101010001110000",       "011110000000001010011111101001110000", 
      "011110000011111010101111111001110001",       "011110000111101011011110011001110001",       "011110001011011100101010111001110010",       "011110001111001110010101100001110010", 
      "011110010011000000011110011001110010",       "011110010110110011000101101001110011",       "011110011010100110001011010001110011",       "011110011110011001101111010001110100", 
      "011110100010001101110001101001110100",       "011110100110000010010010101001110101",       "011110101001110111010010010001110101",       "011110101101101100110000100001110110", 
      "011110110001100010101101100001110110",       "011110110101011001001001010001110111",       "011110111001010000000011110001110111",       "011110111101000111011101010001111000", 
      "011111000000111111010101101001111000",       "011111000100110111101101001001111001",       "011111001000110000100011101001111001",       "011111001100101001111001010001111010", 
      "011111010000100011101110001001111010",       "011111010100011110000010010001111011",       "011111011000011000110101101001111011",       "011111011100010100001000100001111100", 
      "011111100000001111111010101001111100",       "011111100100001100001100100001111101",       "011111101000001000111101110001111101",       "011111101100000110001110110001111110", 
      "011111110000000011111111011001111110",       "011111110100000010001111110001111111",       "011111111000000000111111111001111111",       "011111111100000000010000000010000000" 
)
;
begin
          Y1 <= memVar(conv_integer(X1)); 
          Y2 <= memVar(conv_integer(X2)); 
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_18_f400_uid276
--                     (IntAdderClassical_18_f400_uid278)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_18_f400_uid276 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(17 downto 0);
          Y : in  std_logic_vector(17 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(17 downto 0)   );
end entity;

architecture arch of IntAdder_18_f400_uid276 is
signal X_d1 :  std_logic_vector(17 downto 0);
signal Y_d1 :  std_logic_vector(17 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_18_f400_uid283
--                     (IntAdderClassical_18_f400_uid285)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_18_f400_uid283 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(17 downto 0);
          Y : in  std_logic_vector(17 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(17 downto 0)   );
end entity;

architecture arch of IntAdder_18_f400_uid283 is
signal X_d1 :  std_logic_vector(17 downto 0);
signal Y_d1 :  std_logic_vector(17 downto 0);
signal Cin_d1 : std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--              IntMultiplier_UsingDSP_17_18_19_unsigned_uid290
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_17_18_19_unsigned_uid290 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(16 downto 0);
          Y : in  std_logic_vector(17 downto 0);
          R : out  std_logic_vector(18 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_17_18_19_unsigned_uid290 is
signal XX_m291, XX_m291_d1 :  std_logic_vector(17 downto 0);
signal YY_m291, YY_m291_d1 :  std_logic_vector(16 downto 0);
signal DSP_mult_289 :  std_logic_vector(42 downto 0);
signal heap_bh292_w0_0 : std_logic;
signal heap_bh292_w1_0 : std_logic;
signal heap_bh292_w2_0 : std_logic;
signal heap_bh292_w3_0 : std_logic;
signal heap_bh292_w4_0 : std_logic;
signal heap_bh292_w5_0 : std_logic;
signal heap_bh292_w6_0 : std_logic;
signal heap_bh292_w7_0 : std_logic;
signal heap_bh292_w8_0 : std_logic;
signal heap_bh292_w9_0 : std_logic;
signal heap_bh292_w10_0 : std_logic;
signal heap_bh292_w11_0 : std_logic;
signal heap_bh292_w12_0 : std_logic;
signal heap_bh292_w13_0 : std_logic;
signal heap_bh292_w14_0 : std_logic;
signal heap_bh292_w15_0 : std_logic;
signal heap_bh292_w16_0 : std_logic;
signal heap_bh292_w17_0 : std_logic;
signal heap_bh292_w18_0 : std_logic;
signal heap_bh292_w19_0 : std_logic;
signal heap_bh292_w20_0 : std_logic;
signal heap_bh292_w21_0 : std_logic;
signal heap_bh292_w22_0 : std_logic;
signal heap_bh292_w23_0 : std_logic;
signal CompressionResult292 :  std_logic_vector(23 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            XX_m291_d1 <=  XX_m291;
            YY_m291_d1 <=  YY_m291;
         end if;
      end process;
   XX_m291 <= Y ;
   YY_m291 <= X ;
   ----------------Synchro barrier, entering cycle 1----------------
   DSP_mult_289 <= (("0000000") & XX_m291_d1) * (("0") & YY_m291_d1);
   heap_bh292_w0_0 <= DSP_mult_289(11); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w1_0 <= DSP_mult_289(12); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w2_0 <= DSP_mult_289(13); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w3_0 <= DSP_mult_289(14); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w4_0 <= DSP_mult_289(15); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w5_0 <= DSP_mult_289(16); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w6_0 <= DSP_mult_289(17); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w7_0 <= DSP_mult_289(18); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w8_0 <= DSP_mult_289(19); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w9_0 <= DSP_mult_289(20); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w10_0 <= DSP_mult_289(21); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w11_0 <= DSP_mult_289(22); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w12_0 <= DSP_mult_289(23); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w13_0 <= DSP_mult_289(24); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w14_0 <= DSP_mult_289(25); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w15_0 <= DSP_mult_289(26); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w16_0 <= DSP_mult_289(27); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w17_0 <= DSP_mult_289(28); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w18_0 <= DSP_mult_289(29); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w19_0 <= DSP_mult_289(30); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w20_0 <= DSP_mult_289(31); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w21_0 <= DSP_mult_289(32); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w22_0 <= DSP_mult_289(33); -- cycle= 1 cp= 2.387e-09
   heap_bh292_w23_0 <= DSP_mult_289(34); -- cycle= 1 cp= 2.387e-09
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits

   ----------------Synchro barrier, entering cycle 1----------------
   CompressionResult292 <= heap_bh292_w23_0 & heap_bh292_w22_0 & heap_bh292_w21_0 & heap_bh292_w20_0 & heap_bh292_w19_0 & heap_bh292_w18_0 & heap_bh292_w17_0 & heap_bh292_w16_0 & heap_bh292_w15_0 & heap_bh292_w14_0 & heap_bh292_w13_0 & heap_bh292_w12_0 & heap_bh292_w11_0 & heap_bh292_w10_0 & heap_bh292_w9_0 & heap_bh292_w8_0 & heap_bh292_w7_0 & heap_bh292_w6_0 & heap_bh292_w5_0 & heap_bh292_w4_0 & heap_bh292_w3_0 & heap_bh292_w2_0 & heap_bh292_w1_0 & heap_bh292_w0_0;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult292(23 downto 5);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_27_f400_uid295
--                     (IntAdderClassical_27_f400_uid297)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_27_f400_uid295 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(26 downto 0);
          Y : in  std_logic_vector(26 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(26 downto 0)   );
end entity;

architecture arch of IntAdder_27_f400_uid295 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_33_f400_uid302
--                    (IntAdderAlternative_33_f400_uid306)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_33_f400_uid302 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(32 downto 0);
          Y : in  std_logic_vector(32 downto 0);
          Cin : in std_logic;
          R : out  std_logic_vector(32 downto 0)   );
end entity;

architecture arch of IntAdder_33_f400_uid302 is
signal s_sum_l0_idx0 :  std_logic_vector(25 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(8 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(24 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(7 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(8 downto 0);
signal sum_l1_idx1 :  std_logic_vector(7 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(24 downto 0)) + ( "0" & Y(24 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(32 downto 25)) + ( "0" & Y(32 downto 25));
   sum_l0_idx0 <= s_sum_l0_idx0(24 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(25 downto 25);
   sum_l0_idx1 <= s_sum_l0_idx1(7 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(8 downto 8);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(7 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(8 downto 8);
   R <= sum_l1_idx1(7 downto 0) & sum_l0_idx0_d1(24 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                               FPExp_8_23_400
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, Bogdan Pasca (2008-2013)
--------------------------------------------------------------------------------
-- Pipeline depth: 14 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPExp_8_23_400 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8+34+2 downto 0);
          R : out  std_logic_vector(8+23+2 downto 0)   );
end entity;

architecture arch of FPExp_8_23_400 is
   component FixRealKCM_0_7_M26_log_2_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             R : out  std_logic_vector(33 downto 0)   );
   end component;

   component FixRealKCM_M3_6_0_1_log_2_unsigned is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(9 downto 0);
             R : out  std_logic_vector(7 downto 0)   );
   end component;

   component IntAdder_18_f400_uid276 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(17 downto 0);
             Y : in  std_logic_vector(17 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(17 downto 0)   );
   end component;

   component IntAdder_18_f400_uid283 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(17 downto 0);
             Y : in  std_logic_vector(17 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(17 downto 0)   );
   end component;

   component IntAdder_26_f484_uid267 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(25 downto 0);
             Y : in  std_logic_vector(25 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(25 downto 0)   );
   end component;

   component IntAdder_27_f400_uid295 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(26 downto 0);
             Y : in  std_logic_vector(26 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(26 downto 0)   );
   end component;

   component IntAdder_33_f400_uid302 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(32 downto 0);
             Y : in  std_logic_vector(32 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(32 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_17_18_19_unsigned_uid290 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(16 downto 0);
             Y : in  std_logic_vector(17 downto 0);
             R : out  std_logic_vector(18 downto 0)   );
   end component;

   component LeftShifter_35_by_max_33_uid238 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(34 downto 0);
             S : in  std_logic_vector(5 downto 0);
             R : out  std_logic_vector(67 downto 0)   );
   end component;

   component MagicSPExpTable is
      port ( X1 : in  std_logic_vector(8 downto 0);
             Y1 : out  std_logic_vector(35 downto 0);
             X2 : in  std_logic_vector(8 downto 0);
             Y2 : out  std_logic_vector(35 downto 0)   );
   end component;

signal Xexn, Xexn_d1, Xexn_d2, Xexn_d3, Xexn_d4, Xexn_d5, Xexn_d6, Xexn_d7, Xexn_d8, Xexn_d9, Xexn_d10, Xexn_d11, Xexn_d12, Xexn_d13, Xexn_d14 :  std_logic_vector(1 downto 0);
signal XSign, XSign_d1, XSign_d2, XSign_d3, XSign_d4, XSign_d5, XSign_d6, XSign_d7, XSign_d8, XSign_d9, XSign_d10, XSign_d11, XSign_d12, XSign_d13, XSign_d14 : std_logic;
signal XexpField :  std_logic_vector(7 downto 0);
signal Xfrac :  std_logic_vector(33 downto 0);
signal e0 :  std_logic_vector(9 downto 0);
signal shiftVal, shiftVal_d1 :  std_logic_vector(9 downto 0);
signal resultWillBeOne, resultWillBeOne_d1, resultWillBeOne_d2, resultWillBeOne_d3 : std_logic;
signal mXu :  std_logic_vector(34 downto 0);
signal oufl0, oufl0_d1, oufl0_d2, oufl0_d3, oufl0_d4, oufl0_d5, oufl0_d6, oufl0_d7, oufl0_d8, oufl0_d9, oufl0_d10, oufl0_d11, oufl0_d12, oufl0_d13 : std_logic;
signal shiftValIn :  std_logic_vector(5 downto 0);
signal fixX0, fixX0_d1 :  std_logic_vector(67 downto 0);
signal fixX, fixX_d1, fixX_d2, fixX_d3, fixX_d4 :  std_logic_vector(33 downto 0);
signal xMulIn :  std_logic_vector(9 downto 0);
signal absK :  std_logic_vector(7 downto 0);
signal minusAbsK :  std_logic_vector(8 downto 0);
signal K, K_d1, K_d2, K_d3, K_d4, K_d5, K_d6, K_d7, K_d8 :  std_logic_vector(8 downto 0);
signal absKLog2 :  std_logic_vector(33 downto 0);
signal subOp1 :  std_logic_vector(25 downto 0);
signal subOp2 :  std_logic_vector(25 downto 0);
signal Y :  std_logic_vector(25 downto 0);
signal Addr1, Addr1_d1 :  std_logic_vector(8 downto 0);
signal Z, Z_d1 :  std_logic_vector(16 downto 0);
signal Addr2, Addr2_d1 :  std_logic_vector(8 downto 0);
signal expZ_output :  std_logic_vector(35 downto 0);
signal expA_output :  std_logic_vector(35 downto 0);
signal expA, expA_d1, expA_d2, expA_d3 :  std_logic_vector(26 downto 0);
signal expZmZm1 :  std_logic_vector(8 downto 0);
signal expZminus1X :  std_logic_vector(17 downto 0);
signal expZminus1Y :  std_logic_vector(17 downto 0);
signal expZminus1 :  std_logic_vector(17 downto 0);
signal expArounded0 :  std_logic_vector(17 downto 0);
signal expArounded :  std_logic_vector(16 downto 0);
signal lowerProduct, lowerProduct_d1 :  std_logic_vector(18 downto 0);
signal extendedLowerProduct :  std_logic_vector(26 downto 0);
signal expY, expY_d1 :  std_logic_vector(26 downto 0);
signal needNoNorm, needNoNorm_d1 : std_logic;
signal preRoundBiasSig :  std_logic_vector(32 downto 0);
signal roundBit : std_logic;
signal roundNormAddend :  std_logic_vector(32 downto 0);
signal roundedExpSigRes :  std_logic_vector(32 downto 0);
signal roundedExpSig :  std_logic_vector(32 downto 0);
signal ofl1 : std_logic;
signal ofl2 : std_logic;
signal ofl3 : std_logic;
signal ofl : std_logic;
signal ufl1 : std_logic;
signal ufl2 : std_logic;
signal ufl3 : std_logic;
signal ufl : std_logic;
signal Rexn :  std_logic_vector(1 downto 0);
constant g: positive := 3;
constant wE: positive := 8;
constant wF: positive := 23;
constant wFIn: positive := 34;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xexn_d1 <=  Xexn;
            Xexn_d2 <=  Xexn_d1;
            Xexn_d3 <=  Xexn_d2;
            Xexn_d4 <=  Xexn_d3;
            Xexn_d5 <=  Xexn_d4;
            Xexn_d6 <=  Xexn_d5;
            Xexn_d7 <=  Xexn_d6;
            Xexn_d8 <=  Xexn_d7;
            Xexn_d9 <=  Xexn_d8;
            Xexn_d10 <=  Xexn_d9;
            Xexn_d11 <=  Xexn_d10;
            Xexn_d12 <=  Xexn_d11;
            Xexn_d13 <=  Xexn_d12;
            Xexn_d14 <=  Xexn_d13;
            XSign_d1 <=  XSign;
            XSign_d2 <=  XSign_d1;
            XSign_d3 <=  XSign_d2;
            XSign_d4 <=  XSign_d3;
            XSign_d5 <=  XSign_d4;
            XSign_d6 <=  XSign_d5;
            XSign_d7 <=  XSign_d6;
            XSign_d8 <=  XSign_d7;
            XSign_d9 <=  XSign_d8;
            XSign_d10 <=  XSign_d9;
            XSign_d11 <=  XSign_d10;
            XSign_d12 <=  XSign_d11;
            XSign_d13 <=  XSign_d12;
            XSign_d14 <=  XSign_d13;
            shiftVal_d1 <=  shiftVal;
            resultWillBeOne_d1 <=  resultWillBeOne;
            resultWillBeOne_d2 <=  resultWillBeOne_d1;
            resultWillBeOne_d3 <=  resultWillBeOne_d2;
            oufl0_d1 <=  oufl0;
            oufl0_d2 <=  oufl0_d1;
            oufl0_d3 <=  oufl0_d2;
            oufl0_d4 <=  oufl0_d3;
            oufl0_d5 <=  oufl0_d4;
            oufl0_d6 <=  oufl0_d5;
            oufl0_d7 <=  oufl0_d6;
            oufl0_d8 <=  oufl0_d7;
            oufl0_d9 <=  oufl0_d8;
            oufl0_d10 <=  oufl0_d9;
            oufl0_d11 <=  oufl0_d10;
            oufl0_d12 <=  oufl0_d11;
            oufl0_d13 <=  oufl0_d12;
            fixX0_d1 <=  fixX0;
            fixX_d1 <=  fixX;
            fixX_d2 <=  fixX_d1;
            fixX_d3 <=  fixX_d2;
            fixX_d4 <=  fixX_d3;
            K_d1 <=  K;
            K_d2 <=  K_d1;
            K_d3 <=  K_d2;
            K_d4 <=  K_d3;
            K_d5 <=  K_d4;
            K_d6 <=  K_d5;
            K_d7 <=  K_d6;
            K_d8 <=  K_d7;
            Addr1_d1 <=  Addr1;
            Z_d1 <=  Z;
            Addr2_d1 <=  Addr2;
            expA_d1 <=  expA;
            expA_d2 <=  expA_d1;
            expA_d3 <=  expA_d2;
            lowerProduct_d1 <=  lowerProduct;
            expY_d1 <=  expY;
            needNoNorm_d1 <=  needNoNorm;
         end if;
      end process;
   Xexn <= X(wE+wFIn+2 downto wE+wFIn+1);
   XSign <= X(wE+wFIn);
   XexpField <= X(wE+wFIn-1 downto wFIn);
   Xfrac <= X(wFIn-1 downto 0);
   e0 <= conv_std_logic_vector(101, wE+2);  -- bias - (wF+g)
   shiftVal <= ("00" & XexpField) - e0; -- for a left shift
   -- underflow when input is shifted to zero (shiftval<0), in which case exp = 1
   resultWillBeOne <= shiftVal(wE+1);
   --  mantissa with implicit bit
   mXu <= "1" & Xfrac;
   -- Partial overflow/underflow detection
   ----------------Synchro barrier, entering cycle 1----------------
   oufl0 <= not shiftVal_d1(wE+1) when shiftVal_d1(wE downto 0) >= conv_std_logic_vector(33, wE+1) else '0';
   ---------------- cycle 0----------------
   shiftValIn <= shiftVal(5 downto 0);
   mantissa_shift: LeftShifter_35_by_max_33_uid238  -- pipelineDepth=2 maxInDelay=2.52864e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => fixX0,
                 S => shiftValIn,
                 X => mXu);
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   fixX <=  fixX0_d1(67 downto 34)when resultWillBeOne_d3='0' else "0000000000000000000000000000000000";
   xMulIn <=  fixX(32 downto 23); -- truncation, error 2^-3
   mulInvLog2: FixRealKCM_M3_6_0_1_log_2_unsigned  -- pipelineDepth=2 maxInDelay=1.27192e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => absK,
                 X => xMulIn);
   ----------------Synchro barrier, entering cycle 5----------------
   minusAbsK <= (8 downto 0 => '0') - ('0' & absK);
   K <= minusAbsK when  XSign_d5='1'   else ('0' & absK);
   ---------------- cycle 5----------------
   mulLog2: FixRealKCM_0_7_M26_log_2_unsigned  -- pipelineDepth=2 maxInDelay=6.6272e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => absKLog2,
                 X => absK);
   ----------------Synchro barrier, entering cycle 7----------------
   subOp1 <= fixX_d4(25 downto 0) when XSign_d7='0' else not (fixX_d4(25 downto 0));
   subOp2 <= absKLog2(25 downto 0) when XSign_d7='1' else not (absKLog2(25 downto 0));
   theYAdder: IntAdder_26_f484_uid267  -- pipelineDepth=1 maxInDelay=9.7544e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => Y,
                 X => subOp1,
                 Y => subOp2);

   ----------------Synchro barrier, entering cycle 8----------------
   -- Now compute the exp of this fixed-point value
   Addr1 <= Y(25 downto 17);
   Z <= Y(16 downto 0);
   Addr2 <= Z(16 downto 8);
   ----------------Synchro barrier, entering cycle 9----------------
   table: MagicSPExpTable
      port map ( X1 => Addr1_d1,
                 X2 => Addr2_d1,
                 Y1 => expA_output,
                 Y2 => expZ_output);
   expA <=  expA_output(35 downto 9);
   expZmZm1 <= expZ_output(8 downto 0);
   -- Computing Z + (exp(Z)-1-Z)
   expZminus1X <= '0' & Z_d1;
   expZminus1Y <= (17 downto 9 => '0') & expZmZm1 ;
   Adder_expZminus1: IntAdder_18_f400_uid276  -- pipelineDepth=1 maxInDelay=2.19472e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => expZminus1,
                 X => expZminus1X,
                 Y => expZminus1Y);
   ----------------Synchro barrier, entering cycle 10----------------
   ---------------- cycle 9----------------
   -- Rounding expA to the same accuracy as expZminus1
   --   (truncation would not be accurate enough and require one more guard bit)
   Adder_expArounded0: IntAdder_18_f400_uid283  -- pipelineDepth=1 maxInDelay=2.186e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1' ,
                 R => expArounded0,
                 X => expA(26 downto 9),
                 Y => "000000000000000000");
   ----------------Synchro barrier, entering cycle 10----------------
   expArounded <= expArounded0(17 downto 1);
   TheLowerProduct: IntMultiplier_UsingDSP_17_18_19_unsigned_uid290  -- pipelineDepth=1 maxInDelay=1.518e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => lowerProduct,
                 X => expArounded,
                 Y => expZminus1);

   ----------------Synchro barrier, entering cycle 11----------------
   ----------------Synchro barrier, entering cycle 12----------------
   extendedLowerProduct <= ((26 downto 19 => '0') & lowerProduct_d1(18 downto 0));
   -- Final addition -- the product MSB bit weight is -k+2 = -7
   TheFinalAdder: IntAdder_27_f400_uid295  -- pipelineDepth=0 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => expY,
                 X => expA_d3,
                 Y => extendedLowerProduct);

   needNoNorm <= expY(26);
   ----------------Synchro barrier, entering cycle 13----------------
   -- Rounding: all this should consume one row of LUTs
   preRoundBiasSig <= conv_std_logic_vector(127, wE+2)  & expY_d1(25 downto 3) when needNoNorm_d1 = '1'
      else conv_std_logic_vector(126, wE+2)  & expY_d1(24 downto 2) ;
   roundBit <= expY_d1(2)  when needNoNorm_d1 = '1'    else expY_d1(1) ;
   roundNormAddend <= K_d8(8) & K_d8 & (22 downto 1 => '0') & roundBit;
   roundedExpSigOperandAdder: IntAdder_33_f400_uid302  -- pipelineDepth=1 maxInDelay=1.25448e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => roundedExpSigRes,
                 X => preRoundBiasSig,
                 Y => roundNormAddend);

   ----------------Synchro barrier, entering cycle 14----------------
   -- delay at adder output is 8.52e-10
   roundedExpSig <= roundedExpSigRes when Xexn_d14="01" else  "000" & (wE-2 downto 0 => '1') & (wF-1 downto 0 => '0');
   ofl1 <= not XSign_d14 and oufl0_d13 and (not Xexn_d14(1) and Xexn_d14(0)); -- input positive, normal,  very large
   ofl2 <= not XSign_d14 and (roundedExpSig(wE+wF) and not roundedExpSig(wE+wF+1)) and (not Xexn_d14(1) and Xexn_d14(0)); -- input positive, normal, overflowed
   ofl3 <= not XSign_d14 and Xexn_d14(1) and not Xexn_d14(0);  -- input was -infty
   ofl <= ofl1 or ofl2 or ofl3;
   ufl1 <= (roundedExpSig(wE+wF) and roundedExpSig(wE+wF+1))  and (not Xexn_d14(1) and Xexn_d14(0)); -- input normal
   ufl2 <= XSign_d14 and Xexn_d14(1) and not Xexn_d14(0);  -- input was -infty
   ufl3 <= XSign_d14 and oufl0_d13  and (not Xexn_d14(1) and Xexn_d14(0)); -- input negative, normal,  very large
   ufl <= ufl1 or ufl2 or ufl3;
   Rexn <= "11" when Xexn_d14 = "11"
      else "10" when ofl='1'
      else "00" when ufl='1'
      else "01";
   R <= Rexn & '0' & roundedExpSig(30 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                               FPPow_8_23_400
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, C. Klein  (2008)
--------------------------------------------------------------------------------
-- Pipeline depth: 46 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPPow_8_23_400 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8+23+2 downto 0);
          Y : in  std_logic_vector(8+23+2 downto 0);
          R : out  std_logic_vector(8+23+2 downto 0)   );
end entity;

architecture arch of FPPow_8_23_400 is
   component FPExp_8_23_400 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8+34+2 downto 0);
             R : out  std_logic_vector(8+23+2 downto 0)   );
   end component;

   component FPLog_8_33_9_400 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8+33+2 downto 0);
             R : out  std_logic_vector(8+33+2 downto 0)   );
   end component;

   component FPMultiplier_8_33_8_23_8_34_uid197 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8+33+2 downto 0);
             Y : in  std_logic_vector(8+23+2 downto 0);
             R : out  std_logic_vector(8+34+2 downto 0)   );
   end component;

   component IntAdder_32_f400_uid3 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(31 downto 0);
             Y : in  std_logic_vector(31 downto 0);
             Cin : in std_logic;
             R : out  std_logic_vector(31 downto 0)   );
   end component;

   component LZOC_23_5_uid10 is
      port ( clk, rst : in std_logic;
             I : in  std_logic_vector(22 downto 0);
             OZB : in std_logic;
             O : out  std_logic_vector(4 downto 0)   );
   end component;

signal flagsX :  std_logic_vector(1 downto 0);
signal signX, signX_d1, signX_d2, signX_d3, signX_d4, signX_d5 : std_logic;
signal expFieldX :  std_logic_vector(7 downto 0);
signal fracX :  std_logic_vector(22 downto 0);
signal flagsY :  std_logic_vector(1 downto 0);
signal signY, signY_d1, signY_d2, signY_d3, signY_d4, signY_d5 : std_logic;
signal expFieldY, expFieldY_d1, expFieldY_d2, expFieldY_d3, expFieldY_d4 :  std_logic_vector(7 downto 0);
signal fracY :  std_logic_vector(22 downto 0);
signal zeroX, zeroX_d1, zeroX_d2, zeroX_d3, zeroX_d4, zeroX_d5 : std_logic;
signal zeroY, zeroY_d1, zeroY_d2, zeroY_d3, zeroY_d4, zeroY_d5 : std_logic;
signal normalX, normalX_d1, normalX_d2, normalX_d3, normalX_d4, normalX_d5 : std_logic;
signal normalY, normalY_d1, normalY_d2, normalY_d3, normalY_d4, normalY_d5 : std_logic;
signal infX, infX_d1, infX_d2, infX_d3, infX_d4, infX_d5 : std_logic;
signal infY, infY_d1, infY_d2, infY_d3, infY_d4, infY_d5 : std_logic;
signal s_nan_in, s_nan_in_d1, s_nan_in_d2, s_nan_in_d3, s_nan_in_d4, s_nan_in_d5 : std_logic;
signal OneExpFrac, OneExpFrac_d1 :  std_logic_vector(30 downto 0);
signal ExpFracX :  std_logic_vector(31 downto 0);
signal OneExpFracCompl :  std_logic_vector(31 downto 0);
signal cmpXOneRes, cmpXOneRes_d1 :  std_logic_vector(31 downto 0);
signal XisOneAndNormal, XisOneAndNormal_d1, XisOneAndNormal_d2, XisOneAndNormal_d3, XisOneAndNormal_d4 : std_logic;
signal absXgtOneAndNormal, absXgtOneAndNormal_d1, absXgtOneAndNormal_d2, absXgtOneAndNormal_d3, absXgtOneAndNormal_d4 : std_logic;
signal absXltOneAndNormal, absXltOneAndNormal_d1, absXltOneAndNormal_d2, absXltOneAndNormal_d3, absXltOneAndNormal_d4 : std_logic;
signal fracYreverted :  std_logic_vector(22 downto 0);
signal Z_rightY, Z_rightY_d1 :  std_logic_vector(4 downto 0);
signal WeightLSBYpre :  std_logic_vector(8 downto 0);
signal WeightLSBY, WeightLSBY_d1 :  std_logic_vector(8 downto 0);
signal oddIntY : std_logic;
signal evenIntY : std_logic;
signal notIntNormalY : std_logic;
signal RisInfSpecialCase, RisInfSpecialCase_d1, RisInfSpecialCase_d2, RisInfSpecialCase_d3, RisInfSpecialCase_d4, RisInfSpecialCase_d5, RisInfSpecialCase_d6, RisInfSpecialCase_d7, RisInfSpecialCase_d8, RisInfSpecialCase_d9, RisInfSpecialCase_d10, RisInfSpecialCase_d11, RisInfSpecialCase_d12, RisInfSpecialCase_d13, RisInfSpecialCase_d14, RisInfSpecialCase_d15, RisInfSpecialCase_d16, RisInfSpecialCase_d17, RisInfSpecialCase_d18, RisInfSpecialCase_d19, RisInfSpecialCase_d20, RisInfSpecialCase_d21, RisInfSpecialCase_d22, RisInfSpecialCase_d23, RisInfSpecialCase_d24, RisInfSpecialCase_d25, RisInfSpecialCase_d26, RisInfSpecialCase_d27, RisInfSpecialCase_d28, RisInfSpecialCase_d29, RisInfSpecialCase_d30, RisInfSpecialCase_d31, RisInfSpecialCase_d32, RisInfSpecialCase_d33, RisInfSpecialCase_d34, RisInfSpecialCase_d35, RisInfSpecialCase_d36, RisInfSpecialCase_d37, RisInfSpecialCase_d38, RisInfSpecialCase_d39, RisInfSpecialCase_d40, RisInfSpecialCase_d41 : std_logic;
signal RisZeroSpecialCase, RisZeroSpecialCase_d1, RisZeroSpecialCase_d2, RisZeroSpecialCase_d3, RisZeroSpecialCase_d4, RisZeroSpecialCase_d5, RisZeroSpecialCase_d6, RisZeroSpecialCase_d7, RisZeroSpecialCase_d8, RisZeroSpecialCase_d9, RisZeroSpecialCase_d10, RisZeroSpecialCase_d11, RisZeroSpecialCase_d12, RisZeroSpecialCase_d13, RisZeroSpecialCase_d14, RisZeroSpecialCase_d15, RisZeroSpecialCase_d16, RisZeroSpecialCase_d17, RisZeroSpecialCase_d18, RisZeroSpecialCase_d19, RisZeroSpecialCase_d20, RisZeroSpecialCase_d21, RisZeroSpecialCase_d22, RisZeroSpecialCase_d23, RisZeroSpecialCase_d24, RisZeroSpecialCase_d25, RisZeroSpecialCase_d26, RisZeroSpecialCase_d27, RisZeroSpecialCase_d28, RisZeroSpecialCase_d29, RisZeroSpecialCase_d30, RisZeroSpecialCase_d31, RisZeroSpecialCase_d32, RisZeroSpecialCase_d33, RisZeroSpecialCase_d34, RisZeroSpecialCase_d35, RisZeroSpecialCase_d36, RisZeroSpecialCase_d37, RisZeroSpecialCase_d38, RisZeroSpecialCase_d39, RisZeroSpecialCase_d40, RisZeroSpecialCase_d41 : std_logic;
signal RisOne, RisOne_d1, RisOne_d2, RisOne_d3, RisOne_d4, RisOne_d5, RisOne_d6, RisOne_d7, RisOne_d8, RisOne_d9, RisOne_d10, RisOne_d11, RisOne_d12, RisOne_d13, RisOne_d14, RisOne_d15, RisOne_d16, RisOne_d17, RisOne_d18, RisOne_d19, RisOne_d20, RisOne_d21, RisOne_d22, RisOne_d23, RisOne_d24, RisOne_d25, RisOne_d26, RisOne_d27, RisOne_d28, RisOne_d29, RisOne_d30, RisOne_d31, RisOne_d32, RisOne_d33, RisOne_d34, RisOne_d35, RisOne_d36, RisOne_d37, RisOne_d38, RisOne_d39, RisOne_d40, RisOne_d41 : std_logic;
signal RisNaN, RisNaN_d1, RisNaN_d2, RisNaN_d3, RisNaN_d4, RisNaN_d5, RisNaN_d6, RisNaN_d7, RisNaN_d8, RisNaN_d9, RisNaN_d10, RisNaN_d11, RisNaN_d12, RisNaN_d13, RisNaN_d14, RisNaN_d15, RisNaN_d16, RisNaN_d17, RisNaN_d18, RisNaN_d19, RisNaN_d20, RisNaN_d21, RisNaN_d22, RisNaN_d23, RisNaN_d24, RisNaN_d25, RisNaN_d26, RisNaN_d27, RisNaN_d28, RisNaN_d29, RisNaN_d30, RisNaN_d31, RisNaN_d32, RisNaN_d33, RisNaN_d34, RisNaN_d35, RisNaN_d36, RisNaN_d37, RisNaN_d38, RisNaN_d39, RisNaN_d40, RisNaN_d41 : std_logic;
signal signR, signR_d1, signR_d2, signR_d3, signR_d4, signR_d5, signR_d6, signR_d7, signR_d8, signR_d9, signR_d10, signR_d11, signR_d12, signR_d13, signR_d14, signR_d15, signR_d16, signR_d17, signR_d18, signR_d19, signR_d20, signR_d21, signR_d22, signR_d23, signR_d24, signR_d25, signR_d26, signR_d27, signR_d28, signR_d29, signR_d30, signR_d31, signR_d32, signR_d33, signR_d34, signR_d35, signR_d36, signR_d37, signR_d38, signR_d39, signR_d40, signR_d41 : std_logic;
signal logIn :  std_logic_vector(43 downto 0);
signal lnX, lnX_d1 :  std_logic_vector(43 downto 0);
signal P, P_d1 :  std_logic_vector(44 downto 0);
signal E, E_d1 :  std_logic_vector(33 downto 0);
signal flagsE :  std_logic_vector(1 downto 0);
signal RisZeroFromExp : std_logic;
signal RisZero : std_logic;
signal RisInfFromExp : std_logic;
signal RisInf : std_logic;
signal flagR :  std_logic_vector(1 downto 0);
signal R_expfrac :  std_logic_vector(30 downto 0);
signal X_d1 :  std_logic_vector(8+23+2 downto 0);
signal Y_d1, Y_d2, Y_d3, Y_d4, Y_d5, Y_d6, Y_d7, Y_d8, Y_d9, Y_d10, Y_d11, Y_d12, Y_d13, Y_d14, Y_d15, Y_d16, Y_d17, Y_d18, Y_d19, Y_d20, Y_d21, Y_d22, Y_d23, Y_d24 :  std_logic_vector(8+23+2 downto 0);
constant wE: positive := 8;
constant wF: positive := 23;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            signX_d1 <=  signX;
            signX_d2 <=  signX_d1;
            signX_d3 <=  signX_d2;
            signX_d4 <=  signX_d3;
            signX_d5 <=  signX_d4;
            signY_d1 <=  signY;
            signY_d2 <=  signY_d1;
            signY_d3 <=  signY_d2;
            signY_d4 <=  signY_d3;
            signY_d5 <=  signY_d4;
            expFieldY_d1 <=  expFieldY;
            expFieldY_d2 <=  expFieldY_d1;
            expFieldY_d3 <=  expFieldY_d2;
            expFieldY_d4 <=  expFieldY_d3;
            zeroX_d1 <=  zeroX;
            zeroX_d2 <=  zeroX_d1;
            zeroX_d3 <=  zeroX_d2;
            zeroX_d4 <=  zeroX_d3;
            zeroX_d5 <=  zeroX_d4;
            zeroY_d1 <=  zeroY;
            zeroY_d2 <=  zeroY_d1;
            zeroY_d3 <=  zeroY_d2;
            zeroY_d4 <=  zeroY_d3;
            zeroY_d5 <=  zeroY_d4;
            normalX_d1 <=  normalX;
            normalX_d2 <=  normalX_d1;
            normalX_d3 <=  normalX_d2;
            normalX_d4 <=  normalX_d3;
            normalX_d5 <=  normalX_d4;
            normalY_d1 <=  normalY;
            normalY_d2 <=  normalY_d1;
            normalY_d3 <=  normalY_d2;
            normalY_d4 <=  normalY_d3;
            normalY_d5 <=  normalY_d4;
            infX_d1 <=  infX;
            infX_d2 <=  infX_d1;
            infX_d3 <=  infX_d2;
            infX_d4 <=  infX_d3;
            infX_d5 <=  infX_d4;
            infY_d1 <=  infY;
            infY_d2 <=  infY_d1;
            infY_d3 <=  infY_d2;
            infY_d4 <=  infY_d3;
            infY_d5 <=  infY_d4;
            s_nan_in_d1 <=  s_nan_in;
            s_nan_in_d2 <=  s_nan_in_d1;
            s_nan_in_d3 <=  s_nan_in_d2;
            s_nan_in_d4 <=  s_nan_in_d3;
            s_nan_in_d5 <=  s_nan_in_d4;
            OneExpFrac_d1 <=  OneExpFrac;
            cmpXOneRes_d1 <=  cmpXOneRes;
            XisOneAndNormal_d1 <=  XisOneAndNormal;
            XisOneAndNormal_d2 <=  XisOneAndNormal_d1;
            XisOneAndNormal_d3 <=  XisOneAndNormal_d2;
            XisOneAndNormal_d4 <=  XisOneAndNormal_d3;
            absXgtOneAndNormal_d1 <=  absXgtOneAndNormal;
            absXgtOneAndNormal_d2 <=  absXgtOneAndNormal_d1;
            absXgtOneAndNormal_d3 <=  absXgtOneAndNormal_d2;
            absXgtOneAndNormal_d4 <=  absXgtOneAndNormal_d3;
            absXltOneAndNormal_d1 <=  absXltOneAndNormal;
            absXltOneAndNormal_d2 <=  absXltOneAndNormal_d1;
            absXltOneAndNormal_d3 <=  absXltOneAndNormal_d2;
            absXltOneAndNormal_d4 <=  absXltOneAndNormal_d3;
            Z_rightY_d1 <=  Z_rightY;
            WeightLSBY_d1 <=  WeightLSBY;
            RisInfSpecialCase_d1 <=  RisInfSpecialCase;
            RisInfSpecialCase_d2 <=  RisInfSpecialCase_d1;
            RisInfSpecialCase_d3 <=  RisInfSpecialCase_d2;
            RisInfSpecialCase_d4 <=  RisInfSpecialCase_d3;
            RisInfSpecialCase_d5 <=  RisInfSpecialCase_d4;
            RisInfSpecialCase_d6 <=  RisInfSpecialCase_d5;
            RisInfSpecialCase_d7 <=  RisInfSpecialCase_d6;
            RisInfSpecialCase_d8 <=  RisInfSpecialCase_d7;
            RisInfSpecialCase_d9 <=  RisInfSpecialCase_d8;
            RisInfSpecialCase_d10 <=  RisInfSpecialCase_d9;
            RisInfSpecialCase_d11 <=  RisInfSpecialCase_d10;
            RisInfSpecialCase_d12 <=  RisInfSpecialCase_d11;
            RisInfSpecialCase_d13 <=  RisInfSpecialCase_d12;
            RisInfSpecialCase_d14 <=  RisInfSpecialCase_d13;
            RisInfSpecialCase_d15 <=  RisInfSpecialCase_d14;
            RisInfSpecialCase_d16 <=  RisInfSpecialCase_d15;
            RisInfSpecialCase_d17 <=  RisInfSpecialCase_d16;
            RisInfSpecialCase_d18 <=  RisInfSpecialCase_d17;
            RisInfSpecialCase_d19 <=  RisInfSpecialCase_d18;
            RisInfSpecialCase_d20 <=  RisInfSpecialCase_d19;
            RisInfSpecialCase_d21 <=  RisInfSpecialCase_d20;
            RisInfSpecialCase_d22 <=  RisInfSpecialCase_d21;
            RisInfSpecialCase_d23 <=  RisInfSpecialCase_d22;
            RisInfSpecialCase_d24 <=  RisInfSpecialCase_d23;
            RisInfSpecialCase_d25 <=  RisInfSpecialCase_d24;
            RisInfSpecialCase_d26 <=  RisInfSpecialCase_d25;
            RisInfSpecialCase_d27 <=  RisInfSpecialCase_d26;
            RisInfSpecialCase_d28 <=  RisInfSpecialCase_d27;
            RisInfSpecialCase_d29 <=  RisInfSpecialCase_d28;
            RisInfSpecialCase_d30 <=  RisInfSpecialCase_d29;
            RisInfSpecialCase_d31 <=  RisInfSpecialCase_d30;
            RisInfSpecialCase_d32 <=  RisInfSpecialCase_d31;
            RisInfSpecialCase_d33 <=  RisInfSpecialCase_d32;
            RisInfSpecialCase_d34 <=  RisInfSpecialCase_d33;
            RisInfSpecialCase_d35 <=  RisInfSpecialCase_d34;
            RisInfSpecialCase_d36 <=  RisInfSpecialCase_d35;
            RisInfSpecialCase_d37 <=  RisInfSpecialCase_d36;
            RisInfSpecialCase_d38 <=  RisInfSpecialCase_d37;
            RisInfSpecialCase_d39 <=  RisInfSpecialCase_d38;
            RisInfSpecialCase_d40 <=  RisInfSpecialCase_d39;
            RisInfSpecialCase_d41 <=  RisInfSpecialCase_d40;
            RisZeroSpecialCase_d1 <=  RisZeroSpecialCase;
            RisZeroSpecialCase_d2 <=  RisZeroSpecialCase_d1;
            RisZeroSpecialCase_d3 <=  RisZeroSpecialCase_d2;
            RisZeroSpecialCase_d4 <=  RisZeroSpecialCase_d3;
            RisZeroSpecialCase_d5 <=  RisZeroSpecialCase_d4;
            RisZeroSpecialCase_d6 <=  RisZeroSpecialCase_d5;
            RisZeroSpecialCase_d7 <=  RisZeroSpecialCase_d6;
            RisZeroSpecialCase_d8 <=  RisZeroSpecialCase_d7;
            RisZeroSpecialCase_d9 <=  RisZeroSpecialCase_d8;
            RisZeroSpecialCase_d10 <=  RisZeroSpecialCase_d9;
            RisZeroSpecialCase_d11 <=  RisZeroSpecialCase_d10;
            RisZeroSpecialCase_d12 <=  RisZeroSpecialCase_d11;
            RisZeroSpecialCase_d13 <=  RisZeroSpecialCase_d12;
            RisZeroSpecialCase_d14 <=  RisZeroSpecialCase_d13;
            RisZeroSpecialCase_d15 <=  RisZeroSpecialCase_d14;
            RisZeroSpecialCase_d16 <=  RisZeroSpecialCase_d15;
            RisZeroSpecialCase_d17 <=  RisZeroSpecialCase_d16;
            RisZeroSpecialCase_d18 <=  RisZeroSpecialCase_d17;
            RisZeroSpecialCase_d19 <=  RisZeroSpecialCase_d18;
            RisZeroSpecialCase_d20 <=  RisZeroSpecialCase_d19;
            RisZeroSpecialCase_d21 <=  RisZeroSpecialCase_d20;
            RisZeroSpecialCase_d22 <=  RisZeroSpecialCase_d21;
            RisZeroSpecialCase_d23 <=  RisZeroSpecialCase_d22;
            RisZeroSpecialCase_d24 <=  RisZeroSpecialCase_d23;
            RisZeroSpecialCase_d25 <=  RisZeroSpecialCase_d24;
            RisZeroSpecialCase_d26 <=  RisZeroSpecialCase_d25;
            RisZeroSpecialCase_d27 <=  RisZeroSpecialCase_d26;
            RisZeroSpecialCase_d28 <=  RisZeroSpecialCase_d27;
            RisZeroSpecialCase_d29 <=  RisZeroSpecialCase_d28;
            RisZeroSpecialCase_d30 <=  RisZeroSpecialCase_d29;
            RisZeroSpecialCase_d31 <=  RisZeroSpecialCase_d30;
            RisZeroSpecialCase_d32 <=  RisZeroSpecialCase_d31;
            RisZeroSpecialCase_d33 <=  RisZeroSpecialCase_d32;
            RisZeroSpecialCase_d34 <=  RisZeroSpecialCase_d33;
            RisZeroSpecialCase_d35 <=  RisZeroSpecialCase_d34;
            RisZeroSpecialCase_d36 <=  RisZeroSpecialCase_d35;
            RisZeroSpecialCase_d37 <=  RisZeroSpecialCase_d36;
            RisZeroSpecialCase_d38 <=  RisZeroSpecialCase_d37;
            RisZeroSpecialCase_d39 <=  RisZeroSpecialCase_d38;
            RisZeroSpecialCase_d40 <=  RisZeroSpecialCase_d39;
            RisZeroSpecialCase_d41 <=  RisZeroSpecialCase_d40;
            RisOne_d1 <=  RisOne;
            RisOne_d2 <=  RisOne_d1;
            RisOne_d3 <=  RisOne_d2;
            RisOne_d4 <=  RisOne_d3;
            RisOne_d5 <=  RisOne_d4;
            RisOne_d6 <=  RisOne_d5;
            RisOne_d7 <=  RisOne_d6;
            RisOne_d8 <=  RisOne_d7;
            RisOne_d9 <=  RisOne_d8;
            RisOne_d10 <=  RisOne_d9;
            RisOne_d11 <=  RisOne_d10;
            RisOne_d12 <=  RisOne_d11;
            RisOne_d13 <=  RisOne_d12;
            RisOne_d14 <=  RisOne_d13;
            RisOne_d15 <=  RisOne_d14;
            RisOne_d16 <=  RisOne_d15;
            RisOne_d17 <=  RisOne_d16;
            RisOne_d18 <=  RisOne_d17;
            RisOne_d19 <=  RisOne_d18;
            RisOne_d20 <=  RisOne_d19;
            RisOne_d21 <=  RisOne_d20;
            RisOne_d22 <=  RisOne_d21;
            RisOne_d23 <=  RisOne_d22;
            RisOne_d24 <=  RisOne_d23;
            RisOne_d25 <=  RisOne_d24;
            RisOne_d26 <=  RisOne_d25;
            RisOne_d27 <=  RisOne_d26;
            RisOne_d28 <=  RisOne_d27;
            RisOne_d29 <=  RisOne_d28;
            RisOne_d30 <=  RisOne_d29;
            RisOne_d31 <=  RisOne_d30;
            RisOne_d32 <=  RisOne_d31;
            RisOne_d33 <=  RisOne_d32;
            RisOne_d34 <=  RisOne_d33;
            RisOne_d35 <=  RisOne_d34;
            RisOne_d36 <=  RisOne_d35;
            RisOne_d37 <=  RisOne_d36;
            RisOne_d38 <=  RisOne_d37;
            RisOne_d39 <=  RisOne_d38;
            RisOne_d40 <=  RisOne_d39;
            RisOne_d41 <=  RisOne_d40;
            RisNaN_d1 <=  RisNaN;
            RisNaN_d2 <=  RisNaN_d1;
            RisNaN_d3 <=  RisNaN_d2;
            RisNaN_d4 <=  RisNaN_d3;
            RisNaN_d5 <=  RisNaN_d4;
            RisNaN_d6 <=  RisNaN_d5;
            RisNaN_d7 <=  RisNaN_d6;
            RisNaN_d8 <=  RisNaN_d7;
            RisNaN_d9 <=  RisNaN_d8;
            RisNaN_d10 <=  RisNaN_d9;
            RisNaN_d11 <=  RisNaN_d10;
            RisNaN_d12 <=  RisNaN_d11;
            RisNaN_d13 <=  RisNaN_d12;
            RisNaN_d14 <=  RisNaN_d13;
            RisNaN_d15 <=  RisNaN_d14;
            RisNaN_d16 <=  RisNaN_d15;
            RisNaN_d17 <=  RisNaN_d16;
            RisNaN_d18 <=  RisNaN_d17;
            RisNaN_d19 <=  RisNaN_d18;
            RisNaN_d20 <=  RisNaN_d19;
            RisNaN_d21 <=  RisNaN_d20;
            RisNaN_d22 <=  RisNaN_d21;
            RisNaN_d23 <=  RisNaN_d22;
            RisNaN_d24 <=  RisNaN_d23;
            RisNaN_d25 <=  RisNaN_d24;
            RisNaN_d26 <=  RisNaN_d25;
            RisNaN_d27 <=  RisNaN_d26;
            RisNaN_d28 <=  RisNaN_d27;
            RisNaN_d29 <=  RisNaN_d28;
            RisNaN_d30 <=  RisNaN_d29;
            RisNaN_d31 <=  RisNaN_d30;
            RisNaN_d32 <=  RisNaN_d31;
            RisNaN_d33 <=  RisNaN_d32;
            RisNaN_d34 <=  RisNaN_d33;
            RisNaN_d35 <=  RisNaN_d34;
            RisNaN_d36 <=  RisNaN_d35;
            RisNaN_d37 <=  RisNaN_d36;
            RisNaN_d38 <=  RisNaN_d37;
            RisNaN_d39 <=  RisNaN_d38;
            RisNaN_d40 <=  RisNaN_d39;
            RisNaN_d41 <=  RisNaN_d40;
            signR_d1 <=  signR;
            signR_d2 <=  signR_d1;
            signR_d3 <=  signR_d2;
            signR_d4 <=  signR_d3;
            signR_d5 <=  signR_d4;
            signR_d6 <=  signR_d5;
            signR_d7 <=  signR_d6;
            signR_d8 <=  signR_d7;
            signR_d9 <=  signR_d8;
            signR_d10 <=  signR_d9;
            signR_d11 <=  signR_d10;
            signR_d12 <=  signR_d11;
            signR_d13 <=  signR_d12;
            signR_d14 <=  signR_d13;
            signR_d15 <=  signR_d14;
            signR_d16 <=  signR_d15;
            signR_d17 <=  signR_d16;
            signR_d18 <=  signR_d17;
            signR_d19 <=  signR_d18;
            signR_d20 <=  signR_d19;
            signR_d21 <=  signR_d20;
            signR_d22 <=  signR_d21;
            signR_d23 <=  signR_d22;
            signR_d24 <=  signR_d23;
            signR_d25 <=  signR_d24;
            signR_d26 <=  signR_d25;
            signR_d27 <=  signR_d26;
            signR_d28 <=  signR_d27;
            signR_d29 <=  signR_d28;
            signR_d30 <=  signR_d29;
            signR_d31 <=  signR_d30;
            signR_d32 <=  signR_d31;
            signR_d33 <=  signR_d32;
            signR_d34 <=  signR_d33;
            signR_d35 <=  signR_d34;
            signR_d36 <=  signR_d35;
            signR_d37 <=  signR_d36;
            signR_d38 <=  signR_d37;
            signR_d39 <=  signR_d38;
            signR_d40 <=  signR_d39;
            signR_d41 <=  signR_d40;
            lnX_d1 <=  lnX;
            P_d1 <=  P;
            E_d1 <=  E;
            X_d1 <=  X;
            Y_d1 <=  Y;
            Y_d2 <=  Y_d1;
            Y_d3 <=  Y_d2;
            Y_d4 <=  Y_d3;
            Y_d5 <=  Y_d4;
            Y_d6 <=  Y_d5;
            Y_d7 <=  Y_d6;
            Y_d8 <=  Y_d7;
            Y_d9 <=  Y_d8;
            Y_d10 <=  Y_d9;
            Y_d11 <=  Y_d10;
            Y_d12 <=  Y_d11;
            Y_d13 <=  Y_d12;
            Y_d14 <=  Y_d13;
            Y_d15 <=  Y_d14;
            Y_d16 <=  Y_d15;
            Y_d17 <=  Y_d16;
            Y_d18 <=  Y_d17;
            Y_d19 <=  Y_d18;
            Y_d20 <=  Y_d19;
            Y_d21 <=  Y_d20;
            Y_d22 <=  Y_d21;
            Y_d23 <=  Y_d22;
            Y_d24 <=  Y_d23;
         end if;
      end process;
   flagsX <= X(wE+wF+2 downto wE+wF+1);
   signX <= X(wE+wF);
   expFieldX <= X(wE+wF-1 downto wF);
   fracX <= X(wF-1 downto 0);
   flagsY <= Y(wE+wF+2 downto wE+wF+1);
   signY <= Y(wE+wF);
   expFieldY <= Y(wE+wF-1 downto wF);
   fracY <= Y(wF-1 downto 0);
-- Inputs analysis  --
-- zero inputs--
   zeroX <= '1' when flagsX="00" else '0';
   zeroY <= '1' when flagsY="00" else '0';
-- normal inputs--
   normalX <= '1' when flagsX="01" else '0';
   normalY <= '1' when flagsY="01" else '0';
-- inf input --
   infX <= '1' when flagsX="10" else '0';
   infY <= '1' when flagsY="10" else '0';
-- NaN inputs  --
   s_nan_in <= '1' when flagsX="11" or flagsY="11" else '0';
-- Comparison of X to 1   --
   OneExpFrac <=  "0" & (6 downto 0 => '1') & (22 downto 0 => '0');
   ExpFracX<= "0" & expFieldX & fracX;
   OneExpFracCompl<=  "1" & (not OneExpFrac);
   cmpXOne: IntAdder_32_f400_uid3  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => cmpXOneRes,
                 X => ExpFracX,
                 Y => OneExpFracCompl);

   ----------------Synchro barrier, entering cycle 1----------------
   XisOneAndNormal <= '1' when X_d1 = ("010" & OneExpFrac_d1) else '0';
   absXgtOneAndNormal <= normalX_d1 and (not XisOneAndNormal) and (not cmpXOneRes_d1(31));
   absXltOneAndNormal <= normalX_d1 and cmpXOneRes_d1(31);
   ----------------Synchro barrier, entering cycle 0----------------
   fracYreverted <= fracY(0)&fracY(1)&fracY(2)&fracY(3)&fracY(4)&fracY(5)&fracY(6)&fracY(7)&fracY(8)&fracY(9)&fracY(10)&fracY(11)&fracY(12)&fracY(13)&fracY(14)&fracY(15)&fracY(16)&fracY(17)&fracY(18)&fracY(19)&fracY(20)&fracY(21)&fracY(22);
   right1counter: LZOC_23_5_uid10  -- pipelineDepth=3 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 I => fracYreverted,
                 O => Z_rightY,
                 OZB => '0');
   ----------------Synchro barrier, entering cycle 3----------------
   ----------------Synchro barrier, entering cycle 4----------------
-- compute the weight of the less significant one of the mantissa
   WeightLSBYpre <= ('0' & expFieldY_d4)- CONV_STD_LOGIC_VECTOR(150,9);
   WeightLSBY <= WeightLSBYpre + Z_rightY_d1;
   ----------------Synchro barrier, entering cycle 5----------------
   oddIntY <= normalY_d5 when WeightLSBY_d1 = CONV_STD_LOGIC_VECTOR(0, 9) else '0'; -- LSB has null weight
   evenIntY <= normalY_d5 when WeightLSBY_d1(wE)='0' and oddIntY='0' else '0'; --LSB has strictly positive weight 
   notIntNormalY <= normalY_d5 when WeightLSBY_d1(wE)='1' else '0'; -- LSB has negative weight

-- Pow Exceptions  --
   RisInfSpecialCase  <= 
         (zeroX_d5  and  (oddIntY or evenIntY)  and signY_d5)  -- (+/- 0) ^ (negative int y)
      or (zeroX_d5 and infY_d5 and signY_d5)                      -- (+/- 0) ^ (-inf)
      or (absXgtOneAndNormal_d4   and  infY_d5  and not signY_d5) -- (|x|>1) ^ (+inf)
      or (absXltOneAndNormal_d4   and  infY_d5  and signY_d5)     -- (|x|<1) ^ (-inf)
      or (infX_d5 and  normalY_d5  and not signY_d5) ;            -- (inf) ^ (y>0)
   RisZeroSpecialCase <= 
         (zeroX_d5 and  (oddIntY or evenIntY)  and not signY_d5)  -- (+/- 0) ^ (positive int y)
      or (zeroX_d5 and  infY_d5  and not signY_d5)                   -- (+/- 0) ^ (+inf)
      or (absXltOneAndNormal_d4   and  infY_d5  and not signY_d5)    -- (|x|<1) ^ (+inf)
      or (absXgtOneAndNormal_d4   and  infY_d5  and signY_d5)        -- (|x|>1) ^ (-inf)
      or (infX_d5 and  normalY_d5  and signY_d5) ;                   -- (inf) ^ (y<0)
   RisOne <= 
         zeroY_d5                                          -- x^0 = 1 without exception
      or (XisOneAndNormal_d4 and signX_d5 and infY_d5)           -- (-1) ^ (-/-inf)
      or (XisOneAndNormal_d4  and not signX_d5);              -- (+1) ^ (whatever)
   RisNaN <= (s_nan_in_d5 and not zeroY_d5) or (normalX_d5 and signX_d5 and notIntNormalY);
   signR <= signX_d5 and (oddIntY);
   ----------------Synchro barrier, entering cycle 0----------------
   logIn <= flagsX & "0" & expFieldX & fracX & (9 downto 0 => '0') ;
   log: FPLog_8_33_9_400  -- pipelineDepth=23 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => lnX,
                 X => logIn);
   ----------------Synchro barrier, entering cycle 23----------------
   ----------------Synchro barrier, entering cycle 24----------------
   mult: FPMultiplier_8_33_8_23_8_34_uid197  -- pipelineDepth=6 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => P,
                 X => lnX_d1,
                 Y => Y_d24);
   ----------------Synchro barrier, entering cycle 30----------------
   ----------------Synchro barrier, entering cycle 31----------------
   exp: FPExp_8_23_400  -- pipelineDepth=14 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => E,
                 X => P_d1);
   ----------------Synchro barrier, entering cycle 45----------------
   ----------------Synchro barrier, entering cycle 46----------------
   flagsE <= E_d1(wE+wF+2 downto wE+wF+1);
   RisZeroFromExp <= '1' when flagsE="00" else '0';
   RisZero <= RisZeroSpecialCase_d41 or RisZeroFromExp;
   RisInfFromExp  <= '1' when flagsE="10" else '0';
   RisInf  <= RisInfSpecialCase_d41 or RisInfFromExp;
   flagR <= 
           "11" when RisNaN_d41='1'
      else "00" when RisZero='1'
      else "10" when RisInf='1'
      else "01";
   R_expfrac <= CONV_STD_LOGIC_VECTOR(127,8) &  CONV_STD_LOGIC_VECTOR(0, 23) when RisOne_d41='1'
       else E_d1(30 downto 0);
   R <= flagR & signR_d41 & R_expfrac;
end architecture;

--------------------------------------------------------------------------------
--                           FPPow_8_23_400_Wrapper
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 48 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPPow_8_23_400_Wrapper is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(33 downto 0);
          Y : in  std_logic_vector(33 downto 0);
          R : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of FPPow_8_23_400_Wrapper is
   component FPPow_8_23_400 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8+23+2 downto 0);
             Y : in  std_logic_vector(8+23+2 downto 0);
             R : out  std_logic_vector(8+23+2 downto 0)   );
   end component;

signal i_X, i_X_d1 :  std_logic_vector(33 downto 0);
signal i_Y, i_Y_d1 :  std_logic_vector(33 downto 0);
signal o_R, o_R_d1 :  std_logic_vector(33 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            i_X_d1 <=  i_X;
            i_Y_d1 <=  i_Y;
            o_R_d1 <=  o_R;
         end if;
      end process;
   i_X <= X;
   i_Y <= Y;
   ----------------Synchro barrier, entering cycle 1----------------
   test: FPPow_8_23_400  -- pipelineDepth=46 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => o_R,
                 X => i_X_d1,
                 Y => i_Y_d1);
   ----------------Synchro barrier, entering cycle 48----------------
   R <= o_R_d1;
end architecture;

